/**
 * Created by zhoulanhong on 9/26/17.
 * this is the global db pool for entire project
 * each module can use this pool in their own code
 */

sql = require('mssql');
var conf = require('../config/config');

cloudPool = new sql.ConnectionPool({
    user: conf.db.username,
    password: conf.db.password,
    server: conf.db.hostname,
    database: conf.db.database
});

wisePool = new sql.ConnectionPool({
    user: conf.htdb.username,
    password: conf.htdb.password,
    server: conf.htdb.hostname,
    database: conf.htdb.database
});

sql.on('error', err => {
    console.log("--- sql error ---");
    console.log(err);
});

sql.on('cloudPool', err => {
    console.log("--- cloudPool error ---");
    console.log(err);
});

sql.on('wisePool', err => {
    console.log("--- wisePool error ---");
    console.log(err);
})

console.log("--- init cloudPool success ---" + cloudPool);
console.log("--- init wisePool success ---" + wisePool);