/**
 * Created by zhoulanhong on 8/26/17.
 */
var sql = require('mssql');
var token = require('../libs/token');
var request = require('sync-request');
var conf = require('../config/config');
var Id = require('../libs/Id');
var logger = require('../config/log4js').getLogger('ScheduleJob');
var gys = require('../service/saveSupplier');
var Promise = require('bluebird');
var detail = require('../service/queryHtDetail');

var ScheduleJob = function () {
}
function noticeJob() {
    if (sql) {
        sql.close();
    }
    pool = new sql.ConnectionPool({
        user: conf.db.username,
        password: conf.db.password,
        server: conf.db.hostname, // You can use 'localhost\\instance' to connect to named instance
        database: conf.db.database
    }, err => {
        if (err) {
            console.log(err);
        } else {
            pool.request()
                .query(' select top 1 c.FASSIGNID, d.FNAME, c.FCREATETIME, b.FCONTENT, e.FOBJECTTYPEID, e.FKEYVALUE, b.FRECEIVERID, ' +
                    'c.FPROCINSTID, a.FPHONE, f.FDINACT from T_SEC_USER a, T_WF_RECEIVER b, T_WF_ASSIGN c, T_SEC_USER d, T_WF_APPROVALASSIGN e, ' +
                    'T_DIN_ACCT f where a.FUSERID = b.FRECEIVERID and b.FASSIGNID = c.FASSIGNID and c.FSENDERID = d.FUSERID ' +
                    'and c.FASSIGNID = e.FASSIGNID and c.FSTATUS = 0 and f.fphone = a.FPHONE and c.FASSIGNID not in ' +
                    '(select g.FASSIGNID from T_DIN_NOTICED g) ', (err, result) => {
                    console.log("--------pool--------");
                    if (err) {
                        console.log(err);
                    } else if (result.recordset.length != 0) {
                        let noticeList = result.recordset;
                        pool.request()
                            .query(" insert into T_DIN_NOTICED  values ('" + noticeList[0].FASSIGNID + "',getdate()) ", (err, insResult) => {
                                if (err) {
                                    console.log("insert t_din_notice error" + err);
                                }
                                pool.close();
                                console.log("---------------- "+JSON.stringify(noticeList));
                                //for (var i in noticeList) {
                                    let createGroupBack = request('POST', 'https://oapi.dingtalk.com/message/send?access_token=' + token.token()
                                        , {
                                            json: {
                                                "touser": noticeList[0].FDINACT,
                                                "agentid": conf.agentId,
                                                "msgtype": "link",
                                                "link": {
                                                    "messageUrl": "http://" + conf.hostName + ":" + conf.port + "/detail/" + noticeList[0].FOBJECTTYPEID + "/" + noticeList[0].FASSIGNID + "/" + noticeList[0].FPHONE,
                                                    "picUrl": "https://thumbs.dreamstime.com/z/%E5%AE%A1%E6%89%B9-13432038.jpg",
                                                    "title": noticeList[0].FCONTENT,
                                                    "text": noticeList[0].FNAME
                                                }
                                            }
                                        });
                                    console.log('-------dd ret------ noticed : ' + JSON.stringify(createGroupBack));
                                //}
                            });

                    }
                });
        }
    });

    pool.on('error', err => {
        console.log(err);
        pool.close();
    });
    payPool = new sql.ConnectionPool({
        user: conf.db.username,
        password: conf.db.password,
        server: conf.db.hostname, // You can use 'localhost\\instance' to connect to named instance
        database: conf.db.database
    }, err => {
        let queryRet = null;
        logger.info("select top 1 b.FID, a.FKDCODE,a.FCONTRACT_ID,a.FAPPLY_ID,a.STATUS,a.FCREATETIME,a.FAPPLY_CODE, c.FREALPAYAMOUNTFOR , a.FCONTRACT_MONEY" +
            " from T_FKD_SYNC_WISE a, " +
            "T_AP_PAYBILLSRCENTRY b, T_AP_PAYBILL c where a.FKDCODE = b.FSRCBILLID and b.FID = c.FID " +
            "and c.FDOCUMENTSTATUS = 'C' and b.fid not in (select FKDCODE from T_DEALED_PAY)");
        payPool.request()
            .query("select top 1 b.FID, a.FKDCODE,a.FCONTRACT_ID,a.FAPPLY_ID,a.STATUS,a.FCREATETIME,a.FAPPLY_CODE, c.FREALPAYAMOUNTFOR , a.FCONTRACT_MONEY" +
                " from T_FKD_SYNC_WISE a, " +
                "T_AP_PAYBILLSRCENTRY b, T_AP_PAYBILL c where a.FKDCODE = b.FSRCBILLID and b.FID = c.FID " +
                "and c.FDOCUMENTSTATUS = 'C' and b.fid not in (select FKDCODE from T_DEALED_PAY)").then(result=> {
            queryRet = result;
            if (queryRet.recordset.length != 0) {
                //获取此付款申请单的已付款数据
                logger.info("select FAPPLYAMOUNTFOR as totalApply, (select sum(FREALPAYAMOUNT) from T_AP_PAYBILL where FID in " +
                    "(select FID from T_AP_PAYBILLSRCENTRY where FSRCBILLID = " + queryRet.recordset[0].FKDCODE + ")) as totalPaid, " +
                    "(select max(FEntryID)from KLM_t_Cust_Entry100069 where FID = " + queryRet.recordset[0].FKDCODE + ") as maxEntryId, " +
                    "(select FREALPAYAMOUNT from t_ap_paybill where FID = '" + queryRet.recordset[0].FID + "') as curPaid ," +
                    "(select F_KLM_AMOUNT2 from T_CN_PAYAPPLY where FID = " + queryRet.recordset[0].FKDCODE + ") as htmoney " +
                    "from T_CN_PAYAPPLYENTRY where fid = " + queryRet.recordset[0].FKDCODE);
                payPool.request()
                    .query("select FAPPLYAMOUNTFOR as totalApply, (select sum(FREALPAYAMOUNT) from T_AP_PAYBILL where FID in " +
                        "(select FID from T_AP_PAYBILLSRCENTRY where FSRCBILLID = " + queryRet.recordset[0].FKDCODE + ")) as totalPaid, " +
                        "(select max(FEntryID)from KLM_t_Cust_Entry100069 where FID = " + queryRet.recordset[0].FKDCODE + ") as maxEntryId, " +
                        "(select FREALPAYAMOUNT from t_ap_paybill where FID = '" + queryRet.recordset[0].FID + "') as curPaid ," +
                        "(select F_KLM_AMOUNT2 from T_CN_PAYAPPLY where FID = " + queryRet.recordset[0].FKDCODE + ") as htmoney " +
                        "from T_CN_PAYAPPLYENTRY where fid = " + queryRet.recordset[0].FKDCODE).then(dataRet=> {

                    let totalApply = dataRet.recordset[0].totalApply;
                    let totalPaid = dataRet.recordset[0].totalPaid;
                    let maxEntryId = dataRet.recordset[0].maxEntryId;
                    let curPaid = dataRet.recordset[0].curPaid;
                    let htMoney = dataRet.recordset[0].htmoney;
                    let paidRate = totalPaid / totalApply;
                    let fund = totalApply - totalPaid;

                    try {
                        logger.info("--------payPool--------");
                        let updSqls = [payPool.request()
                            .query("update KLM_t_Cust_Entry100069 set F_KLM_AMOUNT = " + totalPaid + " ,F_KLM_DECIMAL = " + paidRate + " where FEntryID = " + maxEntryId),
                            payPool.request()
                                .query("update T_CN_PAYAPPLYENTRY set FRELATEPAYAMOUNT = " + totalPaid + " , FRELATEREFUNDAMOUNT = " + fund + " where FID = " + queryRet.recordset[0].FKDCODE),
                            payPool.request()
                                .query("insert into T_DEALED_PAY values (" + queryRet.recordset[0].FID + "," + queryRet.recordset[0].FREALPAYAMOUNTFOR + ",GETDATE())"),
                            payPool.request()
                                .query("update T_CN_PAYAPPLY set F_KLM_AMOUNT5 = ( " +
                                    "select sum(FREALPAYAMOUNT) from T_AP_PAYBILL where FID in (select  b.FID from T_FKD_SYNC_WISE a, " +
                                    "T_AP_PAYBILLSRCENTRY b, T_AP_PAYBILL c where a.FKDCODE = b.FSRCBILLID and b.FID = c.FID " +
                                    "and c.FDOCUMENTSTATUS = 'C' and a.FCONTRACT_ID = '" + queryRet.recordset[0].FCONTRACT_ID +
                                    "')) where FID in (select distinct a.FKDCODE from T_FKD_SYNC_WISE a " +
                                    "T_AP_PAYBILLSRCENTRY b, T_AP_PAYBILL c where a.FKDCODE = b.FSRCBILLID and b.FID = c.FID " +
                                    "and c.FDOCUMENTSTATUS = 'C'  and a.FCONTRACT_ID = '" + queryRet.recordset[0].FCONTRACT_ID + "')"),
                            payPool.request()
                                .query("update KLM_t_Cust_Entry100069 set F_KLM_AMOUNT1 = (" +
                                    "select sum(FREALPAYAMOUNT) from T_AP_PAYBILL where FID in (select  b.FID from T_FKD_SYNC_WISE a," +
                                    "T_AP_PAYBILLSRCENTRY b, T_AP_PAYBILL c where a.FKDCODE = b.FSRCBILLID and b.FID = c.FID " +
                                    "and c.FDOCUMENTSTATUS = 'C' and a.FCONTRACT_ID = '1710261051002399b886c7e8d33c6138')) " +
                                    "where FID in (select distinct a.FKDCODE from T_FKD_SYNC_WISE a, " +
                                    "T_AP_PAYBILLSRCENTRY b, T_AP_PAYBILL c where a.FKDCODE = b.FSRCBILLID and b.FID = c.FID " +
                                    "and c.FDOCUMENTSTATUS = 'C'  and a.FCONTRACT_ID = '" + queryRet.recordset[0].FCONTRACT_ID + "')")
                        ];
                        logger.info("-------- a payPool b --------");
                        logger.info("update KLM_t_Cust_Entry100069 set F_KLM_AMOUNT = " + totalPaid + " ,F_KLM_DECIMAL = " + paidRate + " where FEntryID = " + maxEntryId);
                        logger.info("update T_CN_PAYAPPLYENTRY set FRELATEPAYAMOUNT = " + totalPaid + " , FRELATEREFUNDAMOUNT = " + fund + " where FID = " + queryRet.recordset[0].FKDCODE);
                        logger.info("insert into T_DEALED_PAY values (" + queryRet.recordset[0].FID, +"," + queryRet.recordset[0].FREALPAYAMOUNTFOR + ",GETDATE())");
                        logger.info("update T_CN_PAYAPPLY set F_KLM_AMOUNT5 = ( " +
                            "select sum(FREALPAYAMOUNT) from T_AP_PAYBILL where FID in (select  b.FID from T_FKD_SYNC_WISE a, " +
                            "T_AP_PAYBILLSRCENTRY b, T_AP_PAYBILL c where a.FKDCODE = b.FSRCBILLID and b.FID = c.FID " +
                            "and c.FDOCUMENTSTATUS = 'C' and a.FCONTRACT_ID = '" + queryRet.recordset[0].FCONTRACT_ID +
                            "')) where FID in (select distinct a.FKDCODE from T_FKD_SYNC_WISE a " +
                            "T_AP_PAYBILLSRCENTRY b, T_AP_PAYBILL c where a.FKDCODE = b.FSRCBILLID and b.FID = c.FID " +
                            "and c.FDOCUMENTSTATUS = 'C'  and a.FCONTRACT_ID = '" + queryRet.recordset[0].FCONTRACT_ID + "')");
                        logger.info("update KLM_t_Cust_Entry100069 set F_KLM_AMOUNT1 = (" +
                            "select sum(FREALPAYAMOUNT) from T_AP_PAYBILL where FID in (select  b.FID from T_FKD_SYNC_WISE a," +
                            "T_AP_PAYBILLSRCENTRY b, T_AP_PAYBILL c where a.FKDCODE = b.FSRCBILLID and b.FID = c.FID " +
                            "and c.FDOCUMENTSTATUS = 'C' and a.FCONTRACT_ID = '1710261051002399b886c7e8d33c6138')) " +
                            "where FID in (select distinct a.FKDCODE from T_FKD_SYNC_WISE a, " +
                            "T_AP_PAYBILLSRCENTRY b, T_AP_PAYBILL c where a.FKDCODE = b.FSRCBILLID and b.FID = c.FID " +
                            "and c.FDOCUMENTSTATUS = 'C'  and a.FCONTRACT_ID = '" + queryRet.recordset[0].FCONTRACT_ID + "')");
                        return Promise.all(updSqls);
                    } catch (e) {
                        logger.info(e);
                    }

                });
            } else {
                return;
            }
        }).then(upd=> {
                if (queryRet.recordset.length != 0) {
                    //console.log("_____payPool result_____ + " + JSON.stringify(result));
                    let payMethod = "11060315005826298fca20365fdfdcab";
                    if (queryRet.recordset[0].FSETTLETYPEID == 1) {
                        payMethod = '11060315023705999c80e1a69f183a00';
                    }
                    wisePool = new sql.ConnectionPool({
                            user: conf.htdb.username,
                            password: conf.htdb.password,
                            server: conf.htdb.hostname, // You can use 'localhost\\instance' to connect to named instance
                            database: conf.htdb.database
                        }, err => {
                            console.log("---------------------------- before query register --------------------------------");
                            let regCode = queryRet.recordset[0].FAPPLY_CODE + "-F-001";
                            console.log("select top 1 REGISTER_CODE from T_CON_PAY_REGISTER where CONTRACT_ID = '" +
                                queryRet.recordset[0].FCONTRACT_ID + "' and APPLY_ID = '" + queryRet.recordset[0].FAPPLY_ID + "' order by REGISTER_CODE desc");
                            wisePool.request()
                                .query("select top 1 REGISTER_CODE from T_CON_PAY_REGISTER where CONTRACT_ID = '" +
                                    queryRet.recordset[0].FCONTRACT_ID + "' and APPLY_ID = '" + queryRet.recordset[0].FAPPLY_ID + "' order by REGISTER_CODE desc").then(ret=> {
                                if (ret.recordset.length != 0) {
                                    let tmpNbr = ret.recordset[0].REGISTER_CODE.substring(ret.recordset[0].REGISTER_CODE.length - 3);
                                    let newCode = parseInt(tmpNbr) + 1;
                                    if (newCode < 9) {
                                        regCode = queryRet.recordset[0].FAPPLY_CODE + "-F-00" + newCode;
                                    } else if (newCode < 99) {
                                        regCode = queryRet.recordset[0].FAPPLY_CODE + "-F-0" + newCode;
                                    } else {
                                        regCode = queryRet.recordset[0].FAPPLY_CODE + "-F-" + newCode;
                                    }
                                }
                                console.log("insert into T_CON_PAY_REGISTER(REGISTER_ID, CONTRACT_ID, APPLY_ID, PAID_MONEY, PAYMENT_BY," +
                                    "CREATOR_NAME,PAID_TIME,HANDLER,CREATE_TIME,UNIT_ID,UNIT_NAME,REGISTER_CODE) select '" +
                                    (Date.now() + Id.get()) + "',CONTRACT_ID, APPLY_ID, '" + queryRet.recordset[0].FREALPAYAMOUNTFOR + "', '" + payMethod +
                                    "', HANDLER, GETDATE(),HANDLER, GETDATE(),B_ID,B_NAME,'" + regCode + "' " +
                                    "from T_FKD_APPLY where FKDCODE = '" + queryRet.recordset[0].FKDCODE + "'");
                                return wisePool.request()
                                    .query("insert into T_CON_PAY_REGISTER(REGISTER_ID, CONTRACT_ID, APPLY_ID, PAID_MONEY, PAYMENT_BY," +
                                        "CREATOR_NAME,PAID_TIME,HANDLER,CREATE_TIME,UNIT_ID,UNIT_NAME,REGISTER_CODE) select '" +
                                        (Date.now() + Id.get()) + "',CONTRACT_ID, APPLY_ID, '" + queryRet.recordset[0].FREALPAYAMOUNTFOR + "', '" + payMethod +
                                        "', HANDLER, GETDATE(),HANDLER, GETDATE(),B_ID,B_NAME,'" + regCode + "' " +
                                        "from T_FKD_APPLY where FKDCODE = '" + queryRet.recordset[0].FKDCODE + "'");
                            }).then(result=> {
                                console.log("-------- insert into wise success  ---------");
                                console.log("update T_CON_PAY_APPLY set SUM_OUTPUT_VALUE_MONEY = " + totalPaid + " where APPLY_ID = '" + queryRet.recordset[0].FAPPLY_ID + "'");
                                return wisePool.request()
                                    .query("update T_CON_PAY_APPLY set SUM_OUTPUT_VALUE_MONEY = " + totalPaid + " where APPLY_ID = '" + queryRet.recordset[0].FAPPLY_ID + "'");
                            }).then(result=> {
                                console.log("-------- update success  ---------");
                            }).catch(e=> {
                                console.log(e);
                                console.error("-------- error occur  ---------");
                            });
                        }
                    );
                }
            }
        );
    });

    pool.on('error', err => {
        console.log(err);
        pool.close();
    });

//通知合同消息
//    htPool = new sql.ConnectionPool({
//        user: conf.htdb.username,
//        password: conf.htdb.password,
//        server: conf.htdb.hostname, // You can use 'localhost\\instance' to connect to named instance
//        database: conf.htdb.database
//    }, err => {
//        htPool.request()
//            .query("select top 1 a.TASK_ID, a.WF_ID, a.BUSINESS_ID, a.ACTOR_ID, a.BUSINESS_TYPE, d.WF_NAME, e.NAME as CREATOR_NAME, c.FDINACT " +
//                "from T_SYS_WF_TASK a, T_SYS_USERINFO b, T_DIN_ACCT c, T_SYS_WF d, T_SYS_USERINFO e " +
//                "where a.WF_ID = d.WF_ID and d.INITIATOR_ID = e.ORG_ID and " +
//                "a.ACTOR_ID = b.ORG_ID and b.MOBILE = c.FPHONE " +
//                "and a.STATE !=4 and a.TASK_ID not in (select FASSIGNID from T_DIN_NOTICED)", (err, result) => {
//                logger.info("---------------------------- query he tong --------------------------------");
//                if (err) {
//                    console.log(err);
//                } else if (result.recordset.length != 0) {
//                    console.log("contract will notice result : " + JSON.stringify(result));
//                    let noticeList = result.recordset;
//                    htPool.request()
//                        .query("insert into T_DIN_NOTICED values ('" + noticeList[0].TASK_ID + "',getdate())", (err, insResult) => {
//                            if (err) {
//                                console.log("insert t_din_notice error" + err);
//                            }
//                            htPool.close();
//                            for (var i in noticeList) {
//                                //不是合同支付,直接发消息
//                                if (noticeList[i].BUSINESS_TYPE.trim() != "t_con_pay_apply") {
//                                    let createGroupBack = request('POST', 'https://oapi.dingtalk.com/message/send?access_token=' + token.token()
//                                        , {
//                                            json: {
//                                                "touser": noticeList[i].FDINACT,
//                                                "agentid": conf.agentId,
//                                                "msgtype": "link",
//                                                "link": {
//                                                    "messageUrl": "http://" + conf.hostName + ":" + conf.port + "/htdetail/" + noticeList[i].BUSINESS_TYPE.trim() + "/"
//                                                    + noticeList[i].WF_ID + "/" + noticeList[i].ACTOR_ID,
//                                                    "picUrl": "https://thumbs.dreamstime.com/z/%E5%AE%A1%E6%89%B9-13432038.jpg",
//                                                    "title": noticeList[i].WF_NAME,
//                                                    "text": noticeList[i].CREATOR_NAME
//                                                }
//                                            }
//                                        });
//                                    logger.info("send notice message no pay_apply: " + JSON.stringify({
//                                            "touser": noticeList[i].FDINACT,
//                                            "agentid": conf.agentId,
//                                            "msgtype": "link",
//                                            "link": {
//                                                "messageUrl": "http://" + conf.hostName + ":" + conf.port + "/htdetail/" + noticeList[i].BUSINESS_TYPE.trim() + "/"
//                                                + noticeList[i].WF_ID + "/" + noticeList[i].ACTOR_ID,
//                                                "picUrl": "https://thumbs.dreamstime.com/z/%E5%AE%A1%E6%89%B9-13432038.jpg",
//                                                "title": noticeList[i].WF_NAME,
//                                                "text": noticeList[i].CREATOR_NAME
//                                            }
//                                        }));
//                                    logger.info("send notice back message :" + createGroupBack.getBody('utf8'));
//                                } else {
//                                    detail.queryHtDetails("t_con_pay_apply", noticeList[i].WF_ID, null, false, function (err, data) {
//                                        if (err) {
//                                            console.log("--- error ----");
//                                        } else {
//                                            let createGroupBack = request('POST', 'https://oapi.dingtalk.com/message/send?access_token=' + token.token()
//                                                , {
//                                                    json: {
//                                                        "touser": noticeList[i].FDINACT,
//                                                        "agentid": conf.agentId,
//                                                        "msgtype": "link",
//                                                        "link": {
//                                                            "messageUrl": "http://" + conf.hostName + ":" + conf.port + "/htdetail/" + noticeList[i].BUSINESS_TYPE.trim() + "/"
//                                                            + noticeList[i].WF_ID + "/" + noticeList[i].ACTOR_ID,
//                                                            "picUrl": "https://thumbs.dreamstime.com/z/%E5%AE%A1%E6%89%B9-13432038.jpg",
//                                                            "title": data.htmc,
//                                                            "text": "对方单位:" + data.dfdw + "\n合同金额:" + data.htje + "\n本次申请金额:" + data.bcqk
//                                                        }
//                                                    }
//                                                });
//                                            logger.info("in else send send pay_apply message : " + JSON.stringify({
//                                                    "touser": noticeList[i].FDINACT,
//                                                    "agentid": conf.agentId,
//                                                    "msgtype": "link",
//                                                    "link": {
//                                                        "messageUrl": "http://" + conf.hostName + ":" + conf.port + "/htdetail/" + noticeList[i].BUSINESS_TYPE.trim() + "/"
//                                                        + noticeList[i].WF_ID + "/" + noticeList[i].ACTOR_ID,
//                                                        "picUrl": "https://thumbs.dreamstime.com/z/%E5%AE%A1%E6%89%B9-13432038.jpg",
//                                                        "title": data.htmc,
//                                                        "text": "对方单位:" + data.dfdw + "\n合同金额:" + data.htje + "\n本次申请金额:" + data.bcqk
//                                                    }
//                                                }));
//                                            logger.info("in else send pay_apply back message :" + createGroupBack.getBody('utf8'));
//                                        }
//                                    });
//                                }
//                            }
//                        });
//                }
//            });
//
//    });

//同步供应商
//    gysPool = new sql.ConnectionPool({
//        user: conf.htdb.username,
//        password: conf.htdb.password,
//        server: conf.htdb.hostname, // You can use 'localhost\\instance' to connect to named instance
//        database: conf.htdb.database
//    }, err => {
//        gysPool.request()
//            .query("select a.PROVIDER_ID, a.COMPANY_NAME, a.BANKNAMBER, a.BANKMASTERNAME from T_KNO_PROVIDER a, T_KNO_PROVIDER_STATE b " +
//                "where a.PROVIDER_ID = b.PROVIDER_ID and b.LEVEL_ID = 4 and a.PROVIDER_ID not in (select wise_id from T_GYS_SYNC_WISE)", (err, result) => {
//                logger.info("---------------------------- query gys --------------------------------");
//                if (err) {
//                    console.log(err);
//                } else if (result.recordset.length != 0) {
//                    console.log("--gys result :" + JSON.stringify(result));
//                    let noticeList = result.recordset;
//                    for (var i in noticeList) {
//                        gys({
//                            providerId: noticeList[i].PROVIDER_ID, providerName: noticeList[i].COMPANY_NAME,
//                            bnkNbr: noticeList[i].BANKNAMBER, bnkName: noticeList[i].BANKMASTERNAME
//                        }, function (err, data) {
//                            if (err) {
//                                console.log("---err---");
//                            } else {
//                                console.log("---success---");
//                            }
//                        });
//                    }
//                }
//            });
//    });
//
//    htPool.on('error', err => {
//        console.log(err);
//        htPool.close();
//    });
}
ScheduleJob.noticeJob = noticeJob;
module.exports = ScheduleJob;