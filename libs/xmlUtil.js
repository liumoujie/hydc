/**
 * Created by zhoulanhong on 8/15/17.
 */
var logger = require('../config/log4js').getLogger('xmlUtil');
var parser = require('fast-xml-parser');

let findNextStepFun = function (xml, currentUser, action) {
    logger.info("currentUser : " + currentUser + " action : " + action);
    var jsonData = parser.parse(xml);
    let curActivityId = "";
    let curActionEventId = "";
    let nativeActivties = jsonData.Process.Activities.NativeActivity;
    let nextActivity = "";
    let nextActionEventId = "";
    let lines = jsonData.Process.Lines.Line;
    let ret = {};
    let find = false;
    let firstActivityId = null;
    let userFind = false;

    for (var i in nativeActivties) {
        try {

            if (nativeActivties[i].DisplayName.indexOf("一级节点") > 0) {
                firstActivityId = nativeActivties[i].ActivityId;
            }

            let curUserId = nativeActivties[i].ActionEvents.ApprovalAction.Participants.InArgument.Expression.ConstExpression.ConstStorage.MixObjectWrapper.CollectionObject.MixObjectWrapper.ComplexObject.ActionParticipant.Receivers.UserMember;

            //说明有多个审批人
            if (Object.prototype.toString.call(curUserId) === '[object Array]') {
                for (let k in curUserId) {
                    if (curUserId[k].Id == currentUser) {
                        find = true;
                        break;
                    }
                }
            } else if (curUserId == undefined) {
                let combinRow = nativeActivties[i].ActionEvents.ApprovalAction.Participants.InArgument.Expression.ConstExpression.ConstStorage.MixObjectWrapper.CollectionObject.MixObjectWrapper.ComplexObject.ActionParticipant.Receivers.CombinationMember.MemberRow.CombinationMemberRow;

                for (let j in combinRow) {
                    if (combinRow[j].Member.UserMember.Id == currentUser) {
                        find = true;
                        break;
                    }
                }
            }
            if (find || curUserId.Id == currentUser) {
                curActionEventId = nativeActivties[i].ActionEvents.ApprovalAction.ActionEventId;
                curActivityId = nativeActivties[i].ActivityId;
                userFind = true;
                break;
            }
        } catch (e) {
            console.log(i + e.message);
        }
    }

    //说明当前人是发起人
    if (!userFind) {
        nextActivity = firstActivityId;
        curActionEventId = "1";
    }

    if (!nextActivity) {
        for (var k in lines) {
            if (lines[k].SourceId == curActivityId) {
                if (lines[k].DisplayName.length > 0 && lines[k].DisplayName.indexOf(action) < 0) {
                    continue;
                }

                if (lines[k].DisplayName.length == 0) {
                    nextActivity = lines[k].TargetId;
                    break;
                }

                if (lines[k].DisplayName.indexOf(action) > 0) {
                    nextActivity = lines[k].TargetId;
                    break;
                }
            }
        }
    }

    for (var j in nativeActivties) {
        if (nativeActivties[j].ActivityId == nextActivity) {
            let actionXml = null;
            try {
                let collectObjs = nativeActivties[j].ActionEvents.ApprovalAction.Actions.InArgument.Expression.ConstExpression.ConstStorage.MixObjectWrapper.CollectionObject.MixObjectWrapper;

                actionXml = "<AssignResultForSer><AssignResults>"
                let assResults = []
                for (var ac in collectObjs) {
                    let curAssResult = collectObjs[ac].ComplexObject.AssignResult;
                    actionXml += "<AssignResult>";
                    for (let key in curAssResult) {
                        actionXml += "<" + key + ">" + curAssResult[key] + "</" + key + ">"
                    }
                    actionXml += "</AssignResult>";
                }
                actionXml += "</AssignResults > </AssignResultForSer>";

            } catch (e) {
                //console.log(e);
                console.log("---- no actions ------");
            }


            try {
                nextActionEventId = nativeActivties[j].ActionEvents.ApprovalAction.ActionEventId;
            } catch (e) {
                //console.log(e);
                console.log("---- no actions ------");
            }


            let displayName = nativeActivties[j].DisplayName.split("/");
            if (displayName[2] == "完成") {
                ret = {
                    "Id": 0,
                    "Name": "End"
                }
                break;
            }
            if (displayName[2] == "发起人修改") {
                ret = {
                    "Id": 1,
                    "Name": "Back2Starter"
                }
                break;
            }

            if (action != "打回发起人") {
                ret = nativeActivties[j].ActionEvents.ApprovalAction.Participants.InArgument.Expression.ConstExpression.ConstStorage.MixObjectWrapper.CollectionObject.MixObjectWrapper.ComplexObject.ActionParticipant.Receivers.UserMember;

                if (ret == undefined) {
                    let seed = nativeActivties[j].ActionEvents.ApprovalAction.Participants.InArgument.Expression.ConstExpression.ConstStorage.MixObjectWrapper.CollectionObject.MixObjectWrapper.ComplexObject.ActionParticipant.Receivers.CombinationMember.MemberRow.CombinationMemberRow.length;
                    ret = nativeActivties[j].ActionEvents.ApprovalAction.Participants.InArgument.Expression.ConstExpression.ConstStorage.MixObjectWrapper.CollectionObject.MixObjectWrapper.ComplexObject.ActionParticipant.Receivers.CombinationMember.MemberRow.CombinationMemberRow[parseInt(Math.random() * seed)].Member.UserMember;
                }

            }
            if (actionXml) {
                ret.actionXml = actionXml;
            }

            //2052代表是中文
            ret.nextNodeId = 2052;
            for (var n = 0; n < displayName.length; n++) {
                if (displayName[n] == "2052") {
                    ret.nextNodeName = displayName[(n + 1)];
                    break;
                }
            }

            break;
        }
    }
    ret.nextActivity = nextActivity;
    ret.nextActionEventId = nextActionEventId;
    ret.curActionEventId = curActionEventId;
    return ret;
}

exports.findNextStep = findNextStepFun;