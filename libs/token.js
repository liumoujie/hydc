/**
 * Created by ring on 2015/6/16.
 */
var request = require('sync-request');
var conf = require('../config/config');
var id = {};
var WeChatToken = function () {
}

/*
 * 在获取token的时候,同时生成ticket并且设置 token 的过期时间为 2 小时
 * 获取ticket的时候,同时去判断token的过期时间,如果到了,就要重新获取token,同时也再获取ticket
 * */

var getToken = function () {
    console.log("-- begin to get token --");
    console.log(_.isNull(id.access_token) || _.isUndefined(id.access_token));
    console.log(id.expires_in + "  now : " + Date.now() + "  expin : " + (id.expires_in - Date.now()));
    if (_.isNull(id.access_token) || _.isUndefined(id.access_token) || (id.expires_in - Date.now()) < 300000) {
        //if (_.isNull(id.access_token) || _.isUndefined(id.access_token) || (id.expires_in - Date.now()) < 7000 * 1000) {
        console.log("--- token expired need re get get token first ----");
        getDDToken();
    }
    return id.access_token;
}

var getTicket = function () {
    console.log("-- begin to get ticket start --");
    console.log(id.ticket);
    console.log(_.isNull(id.ticket));
    console.log(id.expires_in + "  now : " + Date.now() + "  expin : " + (id.expires_in - Date.now()));
    if (_.isNull(id.access_token) || _.isUndefined(id.access_token) || (id.expires_in - Date.now()) < 300000) {
        //if (_.isNull(id.access_token) || _.isUndefined(id.access_token) || (id.expires_in - Date.now()) < 7000 * 1000) {
        console.log("--- ticket expired need re get get token first ----");
        getToken()
    }

    return id.ticket;
}

function getDDToken() {

    var res = request('GET', 'https://oapi.dingtalk.com/gettoken?corpid=' + conf.corpid +
        '&corpsecret=' + conf.corpsecret);

    console.log("get token back:" + res.getBody('utf8'));
    try {
        var resObj = JSON.parse(res.getBody('utf8'));
        id.access_token = resObj.access_token;
        id.expires_in = 2 * 3600 * 1000 + Date.now();
        getDDTicket(resObj.access_token);
    } catch (e) {
        console.log(e);
        console.log("get token from dd error");
    }
}

function getDDTicket(token) {
    var res = request('GET', 'https://oapi.dingtalk.com/get_jsapi_ticket?access_token=' + token);
    console.log("get ticket back :" + res.getBody('utf8'));
    var resObj = JSON.parse(res.getBody('utf8'));
    id.ticket = resObj.ticket;
}


WeChatToken.token = getToken;
WeChatToken.ticket = getTicket;
module.exports = WeChatToken;