/**
 * Created by zhoulanhong on 11/6/18.
 */
var localConfig = {
    mode: "localhost",
    hostName: "localhost",
    port: 3000,
    baseUrl: "http://kdxrd.gnway.cc:8009/K3Cloud",
    loginInfo: {
        userName: "admin",
        appId: "cdbqtest",
        appPassword: "3964a021740e4ff08a00e6ba9fe0e335",
        dbCenterId: "5bd01bc4ea5ec2",
        lang: "2052",
    },
    webApi: {
        loginUrl: "/Kingdee.BOS.WebApi.ServicesStub.AuthService.LoginByAppSecret.common.kdsvc",
        saveUrl: "/Kingdee.BOS.WebApi.ServicesStub.DynamicFormService.Save.common.kdsvc",
        submit: "/Kingdee.BOS.WebApi.ServicesStub.DynamicFormService.Submit.common.kdsvc",
        audit: "/Kingdee.BOS.WebApi.ServicesStub.DynamicFormService.Audit.common.kdsvc",
        view: "/Kingdee.BOS.WebApi.ServicesStub.DynamicFormService.View.common.kdsvc",
        query: "/Kingdee.BOS.WebApi.ServicesStub.DynamicFormService.ExecuteBillQuery.common.kdsvc"
    }
}

var prodConfig = {
    mode: "prod",
    hostName: "118.122.91.72",
    port: 3000,

}

module.exports=localConfig;
//module.exports=prodConfig;