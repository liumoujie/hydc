/**
 * Created by ring on 2015/5/15.
 * This is the logger configuration file
 */
var log4js = require('log4js');

log4js.configure(
    {
        appenders: {
            dateFile: {
                type: 'dateFile',
                maxLogSize: 10485760,
                filename: 'logs/hyjd.log',
                pattern: 'yyyy-MM-dd-hh',
                compress: false
            },
            console: {type: 'console'}
        },
        categories: {
            default: {appenders: ['console', 'dateFile'], level: 'trace'}
        }
    }
);

module.exports = log4js;