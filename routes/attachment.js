/**
 * Created by zhoulanhong on 8/26/17.
 */
var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var contract = require('../service/contractAction');
var conf = require('../config/config.js');

router.get('/contract/t_con_contract/:fileName', bodyParser.json(), function (req, res, next) {
//approve20170930095338870.jpg
    var options = {
        //root: conf + '/pds/contract/t_con_contract/',
        root: 'c:/rmis/pds/contract/t_con_contract/',
        dotfiles: 'deny',
        headers: {
            'x-timestamp': Date.now(),
            'x-sent': true
        }
    };

    var fileName = req.params.fileName;
    console.log(fileName);
    res.sendFile(fileName, options, function (err) {
        if (err) {
            console.log(err);
            res.status(err.status).end();
        }
        else {
            console.log('Sent:', fileName);
        }
    });
});

router.get('/contract/t_con_pay_apply/:fileName', bodyParser.json(), function (req, res, next) {
//approve20170930095338870.jpg
    var options = {
        //root: conf + '/pds/contract/t_con_contract/',
        root: 'c:/rmis/pds/contract/t_con_pay_apply/',
        dotfiles: 'deny',
        headers: {
            'x-timestamp': Date.now(),
            'x-sent': true
        }
    };

    var fileName = req.params.fileName;
    console.log(fileName);
    res.sendFile(fileName, options, function (err) {
        if (err) {
            console.log(err);
            res.status(err.status).end();
        }
        else {
            console.log('Sent:', fileName);
        }
    });
});

router.get('/sdContract/:fileName', bodyParser.json(), function (req, res, next) {
//approve20170930095338870.jpg
// jpg  bmp  jpeg   gif  tif  tiff
    console.log("--------------- in sd get ------------");
    let extNames = ["jpg","bmp","jpeg","gif","tif","tiff"];
    var options = {
        //root: conf + '/pds/contract/t_con_contract/',
        root: 'E:/K3CLOUDEnclosure/Doc/2018/',
        dotfiles: 'deny',
        headers: {
            'x-timestamp': Date.now(),
            'x-sent': true
        }
    };

    var fileName = req.params.fileName;

    if (extNames.indexOf(fileName.substr(fileName.indexOf(".") + 1)) > -1) {
        options.root = 'E:/K3CLOUDEnclosure/Image/2018/';
    }

    console.log(fileName);
    res.sendFile(fileName, options, function (err) {
        if (err) {
            console.log(err);
            res.status(err.status).end();
        }
        else {
            console.log('Sent:', fileName);
        }
    });
});

router.get('/contract/t_con_change/:fileName', bodyParser.json(), function (req, res, next) {
//approve20170930095338870.jpg
    var options = {
        //root: conf + '/pds/contract/t_con_contract/',
        root: 'c:/rmis/pds/contract/t_con_change/',
        dotfiles: 'deny',
        headers: {
            'x-timestamp': Date.now(),
            'x-sent': true
        }
    };

    var fileName = req.params.fileName;
    console.log(fileName);
    res.sendFile(fileName, options, function (err) {
        if (err) {
            console.log(err);
            res.status(err.status).end();
        }
        else {
            console.log('Sent:', fileName);
        }
    });
});

router.get('/contract/t_con_balance/:fileName', bodyParser.json(), function (req, res, next) {
//approve20170930095338870.jpg
    var options = {
        //root: conf + '/pds/contract/t_con_contract/',
        root: 'c:/rmis/pds/contract/t_con_balance/',
        dotfiles: 'deny',
        headers: {
            'x-timestamp': Date.now(),
            'x-sent': true
        }
    };

    var fileName = req.params.fileName;
    console.log(fileName);
    res.sendFile(fileName, options, function (err) {
        if (err) {
            console.log(err);
            res.status(err.status).end();
        }
        else {
            console.log('Sent:', fileName);
        }
    });
});

module.exports = router;