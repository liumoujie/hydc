/**
 * Created by zhoulanhong on 8/26/17.
 */
var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var action = require('../service/agreeAction');
var contract = require('../service/contractAction');
var logger = require('../config/log4js').getLogger('audit');

router.post('/agree', bodyParser.json(), function (req, res, next) {

    action.agree(req.body.table, req.body.assignid, req.body.userid, req.body.procInstId, req.body.preArrprovId, req.body.remark,
        req.body.msg, req.body.msgName, req.body.keyValue, function (err, data) {
            if (err) {
                res.render("error")
            } else {
                res.send({res: "success"});
            }

        })
})

router.post('/backToInit', bodyParser.json(), function (req, res, next) {

    action.back(req.body.table, req.body.assignid, req.body.userid, req.body.procInstId, req.body.preArrprovId, req.body.remark,
        req.body.msg, req.body.msgName, req.body.keyValue, function (err, data) {
            if (err) {
                res.render("error")
            } else {
                res.send({res: "success"});
            }

        })
})


router.post('/contract/agree', bodyParser.json(), function (req, res, next) {
    contract.agree(req.body.wfId, req.body.nodeId, req.body.key, req.body.remark, req.body.tableName, function (err, data) {
        if (err) {
            logger.info("---- will render ----");
            res.send({res: false, msg: err.message});
        } else {
            res.send({res: true});
        }
    })

});

router.post('/contract/backToInit', bodyParser.json(), function (req, res, next) {
    contract.back(req.body.wfId, req.body.nodeId, req.body.key, req.body.remark, req.body.tableName, function (err, data) {
        if (err) {
            res.render("error")
        } else {
            res.send({res: "success"});
        }
    })
});


module.exports = router;