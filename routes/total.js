var express = require('express');
var router = express.Router();

var _ = require('lodash');

var conf = require('../config/config');
var cmoment = require('moment');
var util = require('../libs/util');
var sql = require('mssql');
var request = require('sync-request');
var token = require('../libs/token');
var Id = require('../libs/Id');
var Promise = require('bluebird');
var logger = require('../config/log4js').getLogger('total');

/* GET home page. */
router.get('/:empid/:type', function (req, res, next) {

    logger.info("empid : " + req.params.empid);

    var phoneNbr = null;
    var pool = null;
    var htPool = null;
    var type = req.params.type;

    var complete = 0;
    var oldData = null;
    var cb = function (err, data) {
        logger.info("--------------- called : " + JSON.stringify(data));
        complete++;
        if (complete == 1) {
            oldData = data;
        }
        if (complete == 2) {

            logger.info("-------------------------------------------------------------------------");
            logger.info({
                title: '浩源审核',
                wait: {
                    total: oldData.wait.total + data.wait.total,
                    hisTotal: oldData.wait.hisTotal + data.wait.hisTotal,
                    list: _.concat(oldData.wait.list, data.wait.list),
                    his: _.concat(oldData.wait.his, data.wait.his)
                }
            });

            res.render('total', {
                title: '浩源审核',
                wait: {
                    total: oldData.wait.total + data.wait.total,
                    hisTotal: oldData.wait.hisTotal + data.wait.hisTotal,
                    list: _.concat(oldData.wait.list, data.wait.list),
                    his: _.concat(oldData.wait.his, data.wait.his)
                }
            });
        }
    }

    var back = request('GET', 'https://oapi.dingtalk.com/user/get?access_token=' + token.token()
        + '&userid=' + req.params.empid);
    try {
        var mobileJson = JSON.parse(back.getBody('utf8'));
        phoneNbr = mobileJson.mobile;

        logger.info("mobileJson : " + JSON.stringify(mobileJson));
        logger.info("phonenbr : " + phoneNbr);

        if (!phoneNbr || phoneNbr.length < 2) {
            res.render('error', {
                message: '出错啦,没找到对应的电话号码',
                error: {
                    status: "",
                    stack: ""
                }
            });
            return;
        }

        pool = new sql.ConnectionPool({
            user: conf.db.username,
            password: conf.db.password,
            server: conf.db.hostname, // You can use 'localhost\\instance' to connect to named instance
            database: conf.db.database
        }, err => {
            logger.info("connect err : " + err);
            pool.request()
                .input('phone', sql.NVarChar, phoneNbr)
                .input('acct', sql.NVarChar, req.params.empid)
                .query(' select FID from T_DIN_ACCT where FPHONE = @phone ', (err, result) => {

                    logger.info(err);
                    logger.info("find ding ding account reustl : " + JSON.stringify(result));

                    if (result.recordset.length == 0) {
                        pool.request()
                            .input('id', sql.NVarChar, Date.now() + Id.get())
                            .input('phone', sql.NVarChar, phoneNbr)
                            .input('acct', sql.NVarChar, req.params.empid)
                            .query('insert into T_DIN_ACCT values (@id, @phone,@acct)', (err, result) => {
                                if (err) {
                                    logger.info("insert dingding acct error" + err);
                                }
                                logger.info(JSON.stringify(result));
                                pool.close();
                                queryList(cb);
                                queryHTList(cb);
                            });
                    }
                    else {
                        pool.close();
                        queryList(cb);
                        queryHTList(cb);
                    }

                });

        });


    } catch (e) {
        res.render('error', {
            message: '出错啦',
            error: {
                status: "",
                stack: ""
            }
        });
    }

    var queryList = function (cb) {
        logger.info("cb : " + cb);
        pool = new sql.ConnectionPool({
            user: conf.db.username,
            password: conf.db.password,
            server: conf.db.hostname, // You can use 'localhost\\instance' to connect to named instance
            database: conf.db.database
        }, err => {
            logger.info("----------- phoneNbr : " + phoneNbr);

            console.log("++++++=====    select c.FASSIGNID, d.FNAME, c.FCREATETIME, b.FTITLE, e.FOBJECTTYPEID, e.FKEYVALUE " +
                ", b.FRECEIVERID, c.FPROCINSTID, a.FPHONE from " +
                "T_SEC_USER a, T_WF_RECEIVER b, T_WF_ASSIGN c, T_SEC_USER d, T_WF_APPROVALASSIGN e " +
                "where a.FUSERID = b.FRECEIVERID and b.FASSIGNID = c.FASSIGNID and c.FSENDERID = d.FUSERID " +
                "and c.FASSIGNID = e.FASSIGNID and c.FSTATUS = 0 and a.FPHONE = @phone and e.FOBJECTTYPEID = '" + type + "'");

            var querys = [];
            querys.push(pool.request()
                .input('phone', sql.NVarChar, phoneNbr)
                .query("select c.FASSIGNID, d.FNAME, c.FCREATETIME, b.FTITLE, e.FOBJECTTYPEID, e.FKEYVALUE " +
                    ", b.FRECEIVERID, c.FPROCINSTID, a.FPHONE from " +
                    "T_SEC_USER a, T_WF_RECEIVER b, T_WF_ASSIGN c, T_SEC_USER d, T_WF_APPROVALASSIGN e " +
                    "where a.FUSERID = b.FRECEIVERID and b.FASSIGNID = c.FASSIGNID and c.FSENDERID = d.FUSERID " +
                    "and c.FASSIGNID = e.FASSIGNID and c.FSTATUS = 0 and a.FPHONE = @phone and e.FOBJECTTYPEID = '" + type + "'"));
            querys.push(pool.request()
                .input('phone', sql.NVarChar, phoneNbr)
                .query("select top 10 * from (select c.FASSIGNID, d.FNAME, c.FCREATETIME, b.FTITLE, e.FOBJECTTYPEID, e.FKEYVALUE, " +
                    "b.FRECEIVERID, c.FPROCINSTID, a.FPHONE, f.FRESULTNAME, ROW_NUMBER() over (partition by c.FPROCINSTID order " +
                    "by c.FPROCINSTID, c.FCREATETIME desc ) as nbr from T_SEC_USER a, T_WF_RECEIVER b, T_WF_ASSIGN c, T_SEC_USER d, " +
                    "T_WF_APPROVALASSIGN e, T_WF_APPROVALASSIGN_L f where a.FUSERID = b.FRECEIVERID and b.FASSIGNID = c.FASSIGNID " +
                    "and c.FSENDERID = d.FUSERID and c.FASSIGNID = e.FASSIGNID and c.FSTATUS = 1 and a.FPHONE = @phone and e.FOBJECTTYPEID = '" + type + "' " +
                    "and e.FAPPROVALASSIGNID = f.FAPPROVALASSIGNID and f.FLOCALEID = 2052) as bb " +
                    "where bb.nbr = 1 order by bb.FCREATETIME desc"));

            logger.info("total sql: " + 'select top 5 * from (select c.FASSIGNID, d.FNAME, c.FCREATETIME, b.FTITLE, e.FOBJECTTYPEID, e.FKEYVALUE, ' +
                'b.FRECEIVERID, c.FPROCINSTID, a.FPHONE, f.FRESULTNAME, ROW_NUMBER() over (partition by c.FPROCINSTID order ' +
                'by c.FPROCINSTID, c.FCREATETIME desc ) as nbr from T_SEC_USER a, T_WF_RECEIVER b, T_WF_ASSIGN c, T_SEC_USER d, ' +
                'T_WF_APPROVALASSIGN e, T_WF_APPROVALASSIGN_L f where a.FUSERID = b.FRECEIVERID and b.FASSIGNID = c.FASSIGNID ' +
                'and c.FSENDERID = d.FUSERID and c.FASSIGNID = e.FASSIGNID and c.FSTATUS = 1 and a.FPHONE = @phone ' +
                'and e.FAPPROVALASSIGNID = f.FAPPROVALASSIGNID and f.FLOCALEID = 2052) as bb ' +
                'where bb.nbr = 1 order by bb.FCREATETIME desc')

            Promise.all(querys).then(allResult => {
                logger.info("allResult : " + JSON.stringify(allResult));
                let showList = [];
                let recordSets = allResult[0].recordset;
                for (var i in recordSets) {

                    let createTime = cmoment(recordSets[i].FCREATETIME).utcOffset("-00:00");
                    let usedTimeMinutes = (cmoment().utcOffset("-00:00").diff(createTime) / (1000 * 60)) + (8 * 60);
                    logger.info(usedTimeMinutes);
                    let usedHours = parseInt(usedTimeMinutes / 60);
                    let usedMins = parseInt(usedTimeMinutes % 60);

                    showList.push({
                        itemDesc: recordSets[i].FTITLE,
                        applyName: recordSets[i].FNAME,
                        textList: ["发起时间 : " + createTime.format('YYYY-MM-DD HH:mm:ss')],
                        status: "待审批",
                        objtypeid: recordSets[i].FOBJECTTYPEID,
                        objkey: recordSets[i].FASSIGNID,
                        userid: recordSets[i].FRECEIVERID,
                        phonenbr : phoneNbr,
                        procinstId: recordSets[i].FPROCINSTID,
                        dingdingId: recordSets[i].FDINACT,
                        usedTime: usedHours + "时:" + usedMins + "分"
                    })
                }

                //处理历史数据
                let hisList = [];
                let hisRecordSets = allResult[1].recordset;
                for (var i in hisRecordSets) {

                    let hisCreateTime = cmoment(hisRecordSets[i].FCREATETIME).utcOffset("-00:00");
                    let hisUsedTimeMinutes = (cmoment().utcOffset("-00:00").diff(hisCreateTime) / (1000 * 60)) + (8 * 60);
                    logger.info(hisUsedTimeMinutes);
                    let hisUsedHours = parseInt(hisUsedTimeMinutes / 60);
                    let hisUsedMins = parseInt(hisUsedTimeMinutes % 60);

                    hisList.push({
                        itemDesc: hisRecordSets[i].FTITLE,
                        applyName: hisRecordSets[i].FNAME,
                        textList: ["发起时间 : " + hisCreateTime.format('YYYY-MM-DD HH:mm:ss')],
                        status: hisRecordSets[i].FRESULTNAME,
                        objtypeid: hisRecordSets[i].FOBJECTTYPEID,
                        objkey: hisRecordSets[i].FASSIGNID,
                        userid: hisRecordSets[i].FRECEIVERID,
                        procinstId: hisRecordSets[i].FPROCINSTID,
                        dingdingId: hisRecordSets[i].FDINACT,
                        usedTime: hisUsedHours + "时:" + hisUsedMins + "分"
                    })
                }

                pool.close();

                cb(null, {
                    wait: {
                        total: allResult[0].rowsAffected[0],
                        hisTotal: allResult[1].rowsAffected[0],
                        list: showList,
                        his: hisList
                    }
                });
            });
        })
    }

    pool.on('error', err => {
        logger.info(err);
        pool.close();
    });


    var queryHTList = function (cb) {
        htPool = new sql.ConnectionPool({
            user: conf.htdb.username,
            password: conf.htdb.password,
            server: conf.htdb.hostname, // You can use 'localhost\\instance' to connect to named instance
            database: conf.htdb.database
        }, err => {
            logger.info("-----ht list------ phoneNbr : " + phoneNbr);

            var querys = [];
            querys.push(htPool.request()
                .input('phone', sql.NVarChar, phoneNbr)
                .query("select a.WF_ID, d.NAME, c.WF_NAME, a.CREATE_TIME, a.ACTOR_ID, a.BUSINESS_TYPE " +
                    "from T_SYS_WF_TASK a , T_SYS_USERINFO b, T_SYS_WF c, T_SYS_USERINFO d " +
                    "where a.WF_ID = c.WF_ID and a.ACTOR_ID = b.ORG_ID and a.STATE != 4 and b.MOBILE = @phone " +
                    "and c.INITIATOR_ID = d.ORG_ID and a.BUSINESS_TYPE = '" + type + "' order by a.CREATE_TIME desc"));
            querys.push(htPool.request()
                .input('phone', sql.NVarChar, phoneNbr)
                .query("select top 5 a.WF_ID, d.NAME, c.WF_NAME, a.CREATE_TIME, a.ACTOR_ID , a.BUSINESS_TYPE" +
                    " from T_SYS_WF_TASK a , T_SYS_USERINFO b, T_SYS_WF c, T_SYS_USERINFO d " +
                    "where a.WF_ID = c.WF_ID and a.ACTOR_ID = b.ORG_ID and a.STATE = 4 and b.MOBILE = @phone " +
                    "and c.INITIATOR_ID = d.ORG_ID and a.BUSINESS_TYPE = '" + type + "' order by a.CREATE_TIME desc"));
            querys.push(htPool.request()
                .query("insert into T_DIN_ACCT select '" + Date.now() + Id.get() + "','" + phoneNbr + "','" + req.params.empid +
                    "' from T_SYS_USERINFO where not exists " +
                    "(select 1 from T_DIN_ACCT where  FPHONE = '" + phoneNbr + "') and MOBILE = '" + phoneNbr + "'"));

            logger.info("Kkkkkkkk total sql: " + 'select a.WF_ID, d.NAME, c.WF_NAME, a.CREATE_TIME, a.ACTOR_ID ' +
                'from T_SYS_WF_TASK a , T_SYS_USERINFO b, T_SYS_WF c, T_SYS_USERINFO d ' +
                'where a.WF_ID = c.WF_ID and a.ACTOR_ID = b.ORG_ID and a.STATE != 4 and b.MOBILE = @phone ' +
                'and c.INITIATOR_ID = d.ORG_ID order by a.CREATE_TIME desc');

            logger.info("insert into T_DIN_ACCT select '" + Date.now() + Id.get() + "','" + phoneNbr + "','" + req.params.empid +
                "' from T_SYS_USERINFO where not exists " +
                "(select 1 from T_DIN_ACCT where  FPHONE = '" + phoneNbr + "') and MOBILE = '" + phoneNbr + "'")

            Promise.all(querys).then(allResult => {
                logger.info("all ht Result : " + JSON.stringify(allResult));
                let showList = [];
                let recordSets = allResult[0].recordset;
                for (var i in recordSets) {

                    let createTime = cmoment(recordSets[i].CREATE_TIME).utcOffset("-00:00");
                    let usedTimeMinutes = (cmoment().utcOffset("-00:00").diff(createTime) / (1000 * 60)) + (8 * 60);
                    logger.info(usedTimeMinutes);
                    let usedHours = parseInt(usedTimeMinutes / 60);
                    let usedMins = parseInt(usedTimeMinutes % 60);

                    showList.push({
                        itemDesc: recordSets[i].WF_NAME,
                        applyName: recordSets[i].NAME,
                        textList: ["发起时间 : " + createTime.format('YYYY-MM-DD HH:mm:ss')],
                        status: "待审批",
                        objtypeid: recordSets[i].BUSINESS_TYPE.trim(),
                        objkey: recordSets[i].WF_ID,
                        userid: recordSets[i].ACTOR_ID,
                        procinstId: recordSets[i].WF_ID,
                        dingdingId: recordSets[i].FDINACT,
                        usedTime: usedHours + "时:" + usedMins + "分"
                    })
                }

                //处理历史数据
                let hisList = [];
                let hisRecordSets = allResult[1].recordset;
                for (var i in hisRecordSets) {

                    let hisCreateTime = cmoment(hisRecordSets[i].CREATE_TIME).utcOffset("-00:00");
                    let hisUsedTimeMinutes = (cmoment().utcOffset("-00:00").diff(hisCreateTime) / (1000 * 60)) + (8 * 60);
                    logger.info(hisUsedTimeMinutes);
                    let hisUsedHours = parseInt(hisUsedTimeMinutes / 60);
                    let hisUsedMins = parseInt(hisUsedTimeMinutes % 60);

                    hisList.push({
                        itemDesc: hisRecordSets[i].WF_NAME,
                        applyName: hisRecordSets[i].NAME,
                        textList: ["发起时间 : " + hisCreateTime.format('YYYY-MM-DD HH:mm:ss')],
                        status: '',
                        objtypeid: hisRecordSets[i].BUSINESS_TYPE.trim(),
                        objkey: hisRecordSets[i].WF_ID,
                        userid: hisRecordSets[i].ACTOR_ID,
                        procinstId: hisRecordSets[i].WF_ID,
                        dingdingId: hisRecordSets[i].FDINACT,
                        usedTime: hisUsedHours + "时:" + hisUsedMins + "分"
                    })
                }


                htPool.close();


                cb(null, {
                    wait: {
                        total: allResult[0].rowsAffected[0],
                        hisTotal: allResult[1].rowsAffected[0],
                        list: showList,
                        his: hisList
                    }
                });
            });
        });

        htPool.on('error', err => {
            logger.info(err);
            pool.close();
        });
    }
});


router.get('/t/:empid/:type', function (req, res, next) {

    logger.info("empid : " + req.params.empid);

    var phoneNbr = req.params.empid;
    var pool = null;
    var htPool = null;
    var type = req.params.type;

    var complete = 0;
    var oldData = null;
    var cb = function (err, data) {
        logger.info("--------------- called : " + JSON.stringify(data));
        complete++;
        if (complete == 1) {
            oldData = data;
        }
        if (complete == 2) {

            logger.info("-------------------------------------------------------------------------");
            logger.info({
                title: '浩源审核',
                wait: {
                    total: oldData.wait.total + data.wait.total,
                    hisTotal: oldData.wait.hisTotal + data.wait.hisTotal,
                    list: _.concat(oldData.wait.list, data.wait.list),
                    his: _.concat(oldData.wait.his, data.wait.his)
                }
            });

            res.render('total', {
                title: '浩源审核',
                wait: {
                    total: oldData.wait.total + data.wait.total,
                    hisTotal: oldData.wait.hisTotal + data.wait.hisTotal,
                    list: _.concat(oldData.wait.list, data.wait.list),
                    his: _.concat(oldData.wait.his, data.wait.his)
                }
            });
        }
    }

        pool = new sql.ConnectionPool({
            user: conf.db.username,
            password: conf.db.password,
            server: conf.db.hostname, // You can use 'localhost\\instance' to connect to named instance
            database: conf.db.database
        }, err => {
            logger.info("connect err : " + err);
            pool.request()
                .input('phone', sql.NVarChar, phoneNbr)
                .input('acct', sql.NVarChar, req.params.empid)
                .query(' select FID from T_DIN_ACCT where FPHONE = @phone and FDINACT = @acct ', (err, result) => {

                    logger.info(err);
                        pool.close();
                        queryList(cb);
                        queryHTList(cb);

                });

        });

    var queryList = function (cb) {
        logger.info("cb : " + cb);
        pool = new sql.ConnectionPool({
            user: conf.db.username,
            password: conf.db.password,
            server: conf.db.hostname, // You can use 'localhost\\instance' to connect to named instance
            database: conf.db.database
        }, err => {
            logger.info("----------- phoneNbr : " + phoneNbr);

            console.log("++++++=====    select c.FASSIGNID, d.FNAME, c.FCREATETIME, b.FTITLE, e.FOBJECTTYPEID, e.FKEYVALUE " +
                ", b.FRECEIVERID, c.FPROCINSTID, a.FPHONE from " +
                "T_SEC_USER a, T_WF_RECEIVER b, T_WF_ASSIGN c, T_SEC_USER d, T_WF_APPROVALASSIGN e " +
                "where a.FUSERID = b.FRECEIVERID and b.FASSIGNID = c.FASSIGNID and c.FSENDERID = d.FUSERID " +
                "and c.FASSIGNID = e.FASSIGNID and c.FSTATUS = 0 and a.FPHONE = @phone and e.FOBJECTTYPEID = '" + type + "'");

            var querys = [];
            querys.push(pool.request()
                .input('phone', sql.NVarChar, phoneNbr)
                .query("select c.FASSIGNID, d.FNAME, c.FCREATETIME, b.FTITLE, e.FOBJECTTYPEID, e.FKEYVALUE " +
                    ", b.FRECEIVERID, c.FPROCINSTID, a.FPHONE from " +
                    "T_SEC_USER a, T_WF_RECEIVER b, T_WF_ASSIGN c, T_SEC_USER d, T_WF_APPROVALASSIGN e " +
                    "where a.FUSERID = b.FRECEIVERID and b.FASSIGNID = c.FASSIGNID and c.FSENDERID = d.FUSERID " +
                    "and c.FASSIGNID = e.FASSIGNID and c.FSTATUS = 0 and a.FPHONE = @phone and e.FOBJECTTYPEID = '" + type + "'"));
            querys.push(pool.request()
                .input('phone', sql.NVarChar, phoneNbr)
                .query("select top 10 * from (select c.FASSIGNID, d.FNAME, c.FCREATETIME, b.FTITLE, e.FOBJECTTYPEID, e.FKEYVALUE, " +
                    "b.FRECEIVERID, c.FPROCINSTID, a.FPHONE, f.FRESULTNAME, ROW_NUMBER() over (partition by c.FPROCINSTID order " +
                    "by c.FPROCINSTID, c.FCREATETIME desc ) as nbr from T_SEC_USER a, T_WF_RECEIVER b, T_WF_ASSIGN c, T_SEC_USER d, " +
                    "T_WF_APPROVALASSIGN e, T_WF_APPROVALASSIGN_L f where a.FUSERID = b.FRECEIVERID and b.FASSIGNID = c.FASSIGNID " +
                    "and c.FSENDERID = d.FUSERID and c.FASSIGNID = e.FASSIGNID and c.FSTATUS = 1 and a.FPHONE = @phone and e.FOBJECTTYPEID = '" + type + "' " +
                    "and e.FAPPROVALASSIGNID = f.FAPPROVALASSIGNID and f.FLOCALEID = 2052) as bb " +
                    "where bb.nbr = 1 order by bb.FCREATETIME desc"));

            logger.info("total sql: " + 'select top 5 * from (select c.FASSIGNID, d.FNAME, c.FCREATETIME, b.FTITLE, e.FOBJECTTYPEID, e.FKEYVALUE, ' +
                'b.FRECEIVERID, c.FPROCINSTID, a.FPHONE, f.FRESULTNAME, ROW_NUMBER() over (partition by c.FPROCINSTID order ' +
                'by c.FPROCINSTID, c.FCREATETIME desc ) as nbr from T_SEC_USER a, T_WF_RECEIVER b, T_WF_ASSIGN c, T_SEC_USER d, ' +
                'T_WF_APPROVALASSIGN e, T_WF_APPROVALASSIGN_L f where a.FUSERID = b.FRECEIVERID and b.FASSIGNID = c.FASSIGNID ' +
                'and c.FSENDERID = d.FUSERID and c.FASSIGNID = e.FASSIGNID and c.FSTATUS = 1 and a.FPHONE = @phone ' +
                'and e.FAPPROVALASSIGNID = f.FAPPROVALASSIGNID and f.FLOCALEID = 2052) as bb ' +
                'where bb.nbr = 1 order by bb.FCREATETIME desc')

            Promise.all(querys).then(allResult => {
                logger.info("allResult : " + JSON.stringify(allResult));
                let showList = [];
                let recordSets = allResult[0].recordset;
                for (var i in recordSets) {

                    let createTime = cmoment(recordSets[i].FCREATETIME).utcOffset("-00:00");
                    let usedTimeMinutes = (cmoment().utcOffset("-00:00").diff(createTime) / (1000 * 60)) + (8 * 60);
                    logger.info(usedTimeMinutes);
                    let usedHours = parseInt(usedTimeMinutes / 60);
                    let usedMins = parseInt(usedTimeMinutes % 60);

                    showList.push({
                        itemDesc: recordSets[i].FTITLE,
                        applyName: recordSets[i].FNAME,
                        textList: ["发起时间 : " + createTime.format('YYYY-MM-DD HH:mm:ss')],
                        status: "待审批",
                        objtypeid: recordSets[i].FOBJECTTYPEID,
                        objkey: recordSets[i].FASSIGNID,
                        userid: recordSets[i].FRECEIVERID,
                        phonenbr : phoneNbr,
                        procinstId: recordSets[i].FPROCINSTID,
                        dingdingId: recordSets[i].FDINACT,
                        usedTime: usedHours + "时:" + usedMins + "分"
                    })
                }

                //处理历史数据
                let hisList = [];
                let hisRecordSets = allResult[1].recordset;
                for (var i in hisRecordSets) {

                    let hisCreateTime = cmoment(hisRecordSets[i].FCREATETIME).utcOffset("-00:00");
                    let hisUsedTimeMinutes = (cmoment().utcOffset("-00:00").diff(hisCreateTime) / (1000 * 60)) + (8 * 60);
                    logger.info(hisUsedTimeMinutes);
                    let hisUsedHours = parseInt(hisUsedTimeMinutes / 60);
                    let hisUsedMins = parseInt(hisUsedTimeMinutes % 60);

                    hisList.push({
                        itemDesc: hisRecordSets[i].FTITLE,
                        applyName: hisRecordSets[i].FNAME,
                        textList: ["发起时间 : " + hisCreateTime.format('YYYY-MM-DD HH:mm:ss')],
                        status: hisRecordSets[i].FRESULTNAME,
                        objtypeid: hisRecordSets[i].FOBJECTTYPEID,
                        objkey: hisRecordSets[i].FASSIGNID,
                        userid: hisRecordSets[i].FRECEIVERID,
                        procinstId: hisRecordSets[i].FPROCINSTID,
                        dingdingId: hisRecordSets[i].FDINACT,
                        usedTime: hisUsedHours + "时:" + hisUsedMins + "分"
                    })
                }

                pool.close();

                cb(null, {
                    wait: {
                        total: allResult[0].rowsAffected[0],
                        hisTotal: allResult[1].rowsAffected[0],
                        list: showList,
                        his: hisList
                    }
                });
            });
        })
    }

    pool.on('error', err => {
        logger.info(err);
        pool.close();
    });


    var queryHTList = function (cb) {
        htPool = new sql.ConnectionPool({
            user: conf.htdb.username,
            password: conf.htdb.password,
            server: conf.htdb.hostname, // You can use 'localhost\\instance' to connect to named instance
            database: conf.htdb.database
        }, err => {
            logger.info("-----ht list------ phoneNbr : " + phoneNbr + htPool);

            var querys = [];
            querys.push(htPool.request()
                .input('phone', sql.NVarChar, phoneNbr)
                .query("select a.WF_ID, d.NAME, c.WF_NAME, a.CREATE_TIME, a.ACTOR_ID, a.BUSINESS_TYPE " +
                    "from T_SYS_WF_TASK a , T_SYS_USERINFO b, T_SYS_WF c, T_SYS_USERINFO d " +
                    "where a.WF_ID = c.WF_ID and a.ACTOR_ID = b.ORG_ID and a.STATE != 4 and b.MOBILE = @phone " +
                    "and c.INITIATOR_ID = d.ORG_ID and a.BUSINESS_TYPE = '" + type + "' order by a.CREATE_TIME desc"));
            querys.push(htPool.request()
                .input('phone', sql.NVarChar, phoneNbr)
                .query("select top 5 a.WF_ID, d.NAME, c.WF_NAME, a.CREATE_TIME, a.ACTOR_ID , a.BUSINESS_TYPE" +
                    " from T_SYS_WF_TASK a , T_SYS_USERINFO b, T_SYS_WF c, T_SYS_USERINFO d " +
                    "where a.WF_ID = c.WF_ID and a.ACTOR_ID = b.ORG_ID and a.STATE = 4 and b.MOBILE = @phone " +
                    "and c.INITIATOR_ID = d.ORG_ID and a.BUSINESS_TYPE = '" + type + "' order by a.CREATE_TIME desc"));
            querys.push(htPool.request()
                .query("insert into T_DIN_ACCT select '" + Date.now() + Id.get() + "','" + phoneNbr + "','" + req.params.empid +
                    "' from T_SYS_USERINFO where not exists " +
                    "(select 1 from T_DIN_ACCT where  FPHONE = '" + phoneNbr + "') and MOBILE = '" + phoneNbr + "'"));

            logger.info("Kkkkkkkk total sql: " + 'select a.WF_ID, d.NAME, c.WF_NAME, a.CREATE_TIME, a.ACTOR_ID ' +
                'from T_SYS_WF_TASK a , T_SYS_USERINFO b, T_SYS_WF c, T_SYS_USERINFO d ' +
                'where a.WF_ID = c.WF_ID and a.ACTOR_ID = b.ORG_ID and a.STATE != 4 and b.MOBILE = @phone ' +
                'and c.INITIATOR_ID = d.ORG_ID order by a.CREATE_TIME desc');

            logger.info("insert into T_DIN_ACCT select '" + Date.now() + Id.get() + "','" + phoneNbr + "','" + req.params.empid +
                "' from T_SYS_USERINFO where not exists " +
                "(select 1 from T_DIN_ACCT where  FPHONE = '" + phoneNbr + "') and MOBILE = '" + phoneNbr + "'")
            logger.info(htPool);
            Promise.all(querys).then(allResult => {
                logger.info("all ht Result : " + JSON.stringify(allResult));
                let showList = [];
                let recordSets = allResult[0].recordset;
                for (var i in recordSets) {

                    let createTime = cmoment(recordSets[i].CREATE_TIME).utcOffset("-00:00");
                    let usedTimeMinutes = (cmoment().utcOffset("-00:00").diff(createTime) / (1000 * 60)) + (8 * 60);
                    logger.info(usedTimeMinutes);
                    let usedHours = parseInt(usedTimeMinutes / 60);
                    let usedMins = parseInt(usedTimeMinutes % 60);

                    showList.push({
                        itemDesc: recordSets[i].WF_NAME,
                        applyName: recordSets[i].NAME,
                        textList: ["发起时间 : " + createTime.format('YYYY-MM-DD HH:mm:ss')],
                        status: "待审批",
                        objtypeid: recordSets[i].BUSINESS_TYPE.trim(),
                        objkey: recordSets[i].WF_ID,
                        userid: recordSets[i].ACTOR_ID,
                        procinstId: recordSets[i].WF_ID,
                        dingdingId: recordSets[i].FDINACT,
                        usedTime: usedHours + "时:" + usedMins + "分"
                    })
                }

                //处理历史数据
                let hisList = [];
                let hisRecordSets = allResult[1].recordset;
                for (var i in hisRecordSets) {

                    let hisCreateTime = cmoment(hisRecordSets[i].CREATE_TIME).utcOffset("-00:00");
                    let hisUsedTimeMinutes = (cmoment().utcOffset("-00:00").diff(hisCreateTime) / (1000 * 60)) + (8 * 60);
                    logger.info(hisUsedTimeMinutes);
                    let hisUsedHours = parseInt(hisUsedTimeMinutes / 60);
                    let hisUsedMins = parseInt(hisUsedTimeMinutes % 60);

                    hisList.push({
                        itemDesc: hisRecordSets[i].WF_NAME,
                        applyName: hisRecordSets[i].NAME,
                        textList: ["发起时间 : " + hisCreateTime.format('YYYY-MM-DD HH:mm:ss')],
                        status: '',
                        objtypeid: hisRecordSets[i].BUSINESS_TYPE.trim(),
                        objkey: hisRecordSets[i].WF_ID,
                        userid: hisRecordSets[i].ACTOR_ID,
                        procinstId: hisRecordSets[i].WF_ID,
                        dingdingId: hisRecordSets[i].FDINACT,
                        usedTime: hisUsedHours + "时:" + hisUsedMins + "分"
                    })
                }


                htPool.close();


                cb(null, {
                    wait: {
                        total: allResult[0].rowsAffected[0],
                        hisTotal: allResult[1].rowsAffected[0],
                        list: showList,
                        his: hisList
                    }
                });
            });
        });

        htPool.on('error', err => {
            logger.info("--- ht pool error ---");
            logger.info(err);
            pool.close();
        });
    }
});



module.exports = router;
