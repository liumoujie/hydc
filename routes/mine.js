var express = require('express');
var router = express.Router();

var conf = require('../config/config');
var cmoment = require('moment');
var util = require('../libs/util');
var sql = require('mssql');
var request = require('sync-request');
var token = require('../libs/token');
var Id = require('../libs/Id');
var Promise = require('bluebird');
var logger = require('../config/log4js').getLogger('mine');

/* GET home page. */
router.get('/:empid/:type', function (req, res, next) {

    logger.info("empid : " + req.params.empid);

    var phoneNbr = null;
    var type = req.params.type;
    var pool = null;

    var back = request('GET', 'https://oapi.dingtalk.com/user/get?access_token=' + token.token()
        + '&userid=' + req.params.empid);
    try {
        var mobileJson = JSON.parse(back.getBody('utf8'));
        phoneNbr = mobileJson.mobile;

        logger.info("mobileJson : " + JSON.stringify(mobileJson));
        logger.info("phonenbr : " + phoneNbr);

        if (!phoneNbr || phoneNbr.length < 2) {
            res.render('error', {
                message: '出错啦,没找到对应的电话号码',
                error: {
                    status: "",
                    stack: ""
                }
            });
            return;
        }

        pool = new sql.ConnectionPool({
            user: conf.db.username,
            password: conf.db.password,
            server: conf.db.hostname, // You can use 'localhost\\instance' to connect to named instance
            database: conf.db.database
        }, err => {
            logger.info("connect err : " + err);
            pool.request()
                .input('phone', sql.NVarChar, phoneNbr)
                .input('acct', sql.NVarChar, req.params.empid)
                .query(' select FID from T_DIN_ACCT where FPHONE = @phone ', (err, result) => {

                    logger.info(err);
                    logger.info("find ding ding account reustl : " + JSON.stringify(result));

                    if (result.recordset.length == 0) {
                        pool.request()
                            .input('id', sql.NVarChar, Date.now() + Id.get())
                            .input('phone', sql.NVarChar, phoneNbr)
                            .input('acct', sql.NVarChar, req.params.empid)
                            .query('insert into T_DIN_ACCT values (@id, @phone,@acct)', (err, result) => {
                                if (err) {
                                    logger.info("insert dingding acct error" + err);
                                }
                                logger.info(JSON.stringify(result));
                                pool.close();
                                logger.info("---- will call queryList -----");
                                queryList();
                            });
                    }
                    else {
                        pool.close();
                        logger.info("---- will call --in else-- queryList -----");
                        queryList();
                    }

                });

        });


    } catch (e) {
        res.render('error', {
            message: '出错啦',
            error: {
                status: "",
                stack: ""
            }
        });
    }

    var queryList = function () {
        pool = new sql.ConnectionPool({
            user: conf.db.username,
            password: conf.db.password,
            server: conf.db.hostname, // You can use 'localhost\\instance' to connect to named instance
            database: conf.db.database
        }, err => {
            logger.info("----------- abc phoneNbr : " + phoneNbr);

            logger.info("select top 10 bb.* from (select a.FPROCINSTID, a.FASSIGNID, b.FTITLE, a.FRECEIVERNAMES, a.FSTATUS, " +
                "a.FCREATETIME, e.FOBJECTTYPEID,ROW_NUMBER() over (partition by a.FPROCINSTID order by a.FPROCINSTID, " +
                "a.FCREATETIME desc) as nbr from T_WF_ASSIGN a , T_WF_RECEIVER b, T_WF_PROCINST c, T_SEC_USER d, " +
                "T_WF_APPROVALASSIGN e where a.FASSIGNID = b.FASSIGNID and a.FPROCINSTID = c.FPROCINSTID and a.FASSIGNID = " +
                "e.FASSIGNID and c.FORIGINATORID = d.FUSERID and d.FPHONE = @phone and e.FOBJECTTYPEID = '" + type + "') " +
                "as bb where  bb.nbr = 1 order by bb.FCREATETIME desc");
            var querys = [];
            querys.push(pool.request()
                .input('phone', sql.NVarChar, phoneNbr)
                .query("select top 10 bb.* from (select a.FPROCINSTID, a.FASSIGNID, b.FTITLE, a.FRECEIVERNAMES, a.FSTATUS, " +
                    "a.FCREATETIME, e.FOBJECTTYPEID,ROW_NUMBER() over (partition by a.FPROCINSTID order by a.FPROCINSTID, " +
                    "a.FCREATETIME desc) as nbr from T_WF_ASSIGN a , T_WF_RECEIVER b, T_WF_PROCINST c, T_SEC_USER d, " +
                    "T_WF_APPROVALASSIGN e where a.FASSIGNID = b.FASSIGNID and a.FPROCINSTID = c.FPROCINSTID and a.FASSIGNID = " +
                    "e.FASSIGNID and c.FORIGINATORID = d.FUSERID and d.FPHONE = @phone and e.FOBJECTTYPEID = '" + type + "') " +
                    "as bb where  bb.nbr = 1 order by bb.FCREATETIME desc"));

            Promise.all(querys).then(allResult => {
                logger.info("allResult : " + JSON.stringify(allResult));
                let showList = [];
                let recordSets = allResult[0].recordset;
                for (var i in recordSets) {

                    let createTime = cmoment(recordSets[i].FCREATETIME).utcOffset("-00:00");
                    let usedTimeMinutes = (cmoment().utcOffset("-00:00").diff(createTime) / (1000 * 60)) + (8 * 60);
                    logger.info(usedTimeMinutes);
                    let usedHours = parseInt(usedTimeMinutes / 60);
                    let usedMins = parseInt(usedTimeMinutes % 60);

                    showList.push({
                        itemDesc: recordSets[i].FTITLE,
                        applyName: recordSets[i].FRECEIVERNAMES,
                        textList: ["发起时间 : " + createTime.format('YYYY-MM-DD HH:mm:ss')],
                        status: recordSets[i].FSTATUS == "1" ? "审批通过" : "待审批",
                        objtypeid: recordSets[i].FOBJECTTYPEID,
                        objkey: recordSets[i].FASSIGNID,
                        userid: recordSets[i].FRECEIVERID,
                        procinstId: recordSets[i].FPROCINSTID,
                        dingdingId: recordSets[i].FDINACT,
                        usedTime: usedHours + "时:" + usedMins + "分"
                    })
                }

                pool.close();
                res.render('mine', {
                    title: '浩源审核',
                    wait: {
                        total: allResult[0].rowsAffected[0],
                        list: showList
                    }
                });
            });
        })
    }

    pool.on('error', err => {
        logger.info(err);
        pool.close();
    });

});

router.get('/t/:empid/:type', function (req, res, next) {

    logger.info("empid : " + req.params.empid);

    var phoneNbr = req.params.empid;
    var type = req.params.type;
    var pool = null;

    //var back = request('GET', 'https://oapi.dingtalk.com/user/get?access_token=' + token.token()
    //    + '&userid=' + req.params.empid);
    try {

        logger.info("---- test mine phonenbr : " + phoneNbr);

        if (!phoneNbr || phoneNbr.length < 2) {
            res.render('error', {
                message: '出错啦,没找到对应的电话号码',
                error: {
                    status: "",
                    stack: ""
                }
            });
            return;
        }

        pool = new sql.ConnectionPool({
            user: conf.db.username,
            password: conf.db.password,
            server: conf.db.hostname, // You can use 'localhost\\instance' to connect to named instance
            database: conf.db.database
        }, err => {
            logger.info("connect err : " + err);
            pool.request()
                .input('phone', sql.NVarChar, phoneNbr)
                .input('acct', sql.NVarChar, req.params.empid)
                .query(' select FID from T_DIN_ACCT where FPHONE = @phone ', (err, result) => {

                    logger.info(err);
                    logger.info("find ding ding account reustl : " + JSON.stringify(result));

                    if (result.recordset.length == 0) {
                        pool.request()
                            .input('id', sql.NVarChar, Date.now() + Id.get())
                            .input('phone', sql.NVarChar, phoneNbr)
                            .input('acct', sql.NVarChar, req.params.empid)
                            .query('insert into T_DIN_ACCT values (@id, @phone,@acct)', (err, result) => {
                                if (err) {
                                    logger.info("insert dingding acct error" + err);
                                }
                                logger.info(JSON.stringify(result));
                                pool.close();
                                logger.info("---- will call queryList -----");
                                if (type == "t_con_contract" ||
                                    type == "t_con_pay_apply" ||
                                    type == "t_con_change" ||
                                    type == "t_con_balance") {
                                    queryHTList()
                                } else {
                                    queryList();
                                }
                            });
                    }
                    else {
                        pool.close();
                        logger.info("---- will call --in else-- queryList -----");
                        if (type == "t_con_contract" ||
                            type == "t_con_pay_apply" ||
                            type == "t_con_change" ||
                            type == "t_con_balance") {
                            queryHTList()
                        } else {
                            queryList();
                        }
                    }

                });

        });


    } catch (e) {
        res.render('error', {
            message: '出错啦',
            error: {
                status: "",
                stack: ""
            }
        });
    }

    var queryHTList = function () {
        pool = new sql.ConnectionPool({
            user: conf.htdb.username,
            password: conf.htdb.password,
            server: conf.htdb.hostname, // You can use 'localhost\\instance' to connect to named instance
            database: conf.htdb.database
        }, err => {
            logger.info("----------- abc phoneNbr : " + phoneNbr);

            logger.info("select top 10 a.WF_NAME, a.WF_ID, b.NAME, a.BUSINESS_TYPE, a.INITIATE_TIME, a.INITIATOR_ID from T_SYS_WF a, " +
                "T_SYS_USERINFO b where a.INITIATOR_ID = b.ORG_ID and b.MOBILE = @phone and a.BUSINESS_TYPE = '" + type +
                "' order by a.INITIATE_TIME desc");
            var querys = [];
            querys.push(pool.request()
                .input('phone', sql.NVarChar, phoneNbr)
                .query("select top 10 a.WF_NAME, a.WF_ID, b.NAME, a.BUSINESS_TYPE, a.INITIATE_TIME, a.INITIATOR_ID, a.STATE from T_SYS_WF a, " +
                    "T_SYS_USERINFO b where a.INITIATOR_ID = b.ORG_ID and b.MOBILE = @phone and a.BUSINESS_TYPE = '" + type +
                    "' order by a.INITIATE_TIME desc"));

            Promise.all(querys).then(allResult => {
                logger.info("my contrct detail : " + JSON.stringify(allResult));
                let showList = [];
                let recordSets = allResult[0].recordset;
                for (var i in recordSets) {

                    let createTime = cmoment(recordSets[i].INITIATE_TIME).utcOffset("-00:00");
                    let usedTimeMinutes = (cmoment().utcOffset("-00:00").diff(createTime) / (1000 * 60)) + (8 * 60);
                    logger.info(usedTimeMinutes);
                    let usedHours = parseInt(usedTimeMinutes / 60);
                    let usedMins = parseInt(usedTimeMinutes % 60);

                    showList.push({
                        itemDesc: recordSets[i].WF_NAME,
                        applyName: recordSets[i].NAME,
                        textList: ["发起时间 : " + createTime.format('YYYY-MM-DD HH:mm:ss')],
                        status: recordSets[i].STATE == "4" ? "审批通过" : "待审批",
                        objtypeid: type,
                        objkey: recordSets[i].WF_ID,
                        userid: recordSets[i].INITIATOR_ID,
                        //procinstId: recordSets[i].FPROCINSTID,
                        //dingdingId: recordSets[i].FDINACT,
                        usedTime: usedHours + "时:" + usedMins + "分"
                    })
                }

                pool.close(JSON.stringify({
                    total: allResult[0].rowsAffected[0],
                    list: showList
                }));
                logger.info()
                res.render('mine', {
                    title: '浩源审核',
                    wait: {
                        total: allResult[0].rowsAffected[0],
                        list: showList
                    }
                });
            });
        })
    }

    var queryList = function () {
        pool = new sql.ConnectionPool({
            user: conf.db.username,
            password: conf.db.password,
            server: conf.db.hostname, // You can use 'localhost\\instance' to connect to named instance
            database: conf.db.database
        }, err => {
            logger.info("----------- abc phoneNbr : " + phoneNbr);

            logger.info("select top 10 bb.* from (select a.FPROCINSTID, a.FASSIGNID, b.FTITLE, a.FRECEIVERNAMES, a.FSTATUS, " +
                "a.FCREATETIME, e.FOBJECTTYPEID,ROW_NUMBER() over (partition by a.FPROCINSTID order by a.FPROCINSTID, " +
                "a.FCREATETIME desc) as nbr from T_WF_ASSIGN a , T_WF_RECEIVER b, T_WF_PROCINST c, T_SEC_USER d, " +
                "T_WF_APPROVALASSIGN e where a.FASSIGNID = b.FASSIGNID and a.FPROCINSTID = c.FPROCINSTID and a.FASSIGNID = " +
                "e.FASSIGNID and c.FORIGINATORID = d.FUSERID and d.FPHONE = @phone and e.FOBJECTTYPEID = '" + type + "') " +
                "as bb where  bb.nbr = 1 order by bb.FCREATETIME desc");
            var querys = [];
            querys.push(pool.request()
                .input('phone', sql.NVarChar, phoneNbr)
                .query("select top 10 bb.* from (select a.FPROCINSTID, a.FASSIGNID, b.FTITLE, a.FRECEIVERNAMES, a.FSTATUS, " +
                    "a.FCREATETIME, e.FOBJECTTYPEID,ROW_NUMBER() over (partition by a.FPROCINSTID order by a.FPROCINSTID, " +
                    "a.FCREATETIME desc) as nbr from T_WF_ASSIGN a , T_WF_RECEIVER b, T_WF_PROCINST c, T_SEC_USER d, " +
                    "T_WF_APPROVALASSIGN e where a.FASSIGNID = b.FASSIGNID and a.FPROCINSTID = c.FPROCINSTID and a.FASSIGNID = " +
                    "e.FASSIGNID and c.FORIGINATORID = d.FUSERID and d.FPHONE = @phone and e.FOBJECTTYPEID = '" + type + "') " +
                    "as bb where  bb.nbr = 1 order by bb.FCREATETIME desc"));

            Promise.all(querys).then(allResult => {
                logger.info("allResult : " + JSON.stringify(allResult));
                let showList = [];
                let recordSets = allResult[0].recordset;
                for (var i in recordSets) {

                    let createTime = cmoment(recordSets[i].FCREATETIME).utcOffset("-00:00");
                    let usedTimeMinutes = (cmoment().utcOffset("-00:00").diff(createTime) / (1000 * 60)) + (8 * 60);
                    logger.info(usedTimeMinutes);
                    let usedHours = parseInt(usedTimeMinutes / 60);
                    let usedMins = parseInt(usedTimeMinutes % 60);

                    showList.push({
                        itemDesc: recordSets[i].FTITLE,
                        applyName: recordSets[i].FRECEIVERNAMES,
                        textList: ["发起时间 : " + createTime.format('YYYY-MM-DD HH:mm:ss')],
                        status: recordSets[i].FSTATUS == "1" ? "审批通过" : "待审批",
                        objtypeid: recordSets[i].FOBJECTTYPEID,
                        objkey: recordSets[i].FASSIGNID,
                        userid: recordSets[i].FRECEIVERID,
                        procinstId: recordSets[i].FPROCINSTID,
                        dingdingId: recordSets[i].FDINACT,
                        usedTime: usedHours + "时:" + usedMins + "分"
                    })
                }

                pool.close();
                res.render('mine', {
                    title: '浩源审核',
                    wait: {
                        total: allResult[0].rowsAffected[0],
                        list: showList
                    }
                });
            });
        })
    }

    pool.on('error', err => {
        logger.info(err);
        pool.close();
    });

});


module.exports = router;
