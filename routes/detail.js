var express = require('express');
var router = express.Router();

var conf = require('../config/config');
var token = require('../libs/token');
var util = require('../libs/util');
var detail = require('../service/queryDetail');

/* GET home page. */
router.get('/:table/:assignid/:phonenbr', function (req, res, next) {

    if (req.params.phonenbr == "only") {
        detail.queryDetails(req.params.table, req.params.assignid, "", true, function (err, data) {
            if (err) {
                res.render("error", err);
            } else {
                data.showSubmit = false;
                console.log("detail data : " + JSON.stringify(data));
                res.render('detail', data);
            }

        })
    } else {
        detail.queryDetails(req.params.table, req.params.assignid, req.params.phonenbr, false, function (err, data) {
            if (err) {
                if (err.error.status == 0) {
                    detail.queryDetails(req.params.table, req.params.assignid, "", true, function (err, data) {
                        if (err) {
                            res.render("error", err);
                        } else {
                            data.showSubmit = false;
                            console.log("detail data : " + JSON.stringify(data));
                            res.render('detail', data);
                        }

                    })
                } else {
                    err = {
                        message: "查询详情出错",
                        error: {
                            status: "错误",
                            status: "出错"
                        }
                    }
                    res.render("error", err);
                }
            } else {
                data.showSubmit = true;
                console.log("detail data : " + JSON.stringify(data));
                res.render('detail', data);
            }

        })
    }
});

//用于只给出显示界面
//router.get('/:table/:assignid/only', function (req, res, next) {
//
//    detail.queryDetails(req.params.table, req.params.assignid, "", true, function (err, data) {
//        if (err) {
//            res.render("error", err);
//        } else {
//            data.showSubmit = false;
//            console.log("detail data : " + JSON.stringify(data));
//            res.render('detail', data);
//        }
//
//    })
//});

module.exports = router;
