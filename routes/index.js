var express = require('express');
var router = express.Router();

var conf = require('../config/config');
var token = require('../libs/token');
var util = require('../libs/util');

/* GET home page. */
router.get('/', function (req, res, next) {

    var timeStamp = new Date().getTime();
    //var signedUrl = "http://" + conf.hostName + ":" + conf.port + "/";
    console.log(req.paths);
    var signedUrl = "http://" + conf.hostName + ":" + conf.port + "/?agentid=" + conf.agentId;
    var ticket = token.ticket();

    console.log("ticket in index : "+ticket);

    var signature = util({
        nonceStr: conf.nonceStr,
        timeStamp: timeStamp,
        url: signedUrl,
        ticket: ticket
    });

    res.render('index', {
        title: '浩源审核', conf: {
            agentId: conf.agentId,
            signature: signature,
            nonceStr: conf.nonceStr,
            timeStamp: timeStamp,
            corpId: conf.corpid
        }
    });
});

module.exports = router;
