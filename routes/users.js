var express = require('express');
var router = express.Router();

var token = require('../libs/token');
var request = require('sync-request');

var queryBq = require('../service/queryCloud');
var yfd = require('../service/auditCloudOrder');
var gys = require('../service/saveSupplier');
var fkd = require('../service/saveFksqd');
var saveContract = require('../service/saveContract');
var saveBgSqd = require('../service/saveBGsqd');
var saveBalanced = require('../service/saveBalanced');
var nbk = require('../service/noticeBack');
var sql = require('mssql');
var conf = require('../config/config');
var Id = require('../libs/Id');
var logger = require('../config/log4js').getLogger('users');
var cmoment = require('moment');

/* GET users listing. */
router.get('/', function (req, res, next) {
    res.send('respond with a resource');
});

router.get('/queryUserCnt', function (req, res, next) {
    var back = request('GET', 'https://oapi.dingtalk.com/user/get_org_user_count?access_token=' +
        token.token() + '&onlyActive=0');

    res.send(back.getBody('utf8'))
});


//https://oapi.dingtalk.com/user/getuserinfo?access_token=ACCESS_TOKEN&code=CODE
//manager6075

router.get('/userById', function (req, res, next) {
    console.log("--- token: " + token.token());
    console.log("--- ticket: " + token.ticket());
    var back = request('GET', 'https://oapi.dingtalk.com/user/getuserinfo?access_token=' + token.token()
        + '&code=dbe647f92cea3b60881492a8bb9e54a1');
    res.send(back.getBody('utf8'))
});

//通过userid获取人员详细信息
router.get('/userDetail/:uid', function (req, res, next) {
    console.log("--- token: " + token.token());
    console.log("--- ticket: " + token.ticket());
    var back = request('GET', 'https://oapi.dingtalk.com/user/get?access_token=' + token.token()
        + '&userid=' + req.param('uid'));
    res.send(back.getBody('utf8'))
});

router.get('/token', function (req, res, next) {
    res.send(token.token())
});

router.get('/ticket', function (req, res, next) {
    res.send(token.ticket())
});


router.get('/department', function (req, res, next) {
    console.log("--- token: " + token.token());
    console.log("--- ticket: " + token.ticket());
    var back = request('GET', 'https://oapi.dingtalk.com/department/list?access_token=' +
        token.token() + "&id=1");
    res.send(back.getBody('utf8'))
});

router.get('/userList/:depId', function (req, res, next) {
    console.log(req.param('depId'));
    var back = request('GET', 'https://oapi.dingtalk.com/user/simplelist?access_token=' +
        token.token() + '&department_id=' + req.param('depId'));
    res.send(back.getBody('utf8'))
});

router.get('/scope', function (req, res, next) {
    console.log(req.param('depId'));
    var back = request('GET', 'https://oapi.dingtalk.com/auth/scopes?access_token=' +
        token.token());
    res.send(back.getBody('utf8'))
});

router.get('/userDetList/:depId', function (req, res, next) {
    console.log(req.param('depId'));
    var back = request('GET', 'https://oapi.dingtalk.com/user/list?access_token=' +
        token.token() + '&department_id=' + req.param('depId'));
    res.send(back.getBody('utf8'))
});

router.get('/sendLinkMsg/:toUser', function (req, res, next) {
    //054536074021334237"},{"name":"刘谋杰修改","userid":"manager6075
    var back = "";
    console.log("-------------start-----------------");
    var createGroupBack = request('POST', 'https://oapi.dingtalk.com/message/send?access_token=' + token.token()
        , {
            json: {
                "touser": req.params.toUser,
                "agentid": "167476086",
                "msgtype": "link",
                "link": {
                    "messageUrl": "http://s.dingtalk.com/market/dingtalk/error_code.php",
                    "picUrl": "https://img.alicdn.com/tfs/TB1jiL0XwoQMeJjy0FnXXb8gFXa-538-213.jpg",
                    "title": "测试",
                    "text": "测试"
                }
            }
        });
    res.send(createGroupBack.getBody('utf8'));
});

router.get('/savefk', function (req, res, next) {
    //fksqd({"parameters": ["CN_PAYAPPLY", "{\"Model\":{\"FBILLTYPEID\":{\"FNumber\":\"GCLCGHTFKSQ\"},\"F_KLM_TEXT5\":\"HFJSMC-C04-2017-001\",\"F_KLM_TEXT\":\"this contract will pay 2000\",\"F_KLM_DATE1\":\"2017-10-26\",\"F_KLM_TEXT8\":\"杨晶晶\",\"F_KLM_AMOUNT5\":\"2000\",\"FDATE\":\"2017-10-26\",\"F_KLM_Text1\":\"华府金沙名城\",\"F_KLM_TEXT2\":\"testContact20171026\",\"F_KLM_Amount2\":\"10000\",\"F_KLM_Amount3\":\"2000\",\"FCONTACTUNITTYPE\":\"BD_Supplier\",\"FCONTACTUNIT\":{\"FNumber\":\"VEN00018\"},\"FRECTUNITTYPE\":\"BD_Supplier\",\"FRECTUNIT\":{\"FNumber\":\"VEN00018\"},\"FCURRENCYID\":{\"FNumber\":\"PRE001\"},\"FPAYORGID\":{\"FNumber\":\"100\"},\"FSETTLEORGID\":{\"FNumber\":\"100\"},\"FDEPARTMENT\":{\"FNumber\":\"BM000018\"},\"FDOCUMENTSTATUS\":\"B\",\"FScanPoint\":{\"FNumber\":\"\"},\"FCANCELSTATUS\":\"A\",\"F_KLM_Base\":{\"FNumber\":\"SFKYT015\"},\"F_KLM_Entity\":[{\"F_KLM_Date\":\"2017-10-26\",\"F_KLM_Amount\":\"2000\",\"F_KLM_Amount1\":\"2000\",\"F_KLM_Decimal\":\"0.2\",\"F_KLM_Text3\":\"dgsdhdfghsaf\",\"F_KLM_Text4\":\"325234535\"}],\"F_KLM_Entity1\":[{\"F_KLM_Text6\":\"杨晶晶\",\"F_KLM_Text7\":\"杨晶晶同意\",\"F_KLM_Time\":\"2017-10-26\"},{\"F_KLM_Text6\":\"刘健\",\"F_KLM_Text7\":\"刘健同意\",\"F_KLM_Time\":\"2017-10-26\"},{\"F_KLM_Text6\":\"唐华\",\"F_KLM_Text7\":\"唐华同意\",\"F_KLM_Time\":\"2017-10-26\"},{\"F_KLM_Text6\":\"张武格\",\"F_KLM_Text7\":\"张老师同意\",\"F_KLM_Time\":\"2017-10-26\"},{\"F_KLM_Text6\":\"陈琦\",\"F_KLM_Text7\":\"陈总同意\",\"F_KLM_Time\":\"2017-10-26\"},{\"F_KLM_Text6\":\"唐飞\",\"F_KLM_Text7\":\"唐飞同意\",\"F_KLM_Time\":\"2017-10-26\"},{\"F_KLM_Text6\":\"樊蓉\",\"F_KLM_Text7\":\"ty\",\"F_KLM_Time\":\"2017-10-30\"}],\"FPAYAPPLYENTRY\":[{\"FPAYPURPOSEID\":{\"FNumber\":\"SFKYT015\"},\"FENDDATE\":\"2017-11-01\",\"FEXPECTPAYDATE\":\"2017-11-01\",\"FAPPLYAMOUNTFOR\":\"2000\",\"FDescription\":\"Use Web Api Demo\"}]}}"]}
    //{"conName":"testContact20171026","conCode":"HFJSMC-C04-2017-001","conMoney":10000,"prjName":"华府金沙名城","venNbr":"VEN00018","approveRemark":"this contract will pay 2000","applyTime":"2017-10-26","applyMoney":2000,"unittype":"BD_Supplier","creator":"杨晶晶","paidList":[{"curPaid":2000,"curPrecent":0.2,"curPaidTime":"2017-10-26","curApply":2000,"curBnkNbr":"325234535","curBnkName":"dgsdhdfghsaf"}],"auditList":[{"actor":"杨晶晶","remark":"杨晶晶同意","time":"2017-10-26"},{"actor":"刘健","remark":"刘健同意","time":"2017-10-26"},{"actor":"唐华","remark":"唐华同意","time":"2017-10-26"},{"actor":"张武格","remark":"张老师同意","time":"2017-10-26"},{"actor":"陈琦","remark":"陈总同意","time":"2017-10-26"},{"actor":"唐飞","remark":"唐飞同意","time":"2017-10-26"},{"actor":"樊蓉","remark":"ty","time":"2017-10-30"}],"totalPaid":2000}

    fkd({
            "conName": "testContact20171026",
            "conCode": "HFJSMC-C04-2017-001",
            "conMoney": 10000,
            "prjName": "华府金沙名城",
            "venNbr": "VEN00014",
            "approveRemark": "第三方好地方国际化",
            "applyTime": "2017-10-30",
            "applyMoney": 5000,
            "unittype": "BD_Supplier",
            "creator": "杨晶晶",
            "paidList": [{
                "curPaid": 2000,
                "curPrecent": 0.2,
                "curPaidTime": "2017-10-26",
                "curApply": 2000,
                "curBnkNbr": "325234535",
                "curBnkName": "dgsdhdfghsaf"
            }, {
                "curPaid": 7000,
                "curPrecent": 0.7,
                "curPaidTime": "2017-10-30",
                "curApply": 5000,
                "curBnkNbr": "325234535",
                "curBnkName": "dgsdhdfghsaf"
            }],
            "auditList": [{"actor": "樊蓉", "remark": null, "time": "Invalid date"}, {
                "actor": "杨晶晶",
                "remark": "三",
                "time": "2017-10-30"
            }, {"actor": "刘健", "remark": "刘建", "time": "2017-10-30"}, {
                "actor": "唐华",
                "remark": "唐华",
                "time": "2017-10-30"
            }, {"actor": "张武格", "remark": "ZWG", "time": "2017-10-30"}, {
                "actor": "陈琦",
                "remark": "陈琦",
                "time": "2017-10-30"
            }, {"actor": "唐飞", "remark": "唐飞", "time": "2017-10-30"}],
            "totalPaid": 7000
        }


        , function (err, data) {
            if (err) {
                res.render("error", err);
            } else {

                res.send(data);
            }
        });
});

router.get('/saveContract', function (req, res, next) {
    //"\"F_KLM_Text10\":\"" + obj.htzynr + "\"," +   // 合同主要内容
    //"\"F_KLM_Text11\":\"" + obj.bz + "\"," +   // 备注
    //"\"F_KLM_Text9\":\"" + obj.jbr + "\"," +   // 经办人
    //"\"F_KLM_Decimal\":\"" + obj.htjk + "\"," +   // 合同价款
    //"\"F_KLM_Text3\":\"" + obj.xmmc + "\"," +   // 项目名称
    //"\"F_KLM_Text3\":\"" + obj.htbh + "\"," +    //  合同编号
    //"\"F_KLM_Text4\":\"" + obj.htmc + "\"," +    //  合同名称
    //"\"F_KLM_Text7\":\"" + obj.dfdw + "\"," +    //  对方单位
    //"\"F_KLM_Text8\":\"" + obj.jbbm + "\"," +    //  经办部门
    //"\"F_KLM_Text5\":\"" + obj.htlb + "\"," +    //  合同类别
    saveContract({
            "htzynr": "范围：华府金沙名城项目地基处理工程：局部土石方外运、换填混凝土地基处理，施工相关内容。暂定价格：10,000,000元;付款进度:第一次付款节点,发包人与分包人办理完结算6个月内,发包人支付分包人50%;第二次付款:结算办理完成后9个月内,承包人支付分包人至结算款的80%;第三次付款节点:结算办理完成12个月内,承包人支付分包人至结算价的95%",
            "bz": "this is remark",
            "jbr": "杨晶晶",
            "htjk": 23456780,
            "xmmc": "华府金沙名城",
            "htbh": "HFJSMC-C04-2017-003",
            "htmc": "testPrint",
            "dfdw": "北京隆博宇仓储设备有限公司",
            "jbbm": "工程部",
            "htlb": "精装修合同",
            "fkfs": "银行汇款",
            "jbsj": "2017-11-07 04:12",
            "auditList": [{
                "actor": "杨晶晶",
                "remark": "同意",
                "time": "2017-11-07 04:15"
            }, {"actor": "刘健", "remark": "同意", "time": "2017-11-07 04:15"}, {
                "actor": "唐华",
                "remark": "同意",
                "time": "2017-11-07 04:15"
            }, {"actor": "张武格", "remark": "同意", "time": "2017-11-07 04:16"}, {
                "actor": "陈琦",
                "remark": "同意",
                "time": "2017-11-07 04:16"
            }, {"actor": "唐飞", "remark": "同意", "time": "2017-11-07 04:17"}]
        }


        , function (err, data) {
            if (err) {
                res.render("error", err);
            } else {

                res.send(data);
            }
        });
});


router.get('/saveYfd', function (req, res, next) {
    yfd(
        {
            "parameters": [
                "SD_FDC_HTHBD",
                //HTHB2018082200001
                "{\"Numbers\":[\"HTHB2018090300003\"]}"
            ]
        }
        , function (err, data) {
        if (err) {
            res.render("error", err);
        } else {
            res.send(data);
        }
    });
});



router.get('/savegys', function (req, res, next) {
    gys({
        providerId: "110601162528673998a7c58d5929f778", providerName: "北京隆博宇仓储设备有限公司",
        bnkNbr: "6225887810429788", bnkName: "科技园招商银行"
    }, function (err, data) {
        if (err) {
            res.render("error", err);
        } else {

            res.send(data);
        }
    });
});


router.get('/sendMsg/:toUser', function (req, res, next) {
    //054536074021334237"},{"name":"刘谋杰修改","userid":"manager6075
    var back = "";
    console.log("req.param.toUser : " + req.params.toUser);
    var createGroupBack = request('POST', 'https://oapi.dingtalk.com/message/send?access_token=' + token.token()
        , {
            json: {
                "touser": "054966445034958781", // req.params.toUser,
                "agentid":  "167476086",  //conf.agentId,
                "msgtype": "text",
                "text": {
                    "content": "詹老板你好,金蝶测试消息"
                }
            }
            //json: {
            //    "touser": "2265444021334237",
            //    "agentid": "124131018",
            //    "msgtype": "link",
            //    "link": {
            //        "messageUrl": "http://",
            //        "picUrl": "https://thumbs.dreamstime.com/z/%E5%AE%A1%E6%89%B9-13432038.jpg",
            //        "title": "这是一个合同",
            //        "text": "[合同名称]:一个合同\n[对方单位]:成都兴锐达\n[合同金额]:300<br>[本次申请金额]:100",
            //    }
            //}
        });
    res.send(createGroupBack.getBody('utf8'));
});

router.get('/nb', function (req, res, next) {
    //054536074021334237"},{"name":"刘谋杰修改","userid":"manager6075

    nbk.wise("曾晋明的单子要不得", "1709221530080740b37694bd13e58183");

    res.send('OK');
});

router.get('/cnb', function (req, res, next) {
    //054536074021334237"},{"name":"刘谋杰修改","userid":"manager6075

    nbk.cloud("曾晋明的单子要求不得" + Date.now(), "59ce02ea23c0fe7c6d", function () {
    });

    res.send('OK');
});

router.get('/queryBq', function (req, res, next) {
    //054536074021334237"},{"name":"刘谋杰修改","userid":"manager6075

    queryBq(function(){
        res.send('OK');
    });

});


router.get('/sendLinkMsgRnm/:toUser', function (req, res, next) {

    //054536074021334237"},{"name":"刘谋杰修改","userid":"manager6075
    var back = "";
    console.log("-------------start-----------------");
    var createGroupBack = request('POST', 'https://oapi.dingtalk.com/message/send?access_token=' + token.token()
        , {
            json: {
                "touser": req.param('toUser'),
                "agentid": "167476086",
                "msgtype": "link",
                "link": {
                    "title": "测试",
                    "text": "测试",
                    "picUrl": "https://gw.alicdn.com/tps/TB1FN16LFXXXXXJXpXXXXXXXXXX-256-130.png",
                    "messageUrl": "http://www.dingtalk.com"
                }
            }
        });
    console.log(createGroupBack.getBody('utf8'));
    res.send(createGroupBack.getBody('utf8'));
});


router.get('/syncPay/:wfId', function (req, res, next) {
    let wfId = req.params.wfId;
    let gkdObj = {};


    sql.connect({
            user: conf.htdb.username,
            password: conf.htdb.password,
            server: conf.htdb.hostname, // You can use 'localhost\\instance' to connect to named instance
            database: conf.htdb.database
        }
    ).then(pool => {

        console.log("------- in syncPay ------");


        let gkdObj = {};
        let payRestSqls = [pool.request().query("select a.CONTRACT_NAME, a.CONTRACT_CODE, a.CONTRACT_MONEY, b.APPROVED_MONEY, b.APPLY_TIME, c.PROJECT_NAME, " +
            "d.name, b.WF_ID, d.BANKNBR, d.BANKNAME, d.CLOUD_CODE, b.APPROVED_REMARK, b.APPLY_ID, a.CONTRACT_ID, b.APPLY_CODE, " +
            "b.SUM_OUTPUT_VALUE_MONEY, a.SPECIALITY_ID " +
            "from T_CON_CONTRACT a  left join T_CON_PAY_APPLY b  on a.CONTRACT_ID = b.CONTRACT_ID " +
            "inner join T_SYS_PROJECT c on a.PROJECT_ID = c.PROJECT_ID " +
            "inner join T_GYS_SYNC_WISE d on a.B_NAME = d.NAME " +
            "where a.CONTRACT_ID = (select contract_id from T_CON_PAY_APPLY where wf_id = '" + wfId + "') " +
            "and b.WF_STATUS = 4 order by b.APPLY_TIME"),
            pool.request().query("select a.ACTOR_NAME, a.FINISH_TIME, a.REMARK, b.NODE_TYPE from T_SYS_WF_TASK a, T_SYS_WF_NODE b " +
                "where a.wf_id = '" + wfId + "' and a.NODE_ID = b.NODE_ID order by a.FINISH_TIME")];

        logger.info("select a.CONTRACT_NAME, a.CONTRACT_CODE, a.CONTRACT_MONEY, b.APPROVED_MONEY, b.APPLY_TIME, c.PROJECT_NAME, " +
            "d.name, b.WF_ID, d.BANKNBR, d.BANKNAME, d.CLOUD_CODE, b.APPROVED_REMARK, b.APPLY_ID, a.CONTRACT_ID, b.APPLY_CODE, " +
            "b.SUM_OUTPUT_VALUE_MONEY, a.SPECIALITY_ID " +
            "from T_CON_CONTRACT a  left join T_CON_PAY_APPLY b  on a.CONTRACT_ID = b.CONTRACT_ID " +
            "inner join T_SYS_PROJECT c on a.PROJECT_ID = c.PROJECT_ID " +
            "inner join T_GYS_SYNC_WISE d on a.B_NAME = d.NAME " +
            "where a.CONTRACT_ID = (select contract_id from T_CON_PAY_APPLY where wf_id = '" + wfId + "') " +
            "and b.WF_STATUS = 4 order by b.APPLY_TIME");
        logger.info("select a.ACTOR_NAME, a.FINISH_TIME, a.REMARK, b.NODE_TYPE from T_SYS_WF_TASK a, T_SYS_WF_NODE b " +
            "where a.wf_id = '" + wfId + "' and a.NODE_ID = b.NODE_ID order by a.FINISH_TIME");

        Promise.all(payRestSqls).then(payRest => {
            pool.close();
            logger.info("----------------- payRest " + JSON.stringify(payRest));

            let records = payRest[0].recordset;
            let total_money = records[0].CONTRACT_MONEY;
            let total_paid = 0;
            let tenTh = 0;
            let paid_list = [];

            for (let j = 0; j < records.length; j++) {
                total_paid += records[j].SUM_OUTPUT_VALUE_MONEY;
                if (j < 9) {
                    paid_list.push({
                        curPaid: total_paid,
                        curPrecent: total_paid / total_money,
                        curPaidTime: cmoment(records[j].APPLY_TIME).utcOffset("-00:00").format('YYYY-MM-DD'),
                        curApply: records[j].APPROVED_MONEY,
                        totalPaid: records[j].SUM_OUTPUT_VALUE_MONEY,
                        curBnkNbr: records[j].BANKNBR,
                        curBnkName: records[j].BANKNAME
                    });
                } else if (j < records.length - 1) {
                    tenTh += records[j].SUM_OUTPUT_VALUE_MONEY;
                } else {
                    paid_list.push({
                        curPaid: tenTh,
                        curPrecent: tenTh / total_money,
                        curPaidTime: cmoment(records[j].APPLY_TIME).utcOffset("-00:00").format('YYYY-MM-DD'),
                        curApply: records[j].APPROVED_MONEY,
                        totalPaid: records[j].SUM_OUTPUT_VALUE_MONEY,
                        curBnkNbr: records[j].BANKNBR,
                        curBnkName: records[j].BANKNAME
                    });
                }
                // 代表是本次申请
                if (wfId == records[j].WF_ID) {
                    gkdObj = {
                        conName: records[j].CONTRACT_NAME,
                        conCode: records[j].CONTRACT_CODE,
                        conMoney: records[j].CONTRACT_MONEY,
                        prjName: records[j].PROJECT_NAME,
                        venNbr: records[j].CLOUD_CODE,
                        approveRemark: records[j].APPROVED_REMARK,
                        applyTime: cmoment(records[j].APPLY_TIME).utcOffset("-00:00").format('YYYY-MM-DD'),
                        applyMoney: records[j].APPROVED_MONEY,
                        FAPPLY_ID: records[j].APPLY_ID,
                        APPLY_CODE: records[j].APPLY_CODE,
                        CONTRACT_ID: records[j].CONTRACT_ID,
                        SPECIALITY_ID: records[j].SPECIALITY_ID,
                        unittype: "BD_Supplier"
                    }
                }
            }

            let auditRecords = payRest[1].recordset;
            let audit_list = [];

            for (let j in auditRecords) {
                if (auditRecords[j].NODE_TYPE == "1") {
                    gkdObj.creator = auditRecords[j].ACTOR_NAME;
                } else {
                    audit_list.push({
                        actor: auditRecords[j].ACTOR_NAME,
                        remark: auditRecords[j].REMARK,
                        time: cmoment(auditRecords[j].FINISH_TIME).utcOffset("-00:00").format('YYYY-MM-DD HH:mm')
                    });
                }
            }


            logger.info(JSON.stringify(audit_list));

            gkdObj.paidList = paid_list;
            gkdObj.auditList = audit_list;
            gkdObj.totalPaid = total_paid;

            logger.info("=======================================================");
            logger.info(JSON.stringify(gkdObj));
            logger.info("=======================================================");

            fkd(gkdObj, function (err, data) {
                if (err) {
                    logger.info("------------- error  twice  and will return here -----------------");
                    callBack(err);
                } else {

                    sql.connect({
                            user: conf.htdb.username,
                            password: conf.htdb.password,
                            server: conf.htdb.hostname, // You can use 'localhost\\instance' to connect to named instance
                            database: conf.htdb.database
                        }
                    ).then(htPool => {
                        logger.info("+++++++++++++++++++++++++++++++++++++" + htPool);
                        logger.info("insert into T_FKD_APPLY select '" + data.Result.Id + "',a.APPLY_ID, a.APPLY_CODE, a.CONTRACT_ID, b.B_ID, " +
                            "b.B_NAME, b.APPROVED_MONEY,a.HANDLER, GETDATE() from T_CON_PAY_APPLY a , T_CON_CONTRACT b where " +
                            "a.CONTRACT_ID = b.CONTRACT_ID and a.wf_id = '" + wfId + "'");
                        htPool.request().query("insert into T_FKD_APPLY select '" + data.Result.Id + "', a.APPLY_ID, a.APPLY_CODE, a.CONTRACT_ID, b.B_ID, " +
                            "b.B_NAME, a.APPROVED_MONEY,a.HANDLER, GETDATE() from T_CON_PAY_APPLY a , T_CON_CONTRACT b where " +
                            "a.CONTRACT_ID = b.CONTRACT_ID and a.wf_id = '" + wfId + "'").then(result => {
                            logger.info("===================== " + JSON.stringify(result));
                            htPool.close();
                            callBack(null, "success");
                        }).catch(e=> {
                            logger.info(e)
                            htPool.close();
                            callBack(e);
                        })

                    });
                }
            });
        }).catch(e=> {
            logger.info("abcdefg:" + e);
            pool.close();
            callBack(e);
        })


    }).catch(sqlE=> {
        logger.info("abcdefg:" + sqlE);
        pool.close();
        res.send("err:" + sqlE);
    })

});


router.get('/syncBgd/:wfId', function (req, res, next) {
    let wfId = req.params.wfId;
    let gkdObj = {};

    if (sql) {
        sql.close();
    }

    sql.connect({
            user: conf.htdb.username,
            password: conf.htdb.password,
            server: conf.htdb.hostname, // You can use 'localhost\\instance' to connect to named instance
            database: conf.htdb.database
        }
    ).then(pool => {

        console.log("------- in BGSQD ------");

        let conObj = {};
        let payRestSqls = [pool.request().query("select a.CONTRACT_CODE, a.CONTRACT_MONEY,a.CREATOR_NAME,a.REMARK, " +
            "a.SUMMARY,a.B_NAME, a.CREATE_TIME,b.PROJECT_NAME, a.CONTRACT_NAME, c.SPECIALITY_NAME , a.WF_ID , d.APPROVED_MONEY, " +
            "d.SUMMARY as BGMS, d.REASON as BGYY from T_CON_CONTRACT a, T_SYS_PROJECT b, T_CON_SPECIALITY c, T_CON_CHANGE d where " +
            "a.PROJECT_ID = b.PROJECT_ID and a.SPECIALITY_ID = c.SPECIALITY_ID and a.CONTRACT_ID = d.CONTRACT_ID and d.WF_ID = '" + wfId + "'"),
            pool.request().query("select a.ACTOR_NAME, a.FINISH_TIME, a.REMARK from T_SYS_WF_TASK a, T_SYS_WF_NODE b " +
                "where a.wf_id = '" + wfId + "' and a.NODE_ID = b.NODE_ID and b.NODE_TYPE = 2 order by a.FINISH_TIME"),
            pool.request().query("select sum(a.approved_money) as TOTAL_PAY from T_CON_PAY_APPLY a , T_CON_CHANGE b " +
                "where a.CONTRACT_ID = b.CONTRACT_ID and a.WF_STATUS = '4' and b.WF_ID = '" + wfId + "'")];

        logger.info("select a.CONTRACT_CODE, a.CONTRACT_MONEY,a.CREATOR_NAME,a.REMARK, " +
            "a.SUMMARY,a.B_NAME, a.CREATE_TIME,b.PROJECT_NAME, a.CONTRACT_NAME, c.SPECIALITY_NAME , a.WF_ID , d.APPROVED_MONEY, " +
            "d.SUMMARY as BGMS, d.REASON as BGYY from T_CON_CONTRACT a, T_SYS_PROJECT b, T_CON_SPECIALITY c, T_CON_CHANGE d where " +
            "a.PROJECT_ID = b.PROJECT_ID and a.SPECIALITY_ID = c.SPECIALITY_ID and a.CONTRACT_ID = d.CONTRACT_ID and d.WF_ID = '" + wfId + "'");
        logger.info("select a.ACTOR_NAME, a.FINISH_TIME, a.REMARK from T_SYS_WF_TASK a, T_SYS_WF_NODE b " +
            "where a.wf_id = '" + wfId + "' and a.NODE_ID = b.NODE_ID and b.NODE_TYPE = 2 order by a.FINISH_TIME");
        logger.info("select sum(a.approved_money) as TOTAL_PAY from T_CON_PAY_APPLY a , T_CON_CHANGE b " +
            "where a.CONTRACT_ID = b.CONTRACT_ID and a.WF_STATUS = '4' and b.WF_ID = '" + wfId + "'");

        Promise.all(payRestSqls).then(payRest => {
            pool.close();
            let records = payRest[0].recordset[0];
            let paySet = payRest[2].recordset[0];


            logger.info(JSON.stringify(records))
            logger.info(JSON.stringify(paySet))
            logger.info(JSON.stringify(paySet.TOTAL_PAY))

            let totalUnPaid = records.CONTRACT_MONEY - paySet.TOTAL_PAY;

            logger.info(totalUnPaid)


            conObj = {
                "xmmc": records.PROJECT_NAME,
                "htbh": records.CONTRACT_CODE,
                "htlb": records.SPECIALITY_NAME,
                "htmc": records.CONTRACT_NAME,
                "dfdw": records.B_NAME,
                "htje": records.CONTRACT_MONEY,
                "ljyfeje": paySet.TOTAL_PAY,//累计已付额金额
                "wfkje": totalUnPaid,//未付款金额
                "bgje": records.APPROVED_MONEY,
                "bghjtje": records.APPROVED_MONEY + records.CONTRACT_MONEY,
                "bghwfk": records.APPROVED_MONEY + records.CONTRACT_MONEY - paySet.TOTAL_PAY,
                "fwfbl": (records.APPROVED_MONEY + records.CONTRACT_MONEY - paySet.TOTAL_PAY) / (records.APPROVED_MONEY + records.CONTRACT_MONEY),
                "bglr": records.BGYY ? records.BGYY.replace(/\r\n/g, "") : "",
                "bgjems": records.BGMS ? records.BGMS.replace(/\r\n/g, "") : "",
                "bz": records.BGMS ? records.BGMS.replace(/\r\n/g, "") : "",  //备注
            }

            let auditRecords = payRest[1].recordset;
            let audit_list = [];

            for (let j in auditRecords) {
                if (auditRecords[j].NODE_TYPE == "1") {
                    gkdObj.creator = auditRecords[j].ACTOR_NAME;
                } else {
                    audit_list.push({
                        actor: auditRecords[j].ACTOR_NAME,
                        remark: auditRecords[j].REMARK,
                        time: cmoment(auditRecords[j].FINISH_TIME).utcOffset("-00:00").format('YYYY-MM-DD HH:mm')
                    });
                }
            }


            logger.info(JSON.stringify(audit_list));

            conObj.auditList = audit_list;

            logger.info("=======================================================");
            logger.info("变更: " + JSON.stringify(conObj));
            logger.info("=======================================================");

            saveBgSqd(conObj, function (err, data) {
                if (err) {
                    logger.info("------------- error  twice  and will return here -----------------");
                    callBack(err);
                } else {
                    callBack(null, "success");
                }
            });
        }).catch(e=> {
            logger.info("abcdefg:" + e);
            pool.close();
            callBack(e);
        });

    });
});


router.get('/syncBalanced/:wfId', function (req, res, next) {
    let wfId = req.params.wfId;
    let gkdObj = {};

    if (sql) {
        sql.close();
    }

    sql.connect({
            user: conf.htdb.username,
            password: conf.htdb.password,
            server: conf.htdb.hostname, // You can use 'localhost\\instance' to connect to named instance
            database: conf.htdb.database
        }
    ).then(pool => {

        console.log("------- in BGSQD ------");

        let conObj = {};
        let payRestSqls = [pool.request().query("select b.CONTRACT_CODE,b.CONTRACT_NAME,b.CONTRACT_MONEY,a.WATCH_MONEY,b.SIGN_DATE, " +
            "c.PROJECT_NAME, b.B_NAME ,a.REMARK, a.REQUEST_TIME from T_CON_BALANCE a, T_CON_CONTRACT b, T_SYS_PROJECT c where " +
            "a.CONTRACT_ID = b.CONTRACT_ID and b.PROJECT_ID = c.PROJECT_ID and a.WF_STATUS = 4 and a.WF_ID = '" + wfId + "'"),
            pool.request().query("select a.ACTOR_NAME, a.FINISH_TIME, a.REMARK from T_SYS_WF_TASK a, T_SYS_WF_NODE b " +
                "where a.wf_id = '" + wfId + "' and a.NODE_ID = b.NODE_ID and b.NODE_TYPE = 2 order by a.FINISH_TIME")];

        logger.info("select b.CONTRACT_CODE,b.CONTRACT_NAME,b.CONTRACT_MONEY,a.WATCH_MONEY,b.SIGN_DATE, " +
            "c.PROJECT_NAME, b.B_NAME ,a.REMARK, a.REQUEST_TIME from T_CON_BALANCE a, T_CON_CONTRACT b, T_SYS_PROJECT c where " +
            "a.CONTRACT_ID = b.CONTRACT_ID and b.PROJECT_ID = c.PROJECT_ID and a.WF_STATUS = 4 and a.WF_ID = '" + wfId + "'");
        logger.info("select a.ACTOR_NAME, a.FINISH_TIME, a.REMARK from T_SYS_WF_TASK a, T_SYS_WF_NODE b " +
            "where a.wf_id = '" + wfId + "' and a.NODE_ID = b.NODE_ID and b.NODE_TYPE = 2 order by a.FINISH_TIME");

        Promise.all(payRestSqls).then(payRest => {
            pool.close();
            let records = payRest[0].recordset[0];

            logger.info(JSON.stringify(records))

            conObj = {
                "xmmc": records.PROJECT_NAME,
                "htbh": records.CONTRACT_CODE,
                "htmc": records.CONTRACT_NAME,
                "dfdw": records.B_NAME,
                "htje": records.CONTRACT_MONEY,
                "httjje": records.WATCH_MONEY - records.CONTRACT_MONEY,//累计已付额金额
                "jgjsje": records.WATCH_MONEY,//未付款金额
                "htjdsj": cmoment(records.SIGN_DATE).utcOffset("-00:00").format('YYYY-MM-DD'),
                "htjgsj": cmoment(records.REQUEST_TIME).utcOffset("-00:00").format('YYYY-MM-DD'),
                "bz": records.REMARK ? records.REMARK.replace(/\r\n/g, "") : ""  //备注
            }

            let auditRecords = payRest[1].recordset;
            let audit_list = [];

            for (let j in auditRecords) {
                if (auditRecords[j].NODE_TYPE == "1") {
                    gkdObj.creator = auditRecords[j].ACTOR_NAME;
                } else {
                    audit_list.push({
                        actor: auditRecords[j].ACTOR_NAME,
                        remark: auditRecords[j].REMARK,
                        time: cmoment(auditRecords[j].FINISH_TIME).utcOffset("-00:00").format('YYYY-MM-DD HH:mm')
                    });
                }
            }

            logger.info(JSON.stringify(audit_list));

            conObj.auditList = audit_list;

            logger.info("=======================================================");
            logger.info("结算算金额: " + JSON.stringify(conObj));
            logger.info("=======================================================");

            saveBalanced(conObj, function (err, data) {
                if (err) {
                    logger.info("------------- error  twice  and will return here -----------------");
                    callBack(err);
                } else {
                    callBack(null, "success");
                }
            });
        }).catch(e=> {
            logger.info("abcdefg:" + e);
            pool.close();
            callBack(e);
        });

    });
});


router.get('/syncTBgd', function (req, res, next) {


    saveBgSqd({
        "xmmc": "华府金沙名城",
        "htbh": "HFJSMC-C03-2017-007",
        "htlb": "建筑安装合同",
        "htmc": "监理一、二标段补充协议",
        "dfdw": "四川兴恒信项目管理咨询有限公司",
        "htje": 2531677,
        "ljyfeje": 442167.7,
        "wfkje": 2089509.3,
        "bgje": 361800,
        "bghjtje": 2893477,
        "bghwfk": 2451309.3,
        "fwfbl": "25%",

        //"bglr": "bglr",
        //"bgjems": "bgjems",
        //"bz": "bz",

        "bglr": "原监理协议绝对工期为42个月，原计划为开工日期2017.1.1，竣工日期2020.6.1，现调整为计划开工日期为2017.11.1，竣工日期为2021.4.1，则按监理人员工资附表5人（总监1人、土建专监2人，安装专监1人，资料员1人）36180元/月*10=361800元。",
        "bgjems": "原监理协议绝对工期为42个月，原计划为开工日期2017.1.1，竣工日期2020.6.1，现调整为计划开工日期为2017.11.1，竣工日期为2021.4.1，则按监理人员工资附表5人（总监1人、土建专监2人，安装专监1人，资料员1人）36180元/月*10=361800元。此费用一次性支付给监理单位。",
        "bz": "原监理协议绝对工期为42个月，原计划为开工日期2017.1.1，竣工日期2020.6.1，现调整为计划开工日期为2017.11.1，竣工日期为2021.4.1，则按监理人员工资附表5人（总监1人、土建专监2人，安装专监1人，资料员1人）36180元/月*10=361800元。此费用一次性支付给监理单位。",
        "auditList": [{"actor": "刘健", "remark": "同意", "time": "2018-01-19 10:32"}, {
            "actor": "唐华",
            "remark": "同意",
            "time": "2018-01-19 13:16"
        }, {"actor": "张武格", "remark": "同意", "time": "2018-01-22 17:47"}, {
            "actor": "陈琦",
            "remark": "同意",
            "time": "2018-01-22 17:50"
        }, {"actor": "唐飞", "remark": "同意，但请提供监理例会签到记录", "time": "2018-01-22 18:35"}, {
            "actor": "刘阳英",
            "remark": "收到",
            "time": "2018-01-23 14:52"
        }]
    }, function (err, data) {
        if (err) {
            logger.info("------------- error  twice  and will return here -----------------");
            callBack(err);
        } else {
            callBack(null, "success");
        }
    });
});

router.get('/syncTBalance', function (req, res, next) {

    //"\"F_KLM_Text\":\"" + obj.xmmc + "\"," +   // 项目名称
    //"\"F_KLM_Text1\":\"" + obj.htbh + "\"," +   // 合同编号
    //"\"F_KLM_Text2\":\"" + obj.htmc + "\"," +   // 合同名称
    //"\"F_KLM_Text3\":\"" + obj.dfdw + "\"," +    //  对方单位
    //"\"F_KLM_Decimal\":\"" + obj.htje + "\"," +    //  合同金额
    //"\"F_KLM_Decimal1\":\"" + obj.httjje + "\"," +    //  合同调整金额
    //"\"F_KLM_Decimal2\":\"" + obj.jgjsje + "\"," +    //  竣工结算金额
    //"\"F_KLM_Text4\":\"" + obj.htjdsj + "\"," +    //  合同签订时间
    //"\"F_KLM_Text5\":\"" + obj.htjgsj + "\"," +    //  合同竣工时间
    //"\"F_KLM_Text6\":\"" + obj.bz + "\"," +    //  备注
    //

    saveBalanced({
        "xmmc": "华府金沙名城",
        "htbh": "HFJSMC-C03-2017-007",
        "htmc": "监理一、二标段补充协议",
        "dfdw": "四川兴恒信项目管理咨询有限公司",
        "htje": 2531677,
        "httjje": 1235,
        "jgjsje": 2345678,
        "htjdsj": "2017-01-01",
        "htjgsj": "2017-01-01",
        "bz": "原监理协议绝对工期为42个月，原计划为开工日期2017.1.1，竣工日期2020.6.1，现调整为计划开工日期为2017.11.1，竣工日期为2021.4.1，则按监理人员工资附表5人（总监1人、土建专监2人，安装专监1人，资料员1人）36180元/月*10=361800元。此费用一次性支付给监理单位。",
        "auditList": [{"actor": "刘健", "remark": "同意", "time": "2018-01-19 10:32"}, {
            "actor": "唐华",
            "remark": "同意",
            "time": "2018-01-19 13:16"
        }, {"actor": "张武格", "remark": "同意", "time": "2018-01-22 17:47"}, {
            "actor": "陈琦",
            "remark": "同意",
            "time": "2018-01-22 17:50"
        }, {"actor": "唐飞", "remark": "同意，但请提供监理例会签到记录", "time": "2018-01-22 18:35"}, {
            "actor": "刘阳英",
            "remark": "收到",
            "time": "2018-01-23 14:52"
        }]
    }, function (err, data) {
        if (err) {
            logger.info("------------- error  twice  and will return here -----------------");
            callBack(err);
        } else {
            callBack(null, "success");
        }
    });
});

router.get('/queryTable/:wfId', function (req, res, next) {
    let wfId = req.params.wfId;
    let gkdObj = {};

    if (sql) {
        sql.close();
    }

    sql.connect({
            user: conf.htdb.username,
            password: conf.htdb.password,
            server: conf.htdb.hostname, // You can use 'localhost\\instance' to connect to named instance
            database: conf.htdb.database
        }
    ).then(pool => {

        console.log("------- in BGSQD ------");

        let conObj = {};
        let reqSql = "select name from sysobjects where xtype = 'U' and name not like 'TMP%'";

        logger.info("select b.CONTRACT_CODE,b.CONTRACT_NAME,b.CONTRACT_MONEY,a.WATCH_MONEY,b.SIGN_DATE, " +
            "c.PROJECT_NAME, b.B_NAME ,a.REMARK, a.REQUEST_TIME from T_CON_BALANCE a, T_CON_CONTRACT b, T_SYS_PROJECT c where " +
            "a.CONTRACT_ID = b.CONTRACT_ID and b.PROJECT_ID = c.PROJECT_ID and a.WF_STATUS = 4 and a.WF_ID = '" + wfId + "'");
        logger.info("select a.ACTOR_NAME, a.FINISH_TIME, a.REMARK from T_SYS_WF_TASK a, T_SYS_WF_NODE b " +
            "where a.wf_id = '" + wfId + "' and a.NODE_ID = b.NODE_ID and b.NODE_TYPE = 2 order by a.FINISH_TIME");

        Promise.all(payRestSqls).then(payRest => {
            pool.close();
            let records = payRest[0].recordset[0];

            logger.info(JSON.stringify(records))

            conObj = {
                "xmmc": records.PROJECT_NAME,
                "htbh": records.CONTRACT_CODE,
                "htmc": records.CONTRACT_NAME,
                "dfdw": records.B_NAME,
                "htje": records.CONTRACT_MONEY,
                "httjje": records.WATCH_MONEY - records.CONTRACT_MONEY,//累计已付额金额
                "jgjsje": records.WATCH_MONEY,//未付款金额
                "htjdsj": cmoment(records.SIGN_DATE).utcOffset("-00:00").format('YYYY-MM-DD'),
                "htjgsj": cmoment(records.REQUEST_TIME).utcOffset("-00:00").format('YYYY-MM-DD'),
                "bz": records.REMARK ? records.REMARK.replace(/\r\n/g, "") : ""  //备注
            }

            let auditRecords = payRest[1].recordset;
            let audit_list = [];

            for (let j in auditRecords) {
                if (auditRecords[j].NODE_TYPE == "1") {
                    gkdObj.creator = auditRecords[j].ACTOR_NAME;
                } else {
                    audit_list.push({
                        actor: auditRecords[j].ACTOR_NAME,
                        remark: auditRecords[j].REMARK,
                        time: cmoment(auditRecords[j].FINISH_TIME).utcOffset("-00:00").format('YYYY-MM-DD HH:mm')
                    });
                }
            }

            logger.info(JSON.stringify(audit_list));

            conObj.auditList = audit_list;

            logger.info("=======================================================");
            logger.info("结算算金额: " + JSON.stringify(conObj));
            logger.info("=======================================================");

            saveBalanced(conObj, function (err, data) {
                if (err) {
                    logger.info("------------- error  twice  and will return here -----------------");
                    callBack(err);
                } else {
                    callBack(null, "success");
                }
            });
        }).catch(e=> {
            logger.info("abcdefg:" + e);
            pool.close();
            callBack(e);
        });

    });
});

module.exports = router;
