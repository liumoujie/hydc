var express = require('express');
var router = express.Router();

var _ = require('lodash');

var conf = require('../config/config');
var util = require('../libs/util');
var sql = require('mssql');
var request = require('sync-request');
var token = require('../libs/token');
var Id = require('../libs/Id');
var Promise = require('bluebird');
var logger = require('../config/log4js').getLogger('total');

/* GET home page. */
router.get('/:empid', function (req, res, next) {

    logger.info("empid : " + req.params.empid);

    var phoneNbr = null;
    var pool = null;
    var htPool = null;

    var complete = 0;
    var oldData = null;
    var cb = function (err, data) {
        logger.info("--------------- called : " + JSON.stringify(data));
        complete++;
        if (complete == 1) {
            oldData = data;
        }
        if (complete == 2) {

            logger.info("-------------------------------------------------------------------------");
            logger.info({
                title: '浩源审核',
                wait: {
                    total: oldData.wait.total + data.wait.total,
                    hisTotal: oldData.wait.hisTotal + data.wait.hisTotal,
                    list: _.concat(oldData.wait.list, data.wait.list),
                    his: _.concat(oldData.wait.his, data.wait.his)
                }
            });

            res.send({
                total: oldData.wait.total + data.wait.total,
                hisTotal: oldData.wait.hisTotal + data.wait.hisTotal,
                list: _.concat(oldData.wait.list, data.wait.list),
                his: _.concat(oldData.wait.his, data.wait.his)
            });
        }
    }

    var back = request('GET', 'https://oapi.dingtalk.com/user/get?access_token=' + token.token()
        + '&userid=' + req.params.empid);
    try {
        var mobileJson = JSON.parse(back.getBody('utf8'));
        phoneNbr = mobileJson.mobile;

        logger.info("mobileJson : " + JSON.stringify(mobileJson));
        logger.info("phonenbr : " + phoneNbr);

        //phoneNbr = "18980670503"
        //phoneNbr = "18980670504";  //曾晋明
        //phoneNbr = "13541326589";  //唐华
        //phoneNbr = "15908161512";  //唐飞
        //phoneNbr = "18980670507";  //唐真虎
        //phoneNbr = "15982833513";  //樊蓉
        //phoneNbr = "18382259996";  //陈琦
        //phoneNbr = "13881809971";  //唐文杰
        //phoneNbr = "13980092927";  //钱娟
        //phoneNbr = "13881722193";  //孔静
        //phoneNbr = "15902802769";  //张武格
        //phoneNbr = "15882484685";  //刘阳英
        //phoneNbr = "13551388705";
        //phoneNbr = "13308022696"; //刘健
        //phoneNbr = "18244238981";//杨晶晶

        if (!phoneNbr || phoneNbr.length < 2) {
            res.render('error', {
                message: '出错啦,没找到对应的电话号码',
                error: {
                    status: "",
                    stack: ""
                }
            });
            return;
        }

        pool = new sql.ConnectionPool({
            user: conf.db.username,
            password: conf.db.password,
            server: conf.db.hostname, // You can use 'localhost\\instance' to connect to named instance
            database: conf.db.database
        }, err => {
            logger.info("connect err : " + err);
            pool.request()
                .input('phone', sql.NVarChar, phoneNbr)
                .input('acct', sql.NVarChar, req.params.empid)
                .query(' select FID from T_DIN_ACCT where FPHONE = @phone ', (err, result) => {

                    logger.info(err);
                    logger.info("find ding ding account reustl : " + JSON.stringify(result));

                    if (result.recordset.length == 0) {
                        pool.request()
                            .input('id', sql.NVarChar, Date.now() + Id.get())
                            .input('phone', sql.NVarChar, phoneNbr)
                            .input('acct', sql.NVarChar, req.params.empid)
                            .query('insert into T_DIN_ACCT values (@id, @phone,@acct)', (err, result) => {
                                if (err) {
                                    logger.info("insert dingding acct error" + err);
                                }
                                logger.info(JSON.stringify(result));
                                pool.close();
                                queryList(cb);
                                queryHTList(cb);
                            });
                    }
                    else {
                        pool.close();
                        queryList(cb);
                        queryHTList(cb);
                    }

                });

        });


    } catch (e) {
        res.render('error', {
            message: '出错啦',
            error: {
                status: "",
                stack: ""
            }
        });
    }

    var queryList = function (cb) {
        logger.info("cb : " + cb);
        pool = new sql.ConnectionPool({
            user: conf.db.username,
            password: conf.db.password,
            server: conf.db.hostname, // You can use 'localhost\\instance' to connect to named instance
            database: conf.db.database
        }, err => {
            logger.info("----------- phoneNbr : " + phoneNbr);

            console.log("++++++===== select count(*) nbrs, e.FOBJECTTYPEID from T_SEC_USER a, T_WF_RECEIVER b, T_WF_ASSIGN c, T_SEC_USER d, " +
                "T_WF_APPROVALASSIGN e  where a.FUSERID = b.FRECEIVERID and b.FASSIGNID = c.FASSIGNID and " +
                "c.FSENDERID = d.FUSERID  and c.FASSIGNID = e.FASSIGNID and c.FSTATUS = 0 and a.FPHONE = @phone " +
                "group by e.FOBJECTTYPEID");

            var querys = [];
            querys.push(pool.request()
                .input('phone', sql.NVarChar, phoneNbr)
                .query("select count(*) nbrs, rtrim(e.FOBJECTTYPEID) as FOBJECTTYPEID from T_SEC_USER a, T_WF_RECEIVER b, T_WF_ASSIGN c, T_SEC_USER d, " +
                    "T_WF_APPROVALASSIGN e  where a.FUSERID = b.FRECEIVERID and b.FASSIGNID = c.FASSIGNID and " +
                    "c.FSENDERID = d.FUSERID  and c.FASSIGNID = e.FASSIGNID and c.FSTATUS = 0 and a.FPHONE = @phone " +
                    "group by e.FOBJECTTYPEID"));

            querys.push(pool.request()
                .input('phone', sql.NVarChar, phoneNbr)
                .query("select count(*) as nbrs, rtrim(b.FOBJECTTYPEID) as FOBJECTTYPEID from (select distinct FOBJECTTYPEID, FKEYVALUE " +
                    "from T_WF_ASSIGN a, T_SEC_USER b, T_WF_APPROVALASSIGN c where b.FPHONE = @phone " +
                    " and FACTIONEVENTID = '4' and FSENDERID = b.fuserid and c.FASSIGNID = a.FASSIGNID) b group by b.FOBJECTTYPEID"));

            logger.info("++++   select count(*) as nbrs, rtrim(b.FOBJECTTYPEID) as FOBJECTTYPEID from (select distinct FOBJECTTYPEID, FKEYVALUE " +
                "from T_WF_ASSIGN a, T_SEC_USER b, T_WF_APPROVALASSIGN c where b.FPHONE = @phone " +
                " and FACTIONEVENTID = '4' and FSENDERID = b.fuserid and c.FASSIGNID = a.FASSIGNID) b group by b.FOBJECTTYPEID");

            Promise.all(querys).then(allResult => {
                logger.info("allResult : " + JSON.stringify(allResult));
                let showList = [];
                let recordSets = allResult[0].recordset;
                for (var i in recordSets) {

                    showList.push({
                        nbrs: recordSets[i].nbrs,
                        type: recordSets[i].FOBJECTTYPEID
                    })
                }

                //处理历史数据
                let hisList = [];
                let hisRecordSets = allResult[1].recordset;
                for (var i in hisRecordSets) {

                    hisList.push({
                        nbrs: hisRecordSets[i].nbrs,
                        type: hisRecordSets[i].FOBJECTTYPEID
                    })
                }
                pool.close();
                cb(null, {
                    wait: {
                        total: allResult[0].rowsAffected[0],
                        hisTotal: allResult[1].rowsAffected[0],
                        list: showList,
                        his: hisList
                    }
                });
            });
        })
    }

    pool.on('error', err => {
        logger.info(err);
        pool.close();
    });


    var queryHTList = function (cb) {
        htPool = new sql.ConnectionPool({
            user: conf.htdb.username,
            password: conf.htdb.password,
            server: conf.htdb.hostname, // You can use 'localhost\\instance' to connect to named instance
            database: conf.htdb.database
        }, err => {
            logger.info("-----ht list------ phoneNbr : " + phoneNbr);

            var querys = [];
            querys.push(htPool.request()
                .input('phone', sql.NVarChar, phoneNbr)
                .query("select count(*) as nbrs, rtrim(a.BUSINESS_TYPE) as  BUSINESS_TYPE from T_SYS_WF_TASK a , T_SYS_USERINFO b, T_SYS_WF c, " +
                    "T_SYS_USERINFO d  where a.WF_ID = c.WF_ID and a.ACTOR_ID = b.ORG_ID and a.STATE != 4 and b.MOBILE = @phone " +
                    "and c.INITIATOR_ID = d.ORG_ID group by a.BUSINESS_TYPE"));
            querys.push(htPool.request()
                .input('phone', sql.NVarChar, phoneNbr)
                .query(" select count(*) as nbrs, rtrim(a.BUSINESS_TYPE) as BUSINESS_TYPE from T_SYS_WF_TASK a , T_SYS_USERINFO b, T_SYS_WF c, " +
                    "T_SYS_USERINFO d where a.WF_ID = c.WF_ID and a.ACTOR_ID = b.ORG_ID and a.STATE = 4 and b.MOBILE = @phone " +
                    "and c.INITIATOR_ID = d.ORG_ID group by a.BUSINESS_TYPE "));

            logger.info("Kkkkkkkk total sql: " + 'select a.WF_ID, d.NAME, c.WF_NAME, a.CREATE_TIME, a.ACTOR_ID ' +
                'from T_SYS_WF_TASK a , T_SYS_USERINFO b, T_SYS_WF c, T_SYS_USERINFO d ' +
                'where a.WF_ID = c.WF_ID and a.ACTOR_ID = b.ORG_ID and a.STATE != 4 and b.MOBILE = @phone ' +
                'and c.INITIATOR_ID = d.ORG_ID order by a.CREATE_TIME desc');

            Promise.all(querys).then(allResult => {
                logger.info("all ht Result : " + JSON.stringify(allResult));
                let showList = [];
                let recordSets = allResult[0].recordset;
                for (var i in recordSets) {
                    showList.push({
                        nbrs: recordSets[i].nbrs,
                        type: recordSets[i].BUSINESS_TYPE
                    })
                }

                //处理历史数据
                let hisList = [];
                let hisRecordSets = allResult[1].recordset;
                for (var i in hisRecordSets) {
                    hisList.push({
                        nbrs: hisRecordSets[i].nbrs,
                        type: hisRecordSets[i].BUSINESS_TYPE
                    })
                }

                htPool.close();

                cb(null, {
                    wait: {
                        total: allResult[0].rowsAffected[0],
                        hisTotal: allResult[1].rowsAffected[0],
                        list: showList,
                        his: hisList
                    }
                });

            });
        });

        htPool.on('error', err => {
            logger.info(err);
            pool.close();
        });
    }
});

/* GET home page. */
router.get('/t/:empid', function (req, res, next) {

    logger.info("empid : " + req.params.empid);

    var phoneNbr = req.params.empid;
    var pool = null;
    var htPool = null;

    var complete = 0;
    var oldData = null;
    var cb = function (err, data) {
        logger.info("--------------- called : " + JSON.stringify(data));
        complete++;
        if (complete == 1) {
            oldData = data;
        }
        if (complete == 2) {

            logger.info("-------------------------------------------------------------------------");
            logger.info({
                title: '浩源审核',
                wait: {
                    total: oldData.wait.total + data.wait.total,
                    hisTotal: oldData.wait.hisTotal + data.wait.hisTotal,
                    list: _.concat(oldData.wait.list, data.wait.list),
                    his: _.concat(oldData.wait.his, data.wait.his)
                }
            });

            res.send({
                total: oldData.wait.total + data.wait.total,
                hisTotal: oldData.wait.hisTotal + data.wait.hisTotal,
                list: _.concat(oldData.wait.list, data.wait.list),
                his: _.concat(oldData.wait.his, data.wait.his)
            });
        }
    }


        pool = new sql.ConnectionPool({
            user: conf.db.username,
            password: conf.db.password,
            server: conf.db.hostname, // You can use 'localhost\\instance' to connect to named instance
            database: conf.db.database
        }, err => {
            logger.info("connect err : " + err);
            pool.request()
                .input('phone', sql.NVarChar, phoneNbr)
                .input('acct', sql.NVarChar, req.params.empid)
                .query(' select FID from T_DIN_ACCT where FPHONE = @phone and FDINACT = @acct ', (err, result) => {

                        pool.close();
                        queryList(cb);
                        queryHTList(cb);

                });

        });

    var queryList = function (cb) {
        logger.info("cb : " + cb);
        pool = new sql.ConnectionPool({
            user: conf.db.username,
            password: conf.db.password,
            server: conf.db.hostname, // You can use 'localhost\\instance' to connect to named instance
            database: conf.db.database
        }, err => {
            logger.info("----------- phoneNbr : " + phoneNbr);

            console.log("++++++===== select count(*) nbrs, e.FOBJECTTYPEID from T_SEC_USER a, T_WF_RECEIVER b, T_WF_ASSIGN c, T_SEC_USER d, " +
                "T_WF_APPROVALASSIGN e  where a.FUSERID = b.FRECEIVERID and b.FASSIGNID = c.FASSIGNID and " +
                "c.FSENDERID = d.FUSERID  and c.FASSIGNID = e.FASSIGNID and c.FSTATUS = 0 and a.FPHONE = @phone " +
                "group by e.FOBJECTTYPEID");

            var querys = [];
            querys.push(pool.request()
                .input('phone', sql.NVarChar, phoneNbr)
                .query("select count(*) nbrs, rtrim(e.FOBJECTTYPEID) as FOBJECTTYPEID from T_SEC_USER a, T_WF_RECEIVER b, T_WF_ASSIGN c, T_SEC_USER d, " +
                    "T_WF_APPROVALASSIGN e  where a.FUSERID = b.FRECEIVERID and b.FASSIGNID = c.FASSIGNID and " +
                    "c.FSENDERID = d.FUSERID  and c.FASSIGNID = e.FASSIGNID and c.FSTATUS = 0 and a.FPHONE = @phone " +
                    "group by e.FOBJECTTYPEID"));
            querys.push(pool.request()
                .input('phone', sql.NVarChar, phoneNbr)
                .query("select count(*) as nbrs, rtrim(b.FOBJECTTYPEID) as FOBJECTTYPEID from (select distinct FOBJECTTYPEID, FKEYVALUE " +
                    "from T_WF_ASSIGN a, T_SEC_USER b, T_WF_APPROVALASSIGN c where b.FPHONE = @phone " +
                    " and FACTIONEVENTID = '4' and FSENDERID = b.fuserid and c.FASSIGNID = a.FASSIGNID) b group by b.FOBJECTTYPEID"));

            logger.info("++++   select count(*) as nbrs, rtrim(b.FOBJECTTYPEID) as FOBJECTTYPEID from (select distinct FOBJECTTYPEID, FKEYVALUE " +
                "from T_WF_ASSIGN a, T_SEC_USER b, T_WF_APPROVALASSIGN c where b.FPHONE = @phone " +
                " and FACTIONEVENTID = '4' and FSENDERID = b.fuserid and c.FASSIGNID = a.FASSIGNID) b group by b.FOBJECTTYPEID");

            Promise.all(querys).then(allResult => {
                logger.info("allResult : " + JSON.stringify(allResult));
                let showList = [];
                let recordSets = allResult[0].recordset;
                for (var i in recordSets) {
                    showList.push({
                        nbrs: recordSets[i].nbrs,
                        type: recordSets[i].FOBJECTTYPEID
                    })
                }

                //处理历史数据
                let hisList = [];
                let hisRecordSets = allResult[1].recordset;
                for (var j in hisRecordSets) {
                    hisList.push({
                        nbrs: hisRecordSets[j].nbrs,
                        type: hisRecordSets[j].FOBJECTTYPEID
                    })
                }
                pool.close();
                cb(null, {
                    wait: {
                        total: allResult[0].rowsAffected[0],
                        hisTotal: allResult[1].rowsAffected[0],
                        list: showList,
                        his: hisList
                    }
                });
            });
        })
    }

    pool.on('error', err => {
        logger.info(err);
        pool.close();
    });


    var queryHTList = function (cb) {
        htPool = new sql.ConnectionPool({
            user: conf.htdb.username,
            password: conf.htdb.password,
            server: conf.htdb.hostname, // You can use 'localhost\\instance' to connect to named instance
            database: conf.htdb.database
        }, err => {
            logger.info("-----ht list------ phoneNbr : " + phoneNbr);

            var querys = [];
            querys.push(htPool.request()
                .input('phone', sql.NVarChar, phoneNbr)
                .query("select count(*) as nbrs, rtrim(a.BUSINESS_TYPE) as  BUSINESS_TYPE from T_SYS_WF_TASK a , T_SYS_USERINFO b, T_SYS_WF c, " +
                    "T_SYS_USERINFO d  where a.WF_ID = c.WF_ID and a.ACTOR_ID = b.ORG_ID and a.STATE != 4 and b.MOBILE = @phone " +
                    "and c.INITIATOR_ID = d.ORG_ID group by a.BUSINESS_TYPE"));
            querys.push(htPool.request()
                .input('phone', sql.NVarChar, phoneNbr)
                .query(" select count(*) as nbrs, rtrim(a.BUSINESS_TYPE) as BUSINESS_TYPE from T_SYS_WF_TASK a , T_SYS_USERINFO b, T_SYS_WF c, " +
                    "T_SYS_USERINFO d where a.WF_ID = c.WF_ID and a.ACTOR_ID = b.ORG_ID and a.STATE = 4 and b.MOBILE = @phone " +
                    "and c.INITIATOR_ID = d.ORG_ID group by a.BUSINESS_TYPE "));

            logger.info("Kkkkkkkk total sql: " + 'select a.WF_ID, d.NAME, c.WF_NAME, a.CREATE_TIME, a.ACTOR_ID ' +
                'from T_SYS_WF_TASK a , T_SYS_USERINFO b, T_SYS_WF c, T_SYS_USERINFO d ' +
                'where a.WF_ID = c.WF_ID and a.ACTOR_ID = b.ORG_ID and a.STATE != 4 and b.MOBILE = @phone ' +
                'and c.INITIATOR_ID = d.ORG_ID order by a.CREATE_TIME desc');

            Promise.all(querys).then(allResult => {
                logger.info("all ht Result : " + JSON.stringify(allResult));
                let showList = [];
                let recordSets = allResult[0].recordset;
                for (var i in recordSets) {
                    showList.push({
                        nbrs: recordSets[i].nbrs,
                        type: recordSets[i].BUSINESS_TYPE
                    })
                }

                //处理历史数据
                let hisList = [];
                let hisRecordSets = allResult[1].recordset;
                for (var i in hisRecordSets) {
                    hisList.push({
                        nbrs: hisRecordSets[i].nbrs,
                        type: hisRecordSets[i].BUSINESS_TYPE
                    })
                }

                htPool.close();

                cb(null, {
                    wait: {
                        total: allResult[0].rowsAffected[0],
                        hisTotal: allResult[1].rowsAffected[0],
                        list: showList,
                        his: hisList
                    }
                });

            });
        });

        htPool.on('error', err => {
            logger.info(err);
            pool.close();
        });
    }
});


module.exports = router;
