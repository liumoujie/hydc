var express = require('express');
var router = express.Router();

var util = require('../libs/util');
var detail = require('../service/queryHtDetail');

/* GET home page. */
router.get('/:table/:assignid/:actor', function (req, res, next) {

    detail.queryHtDetails(req.params.table, req.params.assignid, req.params.actor, false, function (err, data) {
        if (err) {
            if (err.error.status == 0) {
                console.log("-------------------------eeeeeeeeeeeeeee-------------------------------------------------------");
                detail.queryHtDetails(req.params.table, req.params.assignid, req.params.actor, true, function (err, data) {
                    if (err) {
                        res.render("error", err);
                    } else {
                        data.showSubmit = false;
                        console.log("detail data : " + JSON.stringify(data));
                        res.render('detail', data);
                    }

                })
            } else {
                err = {
                    message: "查询详情出错",
                    error: {
                        status: "错误",
                        status: "出错"
                    }
                }
                res.render("error", err);
            }
        } else {
            console.log("--------------------------------------------------------------------------------");
            data.showSubmit = true;
            console.log("detail data : " + JSON.stringify(data));
            res.render('detail', data);
        }

    })
});

//用于只给出显示界面
router.get('/:table/:assignid/:actor/only', function (req, res, next) {

    detail.queryHtDetails(req.params.table, req.params.assignid, req.params.actor, true, function (err, data) {
        if (err) {
            res.render("error", err);
        } else {
            data.showSubmit = false;
            console.log("detail data : " + JSON.stringify(data));
            res.render('detail', data);
        }

    })
});

module.exports = router;
