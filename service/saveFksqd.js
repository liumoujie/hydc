/**
 * Created by zhoulanhong on 9/14/17.
 */

var request = require('sync-request');
var conf = require('../config/config');
var sql = require('mssql');
var token = require('../libs/token');

var logger = require('../config/log4js').getLogger('saveFksqd');

function save(obj, cb) {

    if (sql) {
        sql.close();
    }

    let loginBack = request('POST', conf.webApi.loginUrl
        , {
            json: {
                "parameters": [
                    conf.webApi.dbCenterId,
                    conf.webApi.userName,
                    conf.webApi.passwd,
                    conf.webApi.lang
                ]
            }
        });
    logger.info(conf.webApi.loginUrl);
    logger.info({
        "parameters": [
            conf.webApi.dbCenterId,
            conf.webApi.userName,
            conf.webApi.passwd,
            conf.webApi.lang
        ]
    });
    let rsp = loginBack.getBody('utf8');
    logger.info(JSON.stringify(rsp));
    let kdCooie = null;
    logger.info(JSON.stringify(kdCooie));
    let rspJsonObj = null;
    try {
        logger.info(loginBack.headers);
        let ckstr = JSON.stringify(loginBack.headers);//.substring(loginBack.headers.toString().indexOf('set-cookie'));
        logger.info(ckstr);
        logger.info(ckstr.substring(ckstr.indexOf('kdservice-sessionid')));
        ckstr = (ckstr.substring(ckstr.indexOf('kdservice-sessionid'))).split(";");
        kdCooie = ckstr[0];
        logger.info("kdCooie" + kdCooie);
        rspJsonObj = JSON.parse(rsp);
        logger.info(JSON.stringify(rspJsonObj));
        if (rspJsonObj.LoginResultType != "1") {
            cb({
                message: "登陆k3cloud出错",
                error: {
                    status: 0,
                    stack: rspJsonObj.Message
                }
            });
            return;
        }
    } catch (e) {
        logger.info(e);
        cb(e);
        return;
    }
//F_KLM_DECIMAL1  申请金额
    let payListStr = "{\"F_KLM_Date\":\"" +
        obj.paidList[0].curPaidTime + "\",\"F_KLM_Amount\":\"" +
        obj.paidList[0].totalPaid + "\",\"F_KLM_DECIMAL1\":\"" +
        obj.paidList[0].curApply + "\",\"F_KLM_Amount1\":\"" +
        obj.paidList[0].curPaid + "\",\"F_KLM_Decimal\":\"" +
        obj.paidList[0].curPrecent + "\",\"F_KLM_Text3\":\"" +
        obj.paidList[0].curBnkName + "\",\"F_KLM_Text4\":\"" +
        obj.paidList[0].curBnkNbr + "\"}";
    for (let i = 1; i < obj.paidList.length; i++) {
        payListStr += ",{\"F_KLM_Date\":\"" +
            obj.paidList[i].curPaidTime + "\",\"F_KLM_Amount\":\"" +
            obj.paidList[i].totalPaid + "\",\"F_KLM_DECIMAL1\":\"" +
            obj.paidList[i].curApply + "\",\"F_KLM_Amount1\":\"" +
            obj.paidList[i].curPaid + "\",\"F_KLM_Decimal\":\"" +
            obj.paidList[i].curPrecent + "\",\"F_KLM_Text3\":\"" +
            obj.paidList[i].curBnkName + "\",\"F_KLM_Text4\":\"" +
            obj.paidList[i].curBnkNbr + "\"}"
    }

    let auditListStr = "{\"F_KLM_Text6\":\"" +
        obj.auditList[0].actor + "\",\"F_KLM_Text7\":\"" +
        obj.auditList[0].remark + "\",\"F_KLM_TEXT10\":\"" +
        obj.auditList[0].time + "\"}";
    for (let i = 1; i < obj.auditList.length; i++) {
        auditListStr += ",{\"F_KLM_Text6\":\"" +
            obj.auditList[i].actor + "\",\"F_KLM_Text7\":\"" +
            obj.auditList[i].remark + "\",\"F_KLM_TEXT10\":\"" +
            obj.auditList[i].time + "\"}"
    }

    //1106011924147348b819ff5e0c5376d3, 1711290854378518b4c4e734dd7f14b6,	171208144153768291c8cf119b2624e8     GCFWLFKSQ
    //170627150606452a99a2d6c5b30953a1, 17062715041846898eded4b3233df91e                 GCJDKSQ
    //1706271505016020be5b6db5735fd45c, 17062715021959628b4449b3fc88379e			GCQISQ
    //171129085723316ab038df3387fab6f1											GCCGLSQ
    //1712081441220536bd104169be52b7e6			BJLHTFKSQ
    //1712081442326280bf3a8eeb56206c57			GCLXFKSQD
    //1712081443347948a7db90415f1d3dd0			FGCLXCGSQD
    //171207110528349e91091e1531f3d037			FGCXZ
    //171207110649984fae3c95a83b09bd3a			FGCYX
    //171207110621467ca2d3bf5a02821a91			FGCCW
    //171207110714336f990118e8370e5e1e			FGCQT

    let getFKDTypd = function (contractType) {
        if (contractType == "1106011924147348b819ff5e0c5376d3"
            || contractType == "1711290854378518b4c4e734dd7f14b6"
            || contractType == "171208144153768291c8cf119b2624e8") {
            return "GCFWLFKSQ";
        } else if (contractType == "171129085723316ab038df3387fab6f1") {
            return "GCCGLSQ";
        } else if (contractType == "17062715041846898eded4b3233df91e"
            || contractType == "170627150606452a99a2d6c5b30953a1") {
            return "GCJDKSQ"
        } else if (contractType == "1712081441220536bd104169be52b7e6") {
            return "BJLHTFKSQ"
        } else if (contractType == "1712081442326280bf3a8eeb56206c57") {
            return "GCLXFKSQD"
        } else if (contractType == "1712081443347948a7db90415f1d3dd0") {
            return "FGCLXCGSQD"
        } else if (contractType == "171207110528349e91091e1531f3d037") {
            return "FGCXZ"
        } else if (contractType == "171207110649984fae3c95a83b09bd3a") {
            return "FGCYX"
        } else if (contractType == "171207110621467ca2d3bf5a02821a91") {
            return "FGCCW"
        } else if (contractType == "171207110714336f990118e8370e5e1e") {
            return "FGCQT"
        } else {
            return "GCQISQ";
        }
    }

    let jsonPara = {
        "parameters": [
            "CN_PAYAPPLY",
            "{\"Model\":{" +
            "\"FBILLTYPEID\":{\"FNumber\":\"" + getFKDTypd(obj.SPECIALITY_ID) + "\"}," +
                //"\"FBILLTYPEID\":{\"FNumber\":\"" + conf.webApi.billTypeNbr + "\"}," +
            "\"F_KLM_TEXT5\":\"" + obj.conCode + "\"," +   // 合同编号
            "\"F_KLM_TEXT\":\"" + obj.approveRemark + "\"," +   // 经办日期
            "\"F_KLM_DATE1\":\"" + obj.applyTime + "\"," +   // 经办日期
            "\"F_KLM_TEXT8\":\"" + obj.creator + "\"," +   // 经办人
            "\"F_KLM_AMOUNT5\":\"" + obj.totalPaid + "\"," +   // 累计已付金额
            "\"FDATE\":\"" + obj.applyTime + "\"," +
            "\"F_KLM_Text1\":\"" + obj.prjName + "\"," +   // 项目名称
            "\"F_KLM_TEXT2\":\"" + obj.conName + "\"," +   // 合同名称
            "\"F_KLM_Amount2\":\"" + obj.conMoney + "\"," +    //  合同总价
            "\"F_KLM_Amount3\":\"" + obj.applyMoney + "\"," +     //  本次申请金额
            "\"FCONTACTUNITTYPE\":\"" + obj.unittype + "\"," +
            "\"FCONTACTUNIT\":{\"FNumber\":\"" + obj.venNbr + "\"}," +
            "\"FRECTUNITTYPE\":\"" + obj.unittype + "\"," +
            "\"FRECTUNIT\":{\"FNumber\":\"" + obj.venNbr + "\"}," +
            "\"FCURRENCYID\":{\"FNumber\":\"PRE001\"}," +
            "\"FPAYORGID\":{\"FNumber\":\"100\"}," +
            "\"FSETTLEORGID\":{\"FNumber\":\"100\"}," +
            "\"FDEPARTMENT\":{\"FNumber\":\"BM000018\"}," +
            "\"FDOCUMENTSTATUS\":\"B\"," +
            "\"FScanPoint\":{\"FNumber\":\"\"}," +
            "\"FCANCELSTATUS\":\"A\"," +
            "\"F_KLM_Base\":{\"FNumber\":\"SFKYT015\"}," +

                //"\"F_KLM_Entity\":[{\"F_KLM_Date\":\"1900-01-01\",\"F_KLM_Amount\":\"1000\",\"F_KLM_Amount1\":\"5000\",\"F_KLM_Decimal\":\"20\",\"F_KLM_Text3\":\"兴业银行\",\"F_KLM_Text4\":\"345436456\"}," +
                //"{\"F_KLM_Date\":\"1900-01-01\",\"F_KLM_Amount\":\"2000\",\"F_KLM_Amount1\":\"5000\",\"F_KLM_Decimal\":\"40\",\"F_KLM_Text3\":\"兴业银行\",\"F_KLM_Text4\":\"345436456\"}]," +

            "\"F_KLM_Entity\":[" + payListStr + "]," +


            "\"F_KLM_Entity1\":[" + auditListStr + "]," +


            "\"FPAYAPPLYENTRY\":[{\"FPAYPURPOSEID\":{\"FNumber\":\"SFKYT015\"},\"FENDDATE\":\"2017-11-01\",\"FEXPECTPAYDATE\":\"2017-11-01\",\"FAPPLYAMOUNTFOR\":\"" + obj.applyMoney + "\",\"FDescription\":\"Use Web Api Demo\"}]" +
            "}}"
        ]
    }
    logger.info(JSON.stringify(jsonPara));
    let saveBack = request('POST', conf.webApi.saveFksqdUrl
        , {
            headers: {
                'Cookie': kdCooie
            },
            json: jsonPara
        });
    let saveRsp = saveBack.getBody('utf8');
    let supplierNumber = null;
    let rspJson = null;
    try {
        rspJson = JSON.parse(saveRsp);
        logger.info(JSON.stringify(rspJson));
        if (rspJson.Result.ResponseStatus.IsSuccess) {
            logger.info("--- will start submit/audit fksqd ---");


            //保存完成之后,自动提交,自动审核
            try {
                rspJson = JSON.parse(saveRsp);
                supplierNumber = rspJson.Result.Number;
                logger.info(JSON.stringify(rspJson));

                let submitBack = request('POST', conf.webApi.submit
                    , {
                        headers: {
                            'Cookie': kdCooie
                        },
                        json: {
                            "parameters": [
                                "CN_PAYAPPLY",
                                "{\"Numbers\":[\"" + supplierNumber + "\"]}"
                            ]
                        }
                    }).getBody('utf8');

                let submitRsp = null;
                try {
                    submitRsp = JSON.parse(submitRsp);
                    logger.info(JSON.stringify(submitRsp))
                    let auditBack = request('POST', conf.webApi.audit
                        , {
                            headers: {
                                'Cookie': kdCooie
                            },
                            json: {
                                "parameters": [
                                    "CN_PAYAPPLY",
                                    "{\"Numbers\":[\"" + supplierNumber + "\"]}"
                                ]
                            }
                        }).getBody('utf8');

                } catch (e) {

                }
            } catch (e) {
                cb(e);
            }


            try {
                logger.info("--- before sql ---");
                sql.connect({
                        user: conf.db.username,
                        password: conf.db.password,
                        server: conf.db.hostname, // You can use 'localhost\\instance' to connect to named instance
                        database: conf.db.database
                    }
                ).then(pool => {
                    logger.info("insert into T_FKD_SYNC_WISE values ('" + rspJson.Result.Id + "','" + obj.CONTRACT_ID + "','" + obj.conMoney + "','" + obj.FAPPLY_ID + "','" + obj.APPLY_CODE + "','W',GETDATE())");
                    return pool.request().query("insert into T_FKD_SYNC_WISE values ('" + rspJson.Result.Id + "','" + obj.CONTRACT_ID + "','" + obj.conMoney + "','" + obj.FAPPLY_ID + "','" + obj.APPLY_CODE + "','W',GETDATE())");
                }).then(result => {
                    logger.info("----------------------- sdfsgsdfg     :" + JSON.stringify(result));
                    sql.close();
                    cb(null, rspJson);

                    let createGroupBack = request('POST', 'https://oapi.dingtalk.com/message/send?access_token=' + token.token()
                        , {
                            json: {
                                "touser": conf.finNoticeUsr,
                                "agentid": conf.agentId,
                                "msgtype": "text",
                                "text": {
                                    "content": "你好,付款申请单" + supplierNumber + "已审批通过,请进行后续处理"
                                }
                            }
                        });

                }).catch(e=> {
                    logger.info("----------------------- sdfsgsdfg     :" + e)
                    cb(e);
                })
            } catch (e) {
                logger.info("------------  abc   ----------- sdfsgsdfg     :" + e)
            }
        } else {
            logger.info("-------------- in  cb callback --------- ");
            cb(rspJson);
        }

    } catch (e) {
        cb(e);
    }

    sql.on('error', function (e) {
        logger.info('-----------------------------');
        logger.info(e);
    })

}

module.exports = save;

//"\"F_KLM_Entity1\":[{\"F_KLM_Text6\":\"曾晋明\",\"F_KLM_Text7\":\"同意通过\",\"F_KLM_Time\":\"1900-01-01\"}],"+
