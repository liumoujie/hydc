/**
 * Created by zhoulanhong on 8/26/17.
 */
var conf = require('../config/config');
var sql = require('mssql');
var cmoment = require('moment');
var logger = require('../config/log4js').getLogger('queryHtDetail');

var detail = function () {
}
function queryHtDetail(tableName, assignId, actor, only, callBack) {

    let supportTable = ['t_con_contract', 't_con_pay_apply', 't_con_change', 't_con_balance'];

    if (sql) {
        sql.close();
    }
    logger.info("------------------------------------ table tablename : " + tableName);
    logger.info("------------------------------------ actor actor : " + actor);
    logger.info("------------------------------------ 123 tablename : " + supportTable.indexOf(tableName));

    if (supportTable.indexOf(tableName) < 0) {
        callBack({
            message: "暂不支持付款单以外的单据类型",
            error: {
                status: "错误",
                status: "正在开发中"
            }
        })
        return;
    }


    if (sql) {
        sql.close();
    }

    let ret = {
        title: '浩源审核',
        table: tableName,
        assignid: assignId
    };
    let pool = null;


    let queryPayDetail = function (bottoms) {
        try {
            let result = null;
            let tResult = null;
            let bResult = null;
            logger.info("update T_SYS_WF_TASK set ACCEPT_TIME = GETDATE() where WF_ID = '" + assignId + "' and ACTOR_ID ='" + actor + "' and ACCEPT_TIME is null")
            pool.request().query("update T_SYS_WF_TASK set ACCEPT_TIME = GETDATE() where WF_ID = '" + assignId + "' and ACTOR_ID ='" + actor + "' and ACCEPT_TIME is null").then(updTime => {
                logger.info(JSON.stringify(updTime));
                return pool.request().query("select APPLY_TIME, REQUEST_MONEY, HANDLER, REMARK, WF_STATUS from t_con_pay_apply where wf_id ='" + assignId + "'")
            }).then(pResult => {
                result = pResult
                return pool.request().query("select avg(k.CONTRACT_MONEY) htmoney ,avg(k.ADDITIVE_MONEY) bcmoney ,avg(k.CHANGED_MONEY) bgmoney ," +
                    " avg(k.BALANCED_MONEY) jsmoney ,isnull(sum(b.approved_money),0) appmoney, isnull(sum(b.APPROVE_PERCENT),0) cent " +
                    "from (select a.CONTRACT_MONEY, a.CONTRACT_ID, a.ADDITIVE_MONEY, a.CHANGED_MONEY, a.BALANCED_MONEY " +
                    " from T_CON_CONTRACT a where a.CONTRACT_ID = (select CONTRACT_ID from T_CON_PAY_APPLY " +
                    "where WF_ID = '" + assignId + "')) k left join T_CON_PAY_APPLY b on k.contract_id = b.contract_id and b.WF_STATUS = 4")
                //   select BALANCED_MONEY from T_CON_BALANCE where CONTRACT_ID = (select CONTRACT_ID from T_CON_PAY_APPLY
                //    where WF_ID = '110602170933447e817d5209cd6b8d646')
            }).then(tttResult => {
                tResult = tttResult;
                logger.info("select BALANCED_MONEY from T_CON_CONTRACT where CONTRACT_ID = (select CONTRACT_ID from T_CON_PAY_APPLY " +
                    "where WF_ID = '" + assignId + "')");
                return pool.request().query("select BALANCED_MONEY from T_CON_CONTRACT where CONTRACT_ID = (select CONTRACT_ID from T_CON_PAY_APPLY " +
                    "where WF_ID = '" + assignId + "')");
            }).then(balanceResult=> {
                bResult = balanceResult;
                logger.info("=====" + JSON.stringify(bResult));
                logger.info("select a.FILE_NAME, a.FILE_PATH from T_SYS_ATTACHMENT a, T_CON_PAY_APPLY b " +
                    "where a.BUSINESS_ID = b.APPLY_ID and b.WF_ID = '" + assignId + "'");
                return pool.request().query("select a.FILE_NAME, a.FILE_PATH from T_SYS_ATTACHMENT a, T_CON_PAY_APPLY b " +
                    "where a.BUSINESS_ID = b.APPLY_ID and b.WF_ID = '" + assignId + "'");
            }).then(attResult=> {

                logger.info("all --abc-- atttach : " + JSON.stringify(attResult));
                let attach = [];
                for (let i = 0; i < attResult.recordset.length; i++) {
                    let fn = attResult.recordset[i].FILE_PATH.split("\\")
                    attach.push({
                        fName: attResult.recordset[i].FILE_NAME,
                        fUrl: "http://" + conf.hostName + ":" + conf.port + "/attach/contract/t_con_pay_apply/" + fn[fn.length - 1]
                    });
                }

                let contractMoney = tResult.recordset[0].htmoney;
                //如果有结算,合同价按结算价
                if (bResult.recordset[0].BALANCED_MONEY > 0) {
                    contractMoney = bResult.recordset[0].BALANCED_MONEY;
                }else {
                    logger.info("- tResult  :" + JSON.stringify(tResult));
                    contractMoney = tResult.recordset[0].htmoney + tResult.recordset[0].bcmoney + tResult.recordset[0].bgmoney;
                }

                logger.info("select APPLY_TIME, REQUEST_MONEY, HANDLER, REMARK, WF_STATUS from t_con_pay_apply where wf_id ='" + assignId + "'");
                logger.info(JSON.stringify(result));
                logger.info("---- unit result :" + JSON.stringify(result));
                let middle = [
                    {title: "单据类型", text: "合同请款单"},

                    {title: "合同金额", text: tResult.recordset[0].htmoney},
                    {title: "补充金额", text: tResult.recordset[0].bcmoney},
                    {title: "变更金额", text: tResult.recordset[0].bgmoney},
                    {title: "合同总价", text: contractMoney},
                    {title: "结算金额", text: bResult.recordset.length > 0 ? bResult.recordset[0].BALANCED_MONEY : "未结算"},
                    {title: "已付总额", text: tResult.recordset[0].appmoney},
                    {title: "已付比例", text: ((tResult.recordset[0].appmoney / contractMoney) * 100).toFixed(2) + "%"},
                    {title: "申请人员", text: result.recordset[0].HANDLER},
                    {title: "申请金额", text: result.recordset[0].REQUEST_MONEY},
                    {title: "备注信息", text: result.recordset[0].REMARK},
                    {
                        title: "申请时间",
                        //text: result.recordset[0].FCREATEDATE
                        text: cmoment(result.recordset[0].APPLY_TIME).utcOffset("-00:00").format('YYYY-MM-DD')
                    }
                ];
                ret.page = {
                    top: {
                        name: result.recordset[0].HANDLER,
                        fullname: result.recordset[0].HANDLER,
                        status: result.recordset[0].WF_STATUS == "4" ? "审核完成" : "正在审核"
                    },
                    middle: middle,
                    attach: attach,
                    bottom: bottoms
                }
                callBack(null, ret);
            }).catch(e=> {
                logger.info(e);
                callBack(e);
            });
        } catch (e) {
            callBack(e);
            logger.info(e);
        }
    }

    let queryChangeDetail = function (bottoms) {
        try {
            let result = null;
            pool.request().query("select  CONTRACT_NAME, CONTRACT_CODE, B_NAME,CONTRACT_MONEY, b.WF_STATUS,  a.CHANGE_NAME, a.REQUEST_MONEY, " +
                "c.NAME, a.CREATE_TIME from T_CON_CHANGE a, T_CON_CONTRACT b, T_SYS_USERINFO c where a.CONTRACT_ID = b.CONTRACT_ID " +
                "and a.CREATOR_ID = c.ORG_ID and a.wf_id = '" + assignId + "'").then(detailResult => {
                result = detailResult;
                logger.info("select  CONTRACT_NAME, CONTRACT_CODE, B_NAME,CONTRACT_MONEY, b.WF_STATUS,  a.CHANGE_NAME, a.REQUEST_MONEY, " +
                    "c.NAME, a.CREATE_TIME from T_CON_CHANGE a, T_CON_CONTRACT b, T_SYS_USERINFO c where a.CONTRACT_ID = b.CONTRACT_ID " +
                    "and a.CREATOR_ID = c.ORG_ID and a.wf_id = '" + assignId + "'");
                logger.info("---- unit result :" + JSON.stringify(result));

                return pool.request().query("select a.FILE_NAME, a.FILE_PATH from T_SYS_ATTACHMENT a, T_CON_CHANGE b " +
                    "where a.BUSINESS_ID = b.CHANGE_ID and b.WF_ID = '" + assignId + "'");
            }).then(attResult=> {
                logger.info("all atttach : " + JSON.stringify(attResult));
                let attach = [];
                for (let i = 0; i < attResult.recordset.length; i++) {
                    let fn = attResult.recordset[i].FILE_PATH.split("\\")
                    attach.push({
                        fName: attResult.recordset[i].FILE_NAME,
                        fUrl: "http://" + conf.hostName + ":" + conf.port + "/attach/contract/t_con_change/" + fn[fn.length - 1]
                    });
                }

                let middle = [
                    {title: "单据类型", text: "合同变更"},
                    {title: "合同编号", text: result.recordset[0].CONTRACT_CODE},
                    {title: "合同名称", text: result.recordset[0].CONTRACT_NAME},
                    {title: "乙方单位", text: result.recordset[0].B_NAME},
                    {title: "合同单价", text: result.recordset[0].CONTRACT_MONEY},
                    {title: "变更名称", text: result.recordset[0].CHANGE_NAME},
                    {title: "变更金额", text: result.recordset[0].REQUEST_MONEY},
                    {
                        title: "申请时间",
                        //text: result.recordset[0].FCREATEDATE
                        text: cmoment(result.recordset[0].CREATE_TIME).utcOffset("-00:00").format('YYYY-MM-DD')
                    }
                ];
                ret.page = {
                    top: {
                        name: result.recordset[0].NAME,
                        fullname: result.recordset[0].NAME,
                        status: result.recordset[0].WF_STATUS == "4" ? "审核完成" : "正在审核"
                    },
                    attach: attach,
                    middle: middle,
                    bottom: bottoms
                }
                callBack(null, ret);
            }).catch(e=> {
                logger.info(e);
                callBack(e);
            });
        } catch (e) {
            callBack(e);
            logger.info(e);
        }
    }

    let queryBalannceDetail = function (bottoms) {
        try {
            let result = null;
            pool.request().query("select  CONTRACT_NAME, CONTRACT_CODE, B_NAME,CONTRACT_MONEY, b.WF_STATUS,  a.OUTPUT_VALUE_MONEY REQUEST_MONEY, " +
                "c.NAME, a.CREATE_TIME from T_CON_BALANCE a, T_CON_CONTRACT b, T_SYS_USERINFO c " +
                "where a.CONTRACT_ID = b.CONTRACT_ID and a.CREATOR_ID = c.ORG_ID and a.wf_id = '" + assignId + "'").then(detailResult => {
                result = detailResult;
                logger.info("select  CONTRACT_NAME, CONTRACT_CODE, B_NAME,CONTRACT_MONEY, b.WF_STATUS,  a.REQUEST_MONEY, " +
                    "c.NAME, a.CREATE_TIME from T_CON_BALANCE a, T_CON_CONTRACT b, T_SYS_USERINFO c " +
                    "where a.CONTRACT_ID = b.CONTRACT_ID and a.CREATOR_ID = c.ORG_ID and a.wf_id = '" + assignId + "'");
                logger.info("---- unit result :" + JSON.stringify(result));

                return pool.request().query("select a.FILE_NAME, a.FILE_PATH from T_SYS_ATTACHMENT a, T_CON_BALANCE b " +
                    "where a.BUSINESS_ID = b.BALANCE_ID and b.WF_ID = '" + assignId + "'");
            }).then(attResult=> {
                logger.info("all atttach : " + JSON.stringify(attResult));
                let attach = [];
                for (let i = 0; i < attResult.recordset.length; i++) {
                    let fn = attResult.recordset[i].FILE_PATH.split("\\")
                    attach.push({
                        fName: attResult.recordset[i].FILE_NAME,
                        fUrl: "http://" + conf.hostName + ":" + conf.port + "/attach/contract/t_con_balance/" + fn[fn.length - 1]
                    });
                }

                let middle = [
                    {title: "单据类型", text: "合同结算"},
                    {title: "合同编号", text: result.recordset[0].CONTRACT_CODE},
                    {title: "合同名称", text: result.recordset[0].CONTRACT_NAME},
                    {title: "乙方单位", text: result.recordset[0].B_NAME},
                    {title: "合同金额", text: result.recordset[0].CONTRACT_MONEY},
                    {title: "结算金额", text: result.recordset[0].REQUEST_MONEY},
                    {
                        title: "申请时间",
                        //text: result.recordset[0].FCREATEDATE
                        text: cmoment(result.recordset[0].CREATE_TIME).utcOffset("-00:00").format('YYYY-MM-DD')
                    }
                ];
                ret.page = {
                    top: {
                        name: result.recordset[0].NAME,
                        fullname: result.recordset[0].NAME,
                        status: result.recordset[0].WF_STATUS == "4" ? "审核完成" : "正在审核"
                    },
                    attach: attach,
                    middle: middle,
                    bottom: bottoms
                }
                callBack(null, ret);
            }).catch(e=> {
                logger.info(e);
                callBack(e);
            });
        } catch (e) {
            callBack(e);
            logger.info(e);
        }
    }


    let queryHTDetail = function (bottoms) {
        try {
            let result = null;
            pool.request().query("select a.PROJECT_NAME, CONTRACT_NAME, CONTRACT_CODE, B_NAME,CONTRACT_MONEY, b.WF_STATUS, " +
                "c.NAME, b.CREATE_TIME, b.SPECIALITY_ID from T_SYS_PROJECT a, T_CON_CONTRACT b, T_SYS_USERINFO c " +
                "where a.PROJECT_ID = b.PROJECT_ID and b.CREATOR_ID = c.ORG_ID and b.wf_id = '" + assignId + "'").then(detailResult => {
                result = detailResult;
                logger.info("select a.PROJECT_NAME, CONTRACT_NAME, CONTRACT_CODE, B_NAME,CONTRACT_MONEY, b.WF_STATUS, " +
                    "c.NAME, b.CREATE_TIME, b.SPECIALITY_ID from T_SYS_PROJECT a, T_CON_CONTRACT b, T_SYS_USERINFO c " +
                    "where a.PROJECT_ID = b.PROJECT_ID and b.CREATOR_ID = c.ORG_ID and b.wf_id = '" + assignId + "'");
                logger.info("---- unit result :" + JSON.stringify(result));

                return pool.request().query("select a.FILE_NAME, a.FILE_PATH from T_SYS_ATTACHMENT a, T_CON_CONTRACT b " +
                    "where a.BUSINESS_ID = b.CONTRACT_ID and b.WF_ID = '" + assignId + "'");
            }).then(attResult=> {
                logger.info("all atttach : " + JSON.stringify(attResult));
                let attach = [];
                for (let i = 0; i < attResult.recordset.length; i++) {
                    let fn = attResult.recordset[i].FILE_PATH.split("\\")
                    attach.push({
                        fName: attResult.recordset[i].FILE_NAME,
                        fUrl: "http://" + conf.hostName + ":" + conf.port + "/attach/contract/t_con_contract/" + fn[fn.length - 1]
                    });
                }


                let getFKDTypd = function (contractType) {
                    if (contractType == "1106011924147348b819ff5e0c5376d3"
                        || contractType == "1711290854378518b4c4e734dd7f14b6"
                        || contractType == "171208144153768291c8cf119b2624e8") {
                        return "工程服务类合同";
                    } else if (contractType == "171129085723316ab038df3387fab6f1") {
                        return "工程采购类合同";
                    } else if (contractType == "17062715041846898eded4b3233df91e"
                        || contractType == "170627150606452a99a2d6c5b30953a1") {
                        return "工程类合同"
                    } else if (contractType == "1712081441220536bd104169be52b7e6") {
                        return "报建类合同"
                    } else if (contractType == "1712081442326280bf3a8eeb56206c57") {
                        return "工程零星采购类合同"
                    } else if (contractType == "1712081443347948a7db90415f1d3dd0") {
                        return "非工程零星采购类合同"
                    } else if (contractType == "171207110528349e91091e1531f3d037") {
                        return "行政类合同"
                    } else if (contractType == "171207110649984fae3c95a83b09bd3a") {
                        return "营销类合同"
                    } else if (contractType == "171207110621467ca2d3bf5a02821a91") {
                        return "财务类合同"
                    } else if (contractType == "171207110714336f990118e8370e5e1e") {
                        return "非工程类其它合同"
                    } else {
                        return "工程类其它合同";
                    }
                }

                let middle = [
                    {title: "单据类型", text: getFKDTypd(result.recordset[0].SPECIALITY_ID)},
                    {title: "项目名称", text: result.recordset[0].PROJECT_NAME},
                    {title: "合同编号", text: result.recordset[0].CONTRACT_CODE},
                    {title: "合同名称", text: result.recordset[0].CONTRACT_NAME},
                    {title: "乙方单位", text: result.recordset[0].B_NAME},
                    {title: "合同单价", text: result.recordset[0].CONTRACT_MONEY},
                    {
                        title: "申请时间",
                        //text: result.recordset[0].FCREATEDATE
                        text: cmoment(result.recordset[0].CREATE_TIME).utcOffset("-00:00").format('YYYY-MM-DD')
                    }
                ];
                ret.page = {
                    top: {
                        name: result.recordset[0].NAME,
                        fullname: result.recordset[0].NAME,
                        status: result.recordset[0].WF_STATUS == "4" ? "审核完成" : "正在审核"
                    },
                    attach: attach,
                    middle: middle,
                    bottom: bottoms
                }
                callBack(null, ret);
            }).catch(e=> {
                logger.info(e);
                callBack(e);
            });
        } catch (e) {
            callBack(e);
            logger.info(e);
        }
    }

    let queryPayDetailNoActor = function () {
        logger.info("---- in no actor -----");
        try {
            let result = null;
            let tResult = null;
            logger.info("select APPLY_TIME, REQUEST_MONEY, a.HANDLER, a.REMARK, a.WF_STATUS, b.CONTRACT_NAME, b.A_NAME from t_con_pay_apply a, " +
                "T_CON_CONTRACT b where a.wf_id ='" + assignId + "' and a.CONTRACT_ID = b.CONTRACT_ID")
            pool.request().query("select APPLY_TIME, REQUEST_MONEY, a.HANDLER, a.REMARK, a.WF_STATUS, b.CONTRACT_NAME, b.B_NAME from t_con_pay_apply a, " +
                    "T_CON_CONTRACT b where a.wf_id ='" + assignId + "' and a.CONTRACT_ID = b.CONTRACT_ID")
                .then(pResult => {
                    if (pResult.recordset.length == 0) {
                        callBack("NO", null);
                        return;
                    } else {
                        result = pResult
                        return pool.request().query("select avg(k.CONTRACT_MONEY) htmoney ,avg(k.ADDITIVE_MONEY) bcmoney ,avg(k.CHANGED_MONEY) bgmoney " +
                            ",avg(k.BALANCED_MONEY) jsmoney ,isnull(sum(b.approved_money),0) appmoney, isnull(sum(b.APPROVE_PERCENT),0) cent " +
                            "from (select a.CONTRACT_MONEY, a.CONTRACT_ID, a.ADDITIVE_MONEY, a.CHANGED_MONEY, a.BALANCED_MONEY " +
                            " from T_CON_CONTRACT a where a.CONTRACT_ID = (select CONTRACT_ID from T_CON_PAY_APPLY " +
                            "where WF_ID = '" + assignId + "')) k left join T_CON_PAY_APPLY b on k.contract_id = b.contract_id and b.WF_STATUS = 4");
                    }
                }).then(tttResult => {
                tResult = tttResult;
                logger.info("select APPLY_TIME, REQUEST_MONEY, HANDLER, REMARK, WF_STATUS from t_con_pay_apply where wf_id ='" + assignId + "'");
                logger.info(JSON.stringify(result));
                logger.info("---- unit result :" + JSON.stringify(result));
                let middle = [
                    {htmc: "单据类型", text: "合同请款单"},
                    {title: "合同金额", text: tResult.recordset[0].htmoney},
                    {title: "补充金额", text: tResult.recordset[0].bcmoney},
                    {title: "变更金额", text: tResult.recordset[0].bgmoney},
                    {title: "合同总价", text: tResult.recordset[0].htmoney + tResult.recordset[0].bcmoney + tResult.recordset[0].bgmoney},
                    {title: "已付总额", text: tResult.recordset[0].appmoney},
                    {title: "已付比例", text: (tResult.recordset[0].appmoney/(tResult.recordset[0].htmoney + tResult.recordset[0].bcmoney + tResult.recordset[0].bgmoney)).toFixed(2) + "%"},
                    {title: "申请人员", text: result.recordset[0].HANDLER},
                    {title: "申请金额", text: result.recordset[0].REQUEST_MONEY},
                    {title: "备注信息", text: result.recordset[0].REMARK},
                    {
                        title: "申请时间",
                        //text: result.recordset[0].FCREATEDATE
                        text: cmoment(result.recordset[0].APPLY_TIME).utcOffset("-00:00").format('YYYY-MM-DD')
                    }
                ];
                ret = {
                    htmc: result.recordset[0].CONTRACT_NAME,
                    dfdw: result.recordset[0].B_NAME,
                    htje: tResult.recordset[0].htmoney,
                    bcqk: result.recordset[0].REQUEST_MONEY
                }
                logger.info("-- back in here : " + JSON.stringify(ret))
                callBack(null, ret);
            }).catch(e=> {
                logger.info(e);
                callBack(e);
            });
        } catch (e) {
            callBack(e);
            logger.info(e);
        }
    }


    sql.connect({
            user: conf.htdb.username,
            password: conf.htdb.password,
            server: conf.htdb.hostname, // You can use 'localhost\\instance' to connect to named instance
            database: conf.htdb.database
        }
    ).then(conPool => {
        pool = conPool;

        logger.info("++++++++ select  a.WF_ID, a.ACTOR_ID, a.NODE_ID, a.BUSINESS_ID, d.NAME, c.WF_NAME, a.CREATE_TIME, " +
            "a.CREATOR_ID from T_SYS_WF_TASK a , T_SYS_USERINFO b, T_SYS_WF c, T_SYS_USERINFO d where a.WF_ID = c.WF_ID " +
            "and a.ACTOR_ID = b.ORG_ID " + (only ? "" : " and a.STATE != 4") + " and c.INITIATOR_ID = d.ORG_ID " +
            "and a.WF_ID = '" + assignId + "' and a.ACTOR_ID = '" + actor + "'");
        return pool.request().query("select  a.WF_ID, a.ACTOR_ID, a.NODE_ID, a.BUSINESS_ID, d.NAME, c.WF_NAME, a.CREATE_TIME, " +
            "a.CREATOR_ID from T_SYS_WF_TASK a , T_SYS_USERINFO b, T_SYS_WF c, T_SYS_USERINFO d where a.WF_ID = c.WF_ID " +
            "and a.ACTOR_ID = b.ORG_ID " + (only ? "" : " and a.STATE != 4") + " and c.INITIATOR_ID = d.ORG_ID " +
            "and a.WF_ID = '" + assignId + "' and a.ACTOR_ID = '" + actor + "'");
    }).then(result => {

        logger.info("+++abc==" + JSON.stringify(result));
        logger.info(result.recordset.length);
        if (result.recordset.length == 0) {
            if (actor) {
                callBack({
                    message: "此记录已审核",
                    error: {
                        status: 0,
                        stack: "无需要再审核"
                    }
                })
            } else {
                queryPayDetailNoActor();
            }
        } else {

            ret.userid = result.recordset[0].ACTOR_ID;
            ret.keyValue = result.recordset[0].BUSINESS_ID;
            ret.procInstId = result.recordset[0].WF_ID;
            ret.preArrprovId = result.recordset[0].NODE_ID;
            ret.msgName = 'result.recordset[0].FNAME';
            ret.msg = 'result.recordset[0].FTITLE';

            pool.request().query("select ACTOR_NAME, opinion, REMARK, FINISH_TIME from T_SYS_WF_TASK " +
                "where WF_ID = '" + assignId + "' order by CREATE_TIME ").then(procResult => {

                let procLists = procResult.recordset;
                let bottoms = [];
                for (let k = 0; k < procLists.length; k++) {
                    bottoms.push({
                        name: procLists[k].ACTOR_NAME,
                        fullName: procLists[k].ACTOR_NAME,
                        item: procLists[k].ACTOR_NAME,
                        result: procLists[k].opinion,
                        remark: procLists[k].REMARK ? procLists[k].REMARK : (procLists[k].opinion == "起草" ? "发起流程" : "正在审核" ),
                        date: procLists[k].FINISH_TIME ? cmoment(procLists[k].FINISH_TIME).utcOffset("-00:00").format('YYYY-MM-DD') : "",
                        time: procLists[k].FINISH_TIME ? cmoment(procLists[k].FINISH_TIME).utcOffset("-00:00").format('HH:mm:ss') : "",
                        margin: k == 0 ? "1" : "40",
                        imgDiv: k != 0
                    })
                }

                if (tableName == "t_con_contract") {
                    queryHTDetail(bottoms);
                } else if (tableName == "t_con_pay_apply") {
                    if (actor == null) {
                        queryPayDetailNoActor();
                    } else {
                        queryPayDetail(bottoms);
                    }
                } else if (tableName == "t_con_change") {
                    queryChangeDetail(bottoms);
                } else if (tableName == "t_con_balance") {
                    queryBalannceDetail(bottoms);
                }
            });

        }
    }).catch(pe=> {
        logger.info("-------------------- execute error ----------------------");
        logger.info(pe);
        callBack(pe)
    })

    sql.on('error', err => {
        logger.info("-------------------- sql error ----------------------");
        logger.info(err);
        pool.close();
    });

}

detail.queryHtDetails = queryHtDetail;

module.exports = detail;