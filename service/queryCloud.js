/**
 * Created by zhoulanhong on 9/14/17.
 */

var request = require('sync-request');
var conf = require('../config/config');
var sql = require('mssql');
var token = require('../libs/token');

var Iconv = require('iconv-lite');

//var webIc = require('web-iconv');


var logger = require('../config/log4js').getLogger('query');

function query(obj, cb) {

    if (sql) {
        sql.close();
    }

    let loginBack = request('POST', "http://kdxrd.gnway.cc:8009/k3cloud/Kingdee.BOS.WebApi.ServicesStub.AuthService.LoginByAppSecret.common.kdsvc"
        , {
            json: {
                "parameters": [
                    //conf.webApi.dbCenterId,
                    "5bd01bc4ea5ec2",
                    "admin",
                    "cdbqtest",
                    "3964a021740e4ff08a00e6ba9fe0e335",
                    2052
                ]
            }
        });

    let rsp = loginBack.getBody('utf-8');

    //console.log(rsp);

    //console.log(Iconv.decode(rsp, 'gb2312').toString());

    //webIc.decode(rsp,'gb2312').then(function(data){
    //    console.log(data);
    //})


    //cb();

    //logger.info(iconv.decode(rsp, 'gb2312'));
    let kdCooie = null;
    //logger.info(JSON.stringify(kdCooie));
    let rspJsonObj = null;
    try {
        logger.info(loginBack.headers);
        let ckstr = JSON.stringify(loginBack.headers);//.substring(loginBack.headers.toString().indexOf('set-cookie'));
        //logger.info(ckstr);
        logger.info(ckstr.substring(ckstr.indexOf('kdservice-sessionid')));
        ckstr = (ckstr.substring(ckstr.indexOf('kdservice-sessionid'))).split(";");
        kdCooie = ckstr[0];
        //logger.info("kdCooie" + kdCooie);
        //logger.info("kdCooie" + rsp);

        let supplierNumber = 'CGRK00010';

        rspJsonObj = JSON.parse(rsp);
        logger.info(JSON.stringify(rspJsonObj));
        if (rspJsonObj.LoginResultType != "1") {
            cb({
                message: "登陆k3cloud出错",
                error: {
                    status: 0,
                    stack: rspJsonObj.Message
                }
            });
            return;
        } else {

            //http://ServerIp/K3Cloud/Kingdee.BOS.WebApi.ServicesStub.DynamicFormService.ExecuteBillQuery.common.kdsvc


                //let queryInStock = request('POST', "http://kdxrd.gnway.cc:8009/K3Cloud/Kingdee.BOS.WebApi.ServicesStub.DynamicFormService.ExecuteBillQuery.common.kdsvc"
                //    //let queryInStock = request('POST', "http://kdxrd.gnway.cc:8009/K3Cloud/Kingdee.BOS.WebApi.ServicesStub.DynamicFormService.Delete.common.kdsvc"
                //    , {
                //        headers: {
                //            'Cookie': kdCooie
                //        },
                //        json: {
                //            "parameters": [
                //                "STK_InStock",
                //                "{\"FormId\":\"STK_InStock\",\"FieldKeys\":\"FBillNo\",\"FilterString\":\"FDate='2018-11-05'\",\"OrderString\":\"\",\"TopRowCount\":\"0\",\"StartRow\":\"0\",\"Limit\":\"0\"}"
                //            ]
                //        }
                //    }).getBody('utf8');



            let queryInStock = request('POST', "http://kdxrd.gnway.cc:8009/K3Cloud/Kingdee.BOS.WebApi.ServicesStub.DynamicFormService.View.common.kdsvc"
                //let queryInStock = request('POST', "http://kdxrd.gnway.cc:8009/K3Cloud/Kingdee.BOS.WebApi.ServicesStub.DynamicFormService.Delete.common.kdsvc"
                , {
                    headers: {
                        'Cookie': kdCooie
                    },
                    json: {
                        "parameters": [
                            "STK_InStock",
                            "{\"CreateOrgId\":\"0\",\"Number\":\"CGRK00010\",\"Id\":\"\"}"
                        ]
                    }
                }).getBody('utf8');
            console.log("will start queryInStock ");
            console.log(queryInStock)
            let queryRet = JSON.parse(queryInStock);
            let retData = queryRet.Result.Result;
            let entries = retData.InStockEntry;
            for (let i in entries) {
                console.log("----------------------------");
                console.log("RKRQ:" + retData.Date); //入库日期
                console.log("GYSID:" + retData.SupplierId.Number); //供应商ID
                //let gysNames = retData.SupplierId.Name;
                console.log("GYSNAME:" + getChineseName(retData.SupplierId.Name)); //供应商名称
                console.log("RKDH:" + retData.BillNo); //入库单号
                console.log("CGDDH:" + retData.FSRCBillNo); //采购订单号

                console.log("CKDM:" + entries[i].StockId.Number); //仓库代码
                console.log("CKMC:" + getChineseName(entries[i].StockId.Name)); //仓库名称

                if (entries[i].StockLocId) {
                    console.log("CKDM:" + entries[i].StockLocId.Number); //仓库代码
                    console.log("CKMC:" + getChineseName(entries[i].StockLocId.Name)); //仓库名称
                }

                console.log("WLBM:" + entries[i].MaterialId.Number); //仓库代码
                console.log("WLMC:" + getChineseName(entries[i].MaterialId)); //仓库名称


                console.log("SSSL:" + entries[i].RealQty); //实收数量
                console.log("SSJE:" + entries[i].FCostAmount); //实收金额

            }
        }
    } catch (e) {
        logger.info(e);
        cb(e);
        return;
    }

}

function getChineseName(vList) {
    for (let j in vList) {
        if (vList[j].Key == "2052") {
            //console.log("GYSNAME:" + vList[j].Value); //供应商名称
            return vList[j].Value;
        }
    }
    return "";
}

module.exports = query;

//"\"F_KLM_Entity1\":[{\"F_KLM_Text6\":\"曾晋明\",\"F_KLM_Text7\":\"同意通过\",\"F_KLM_Time\":\"1900-01-01\"}],"+
