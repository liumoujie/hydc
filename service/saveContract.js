/**
 * Created by zhoulanhong on 9/14/17.
 */

var request = require('sync-request');
var conf = require('../config/config');
var sql = require('mssql');
var token = require('../libs/token');

var logger = require('../config/log4js').getLogger('saveConsqd');

function save(obj, cb) {

    if (sql) {
        sql.close();
    }

    let loginBack = request('POST', conf.webApi.loginUrl
        , {
            json: {
                "parameters": [
                    conf.webApi.dbCenterId,
                    conf.webApi.userName,
                    conf.webApi.passwd,
                    conf.webApi.lang
                ]
            }
        });
    let rsp = loginBack.getBody('utf8');
    logger.info("===================== ===== "+JSON.stringify(obj));
    let kdCooie = null;
    logger.info(JSON.stringify(kdCooie));
    let rspJsonObj = null;
    try {
        logger.info(loginBack.headers);
        let ckstr = JSON.stringify(loginBack.headers);//.substring(loginBack.headers.toString().indexOf('set-cookie'));
        logger.info(ckstr);
        logger.info(ckstr.substring(ckstr.indexOf('kdservice-sessionid')));
        ckstr = (ckstr.substring(ckstr.indexOf('kdservice-sessionid'))).split(";");
        kdCooie = ckstr[0];
        logger.info("kdCooie" + kdCooie);
        rspJsonObj = JSON.parse(rsp);
        logger.info(JSON.stringify(rspJsonObj));
        if (rspJsonObj.LoginResultType != "1") {
            cb({
                message: "登陆k3cloud出错",
                error: {
                    status: 0,
                    stack: rspJsonObj.Message
                }
            });
            return;
        }
    } catch (e) {
        logger.info(e);
        cb(e);
        return;
    }

    let auditListStr = "{\"F_KLM_Text\":\"" +
        obj.auditList[0].actor + "\",\"F_KLM_Text1\":\"" +
        obj.auditList[0].remark + "\",\"F_KLM_Text2\":\"" +
        obj.auditList[0].time + "\"}";
    for (let i = 1; i < obj.auditList.length; i++) {
        auditListStr += ",{\"F_KLM_Text\":\"" +
            obj.auditList[i].actor + "\",\"F_KLM_Text1\":\"" +
            obj.auditList[i].remark + "\",\"F_KLM_Text2\":\"" +
            obj.auditList[i].time + "\"}"
    }


    let jsonPara = {
        "parameters": [
            "033f2a60c4d34a0b8539d997092d5a86",
            "{\"Model\":{" +
            "\"F_KLM_Text10\":\"" + obj.htzynr + "\"," +   // 合同主要内容
            "\"F_KLM_Text11\":\"" + obj.bz + "\"," +   // 备注
            "\"F_KLM_Text9\":\"" + obj.jbr + "\"," +   // 经办人
            "\"F_KLM_Decimal\":\"" + obj.htjk + "\"," +   // 合同价款
            "\"F_KLM_Text3\":\"" + obj.htbh + "\"," +    //  合同编号
            "\"F_KLM_Text4\":\"" + obj.htmc + "\"," +    //  合同名称
            "\"F_KLM_Text7\":\"" + obj.dfdw + "\"," +    //  对方单位
            "\"F_KLM_Text8\":\"" + obj.jbbm + "\"," +    //  经办部门
            "\"F_KLM_Text5\":\"" + obj.htlb + "\"," +    //  合同类别
            "\"F_KLM_Text6\":\"" + obj.xmmc + "\"," +    //  项目名称
            "\"F_KLM_Text12\":\"" + obj.fkfs + "\"," +    //  付款方式
            "\"F_KLM_Text13\":\"" + obj.jbsj + "\"," +    //  经办时间

                //"\"FEntity\":[{\"F_KLM_Text\":\"liumoujiering\",\"F_KLM_Text1\":\"verygood\",\"F_KLM_Text2\":\"2017-11-07 11:48\"}]" +

            "\"FEntity\":[" + auditListStr + "]" +
            "}}"
        ]
    }
    logger.info(JSON.stringify(jsonPara));
    let saveBack = request('POST', conf.webApi.saveFksqdUrl
        , {
            headers: {
                'Cookie': kdCooie
            },
            json: jsonPara
        });
    let saveRsp = saveBack.getBody('utf8');
    logger.info(saveRsp);
    let supplierNumber = null;
    let rspJson = null;
    try {
        rspJson = JSON.parse(saveRsp);
        logger.info(JSON.stringify(rspJson));
        if (rspJson.Result.ResponseStatus.IsSuccess) {
            logger.info("--- will start submit/audit fksqd ---");


            //保存完成之后,自动提交,自动审核
            try {
                rspJson = JSON.parse(saveRsp);
                supplierNumber = rspJson.Result.Number;
                logger.info(JSON.stringify(rspJson));

                let submitBack = request('POST', conf.webApi.submit
                    , {
                        headers: {
                            'Cookie': kdCooie
                        },
                        json: {
                            "parameters": [
                                "033f2a60c4d34a0b8539d997092d5a86",
                                "{\"Numbers\":[\"" + supplierNumber + "\"]}"
                            ]
                        }
                    }).getBody('utf8');

                let submitRsp = null;
                try {
                    submitRsp = JSON.parse(submitRsp);
                    logger.info(JSON.stringify(submitRsp))
                    let auditBack = request('POST', conf.webApi.audit
                        , {
                            headers: {
                                'Cookie': kdCooie
                            },
                            json: {
                                "parameters": [
                                    "033f2a60c4d34a0b8539d997092d5a86",
                                    "{\"Numbers\":[\"" + supplierNumber + "\"]}"
                                ]
                            }
                        }).getBody('utf8');

                    if (obj.creatorId) {
                        let createGroupBack = request('POST', 'https://oapi.dingtalk.com/message/send?access_token=' + token.token()
                            , {
                                json: {
                                    "touser": obj.creatorId,
                                    "agentid": conf.agentId,
                                    "msgtype": "text",
                                    "text": {
                                        "content": obj.creatorName + "你好,合同:" + obj.htmc + " 已审批通过,请进行后续处理."
                                    }
                                }
                            });
                    }

                    cb(null, "success");
                } catch (e) {

                }
            } catch (e) {
                cb(e);
            }

        } else {
            logger.info("-------------- in  cb callback --------- ");
            cb(rspJson);
        }

    } catch (e) {
        cb(e);
    }

    sql.on('error', function (e) {
        logger.info('-----------------------------');
        logger.info(e);
    })

}

module.exports = save;

//"\"F_KLM_Entity1\":[{\"F_KLM_Text6\":\"曾晋明\",\"F_KLM_Text7\":\"同意通过\",\"F_KLM_Time\":\"1900-01-01\"}],"+
