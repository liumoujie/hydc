/**
 * Created by zhoulanhong on 9/14/17.
 */

var request = require('sync-request');
var conf = require('../config/config');
var sql = require('mssql');
var token = require('../libs/token');

var logger = require('../config/log4js').getLogger('saveFksqd');

function save(obj, cb) {

    if (sql) {
        sql.close();
    }

    let loginBack = request('POST', conf.webApi.loginUrl
        , {
            json: {
                "parameters": [
                    conf.webApi.dbCenterId,
                    conf.webApi.userName,
                    conf.webApi.passwd,
                    conf.webApi.lang
                ]
            }
        });
    logger.info(conf.webApi.loginUrl);
    logger.info({
        "parameters": [
            conf.webApi.dbCenterId,
            conf.webApi.userName,
            conf.webApi.passwd,
            conf.webApi.lang
        ]
    });
    let rsp = loginBack.getBody('utf8');
    logger.info(JSON.stringify(rsp));
    let kdCooie = null;
    logger.info(JSON.stringify(kdCooie));
    let rspJsonObj = null;
    try {
        logger.info(loginBack.headers);
        let ckstr = JSON.stringify(loginBack.headers);//.substring(loginBack.headers.toString().indexOf('set-cookie'));
        logger.info(ckstr);
        logger.info(ckstr.substring(ckstr.indexOf('kdservice-sessionid')));
        ckstr = (ckstr.substring(ckstr.indexOf('kdservice-sessionid'))).split(";");
        kdCooie = ckstr[0];
        logger.info("kdCooie" + kdCooie);
        rspJsonObj = JSON.parse(rsp);
        logger.info(JSON.stringify(rspJsonObj));
        if (rspJsonObj.LoginResultType != "1") {
            cb({
                message: "登陆k3cloud出错",
                error: {
                    status: 0,
                    stack: rspJsonObj.Message
                }
            });
            return;
        }
    } catch (e) {
        logger.info(e);
        cb(e);
        return;
    }

    let jsonPara = {
        "parameters": [
            "AP_Payable",
            "{\"Model\":{" +
            "\"FSUPPLIERID\":{\"FNumber\":\"VEN00002\"}," +
            "\"FPAYORGID\":{\"FNumber\":\"100\"}," +
            "\"FBillTypeID\":{\"FNumber\":\"YFD01_SYS\"}," +
                //"\"FTaxRateId\":{\"FNumber\":\"SL04_SYS\"}," +
                //"\"FTaxRate\":{\"FNumber\":\"0\"}," +
            "\"FALLAMOUNTFOR\":\"2000\"," +   // 合同编号
                //"\"F_KLM_TEXT\":\"" + obj.approveRemark + "\"," +   // 经办日期
                //"\"F_KLM_DATE1\":\"" + obj.applyTime + "\"," +   // 经办日期
                //"\"F_KLM_TEXT8\":\"" + obj.creator + "\"," +   // 经办人
                //"\"F_KLM_AMOUNT5\":\"" + obj.totalPaid + "\"," +   // 累计已付金额
                //"\"FDATE\":\"" + obj.applyTime + "\"," +
                //"\"F_KLM_Text1\":\"" + obj.prjName + "\"," +   // 项目名称
                //"\"F_KLM_TEXT2\":\"" + obj.conName + "\"," +   // 合同名称
                //"\"F_KLM_Amount2\":\"" + obj.conMoney + "\"," +    //  合同总价
                //"\"F_KLM_Amount3\":\"" + obj.applyMoney + "\"," +     //  本次申请金额
                //"\"FCONTACTUNITTYPE\":\"" + obj.unittype + "\"," +
                //"\"FCONTACTUNIT\":{\"FNumber\":\"" + obj.venNbr + "\"}," +
                //"\"FRECTUNITTYPE\":\"" + obj.unittype + "\"," +
                //"\"FRECTUNIT\":{\"FNumber\":\"" + obj.venNbr + "\"}," +
                //"\"FCURRENCYID\":{\"FNumber\":\"PRE001\"}," +
                //"\"FSETTLEORGID\":{\"FNumber\":\"100\"}," +
                //"\"FDEPARTMENT\":{\"FNumber\":\"BM000018\"}," +
                //"\"FDOCUMENTSTATUS\":\"B\"," +
                //"\"FScanPoint\":{\"FNumber\":\"\"}," +
                //"\"FCANCELSTATUS\":\"A\"," +
                //"\"F_KLM_Base\":{\"FNumber\":\"SFKYT015\"}," +
                //
                //    //"\"F_KLM_Entity\":[{\"F_KLM_Date\":\"1900-01-01\",\"F_KLM_Amount\":\"1000\",\"F_KLM_Amount1\":\"5000\",\"F_KLM_Decimal\":\"20\",\"F_KLM_Text3\":\"兴业银行\",\"F_KLM_Text4\":\"345436456\"}," +
                //    //"{\"F_KLM_Date\":\"1900-01-01\",\"F_KLM_Amount\":\"2000\",\"F_KLM_Amount1\":\"5000\",\"F_KLM_Decimal\":\"40\",\"F_KLM_Text3\":\"兴业银行\",\"F_KLM_Text4\":\"345436456\"}]," +


            "\"FEntityDetail\":[{\"FMATERIALID\":{\"FNumber\":\"gck\"}," +
            "\"FPriceQty\":\"1\"," +
                //FPrice
            "\"FPrice\":\"2000\"," +
            "\"FEntryTaxRate\":\"0\"," +
            "\"FTaxRateId\":{\"FNumber\":\"SL04_SYS\"},}]" +
            "}}"
        ]
    }
    logger.info(JSON.stringify(jsonPara));
    //let saveBack = request('POST', conf.webApi.saveFksqdUrl
    //    , {
    //        headers: {
    //            'Cookie': kdCooie
    //        },
    //        json: jsonPara
    //    });
    //let saveRsp = saveBack.getBody('utf8');
    let supplierNumber = null;
    let rspJson = null;
    try {
        //rspJson = JSON.parse(saveRsp);
        //logger.info(JSON.stringify(rspJson));
        //if (rspJson.Result.ResponseStatus.IsSuccess) {
        if (1 == 1) {
            logger.info("--- will start submit/audit fksqd ---");


            //保存完成之后,自动提交,自动审核
            try {
                //rspJson = JSON.parse(saveRsp);
                //supplierNumber = rspJson.Result.Number;
                logger.info(JSON.stringify(rspJson));

                let submitBack = request('POST', conf.webApi.submit
                    , {
                        headers: {
                            'Cookie': kdCooie
                        },
                        json: {
                            "parameters": [
                                "SD_FDC_HTHBD",
                                "{\"Numbers\":[\"HTHB2018081000002\"]}"
                            ]
                        }
                    }).getBody('utf8');
                logger.info(JSON.stringify({
                    headers: {
                        'Cookie': kdCooie
                    },
                    json: {
                        "parameters": [
                            "SD_FDC_HTHBD",
                            "{\"Numbers\":[\"HTHB2018080800001\"]}"
                        ]
                    }}))
                let submitRsp = null;
                try {
                    submitRsp = JSON.parse(submitRsp);
                    logger.info("submit : "+JSON.stringify(submitRsp))
                    let auditBack = request('POST', conf.webApi.audit
                        , {
                            headers: {
                                'Cookie': kdCooie
                            },
                            json: {
                                "parameters": [
                                    "SD_FDC_HTHBD",
                                    "{\"Numbers\":[\"HTHB2018080800001\"]}"
                                ]
                            }
                        }).getBody('utf8');

                    logger.info("audit : "+JSON.stringify(auditBack))

                } catch (e) {

                }
            } catch (e) {
                cb(e);
            }


            //try {
            //    logger.info("--- before sql ---");
            //    sql.connect({
            //            user: conf.db.username,
            //            password: conf.db.password,
            //            server: conf.db.hostname, // You can use 'localhost\\instance' to connect to named instance
            //            database: conf.db.database
            //        }
            //    ).then(pool => {
            //        logger.info("insert into T_FKD_SYNC_WISE values ('" + rspJson.Result.Id + "','" + obj.CONTRACT_ID + "','" + obj.conMoney + "','" + obj.FAPPLY_ID + "','" + obj.APPLY_CODE + "','W',GETDATE())");
            //        return pool.request().query("insert into T_FKD_SYNC_WISE values ('" + rspJson.Result.Id + "','" + obj.CONTRACT_ID + "','" + obj.conMoney + "','" + obj.FAPPLY_ID + "','" + obj.APPLY_CODE + "','W',GETDATE())");
            //    }).then(result => {
            //        logger.info("----------------------- sdfsgsdfg     :" + JSON.stringify(result));
            //        sql.close();
            //        cb(null, rspJson);
            //
            //        let createGroupBack = request('POST', 'https://oapi.dingtalk.com/message/send?access_token=' + token.token()
            //            , {
            //                json: {
            //                    "touser": conf.finNoticeUsr,
            //                    "agentid": conf.agentId,
            //                    "msgtype": "text",
            //                    "text": {
            //                        "content": "你好,付款申请单" + supplierNumber + "已审批通过,请进行后续处理"
            //                    }
            //                }
            //            });
            //
            //    }).catch(e=> {
            //        logger.info("----------------------- sdfsgsdfg     :" + e)
            //        cb(e);
            //    })
            //} catch (e) {
            //    logger.info("------------  abc   ----------- sdfsgsdfg     :" + e)
            //}
        } else {
            logger.info("-------------- in  cb callback --------- ");
            cb(rspJson);
        }

    } catch (e) {
        cb(e);
    }

    sql.on('error', function (e) {
        logger.info('-----------------------------');
        logger.info(e);
    })

}

module.exports = save;

//"\"F_KLM_Entity1\":[{\"F_KLM_Text6\":\"曾晋明\",\"F_KLM_Text7\":\"同意通过\",\"F_KLM_Time\":\"1900-01-01\"}],"+
