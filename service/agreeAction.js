/**
 * Created by zhoulanhong on 8/26/17.
 */
var sql = require('mssql');
var conf = require('../config/config');
var Id = require('../libs/Id');
var xmlUtil = require('../libs/xmlUtil');
var logger = require('../config/log4js').getLogger('agreeAction');
var noticeBk = require('./noticeBack');
var auditCloudOrder = require('../service/auditCloudOrder');

function action() {
}

function back(table, assignId, userId, procInstId, preArrprovId, remark, msg, msgName, keyValue,
              callBack) {

    if (sql) {
        sql.close();
    }

    logger.info("table " + table)
    logger.info("assignId " + assignId)
    logger.info("userId " + userId)
    logger.info("remark " + remark)
    logger.info("procInstId " + procInstId)
    logger.info("preArrprovId " + preArrprovId)

    var request = null;
    var trans = null;
    var pool = null;
    let nextAssignId = Date.now() + Id.get();
    let nextFacInstId = Date.now() + Id.get();
    let nextApprovItemId = Date.now() + Id.get();
    let xmlResult = "";
    let bakMsg = "";
    let notices = [];

    let realProcess = function (origId, origName) {
        request.query("update T_WF_ASSIGN set FCOMPLETEDTIME = GETDATE(), FSTATUS = 1 where FASSIGNID = '" + assignId + "'").then(result => {
            return request.query("update T_WF_ACTINST set FSTATUS = 1, FCOMPLETEDTIME = GETDATE() where FACTINSTID =" +
                "(select FACTINSTID from T_WF_ACTINST where FPROCINSTID = '" + procInstId + "' and FCOMPLETEDTIME is null)");
        }).then(result => {
            return request.query("update T_WF_APPROVALASSIGN set FRESULT = '02', FDISPOSITION = '" + remark + "', " +
                " FOPERATIONNUMBER = 'UnAudit' where FASSIGNID = '" + assignId + "'");
        }).then(result => {
            //先删除掉已存在的
            return request.query("delete T_WF_APPROVALITEM  where FAPPROVALASSIGNID = '" + preArrprovId + "' and freceiverid ='" + userId + "' ");
        }).then(result => {
            return request.query("insert into T_WF_APPROVALITEM values('" + nextApprovItemId
                + "', (select FAPPROVALASSIGNID from T_WF_APPROVALASSIGN where FASSIGNID = '" + assignId + "'), " +
                "GETDATE(), GETDATE(), '1', '1', " +
                "'" + userId + "', '02', '" + remark + "', '0', '0', '2', '2', '0')");
        }).then(result => {
            logger.info("insert into T_WF_APPROVALITEM_L values('" + (Date.now() + Id.get()) + "','" + nextApprovItemId
                + "','2052','打回发起人')");
            return request.query("insert into T_WF_APPROVALITEM_L values('" + (Date.now() + Id.get()) + "','" + nextApprovItemId
                + "','2052','打回发起人')");
        }).then(result => {

            logger.info("insert into T_WF_APPROVALASSIGN_L values('" + (Date.now() + Id.get())
                + "',(select FAPPROVALASSIGNID from T_WF_APPROVALASSIGN where FASSIGNID = '" + assignId + "'), '2052', '打回发起人', '')");


            return request.query("insert into T_WF_APPROVALASSIGN_L values('" + (Date.now() + Id.get())
                + "',(select FAPPROVALASSIGNID from T_WF_APPROVALASSIGN where FASSIGNID = '" + assignId + "'), '2052', '打回发起人', '')");
        }).then(result => {
            return request.query("select FACTINSTID,FACTIVITYID,FBACKACTINSTIDS from T_WF_ACTINST " +
                "where FPROCINSTID = '" + procInstId + "' and FLASTACTIONEVENTID > 0 order by FACTINSTID");
        }).then(actInstResult => {
            logger.info("actInstResult :" + JSON.stringify(actInstResult))
            let actSets = actInstResult.recordset;
            let bkactInst = getBackInsts(actSets)
            logger.info("insert into T_WF_ACTINST values ('" + nextFacInstId + "','" + procInstId + "','2','" + xmlResult.nextActivity + "','" + bkactInst + "'," +
                "GETDATE(),null,'" + xmlResult.curActionEventId + "','<VariableInstanceForSer><VariableInstances><VariableInstance><Name>ActionResult" +
                "</Name><Value><String>02</String></Value></VariableInstance></VariableInstances></VariableInstanceForSer>','')");
            return request.query("insert into T_WF_ACTINST values ('" + nextFacInstId + "','" + procInstId + "','2','" + xmlResult.nextActivity + "','" + bkactInst + "'," +
                "GETDATE(),null,'" + xmlResult.curActionEventId + "','<VariableInstanceForSer><VariableInstances><VariableInstance><Name>ActionResult" +
                "</Name><Value><String>02</String></Value></VariableInstance></VariableInstances></VariableInstanceForSer>','')");
        }).then(result => {
            logger.info("insert into T_WF_ASSIGN values ('" + nextAssignId + "','" + procInstId + "','" + nextFacInstId + "','" +
                userId + "',GETDATE(),null,'0','" + origName + "','" + preArrprovId + "','" + xmlResult.nextActionEventId + "','')");
            return request.query("insert into T_WF_ASSIGN values ('" + nextAssignId + "','" + procInstId + "','" + nextFacInstId + "','" +
                userId + "',GETDATE(),null,'0','" + origName + "','" + preArrprovId + "','" + xmlResult.nextActionEventId + "','')");
        }).then(result => {
            logger.info("insert into T_WF_ACTINST_L values ('" + (Date.now() + Id.get()) + "', '" + nextFacInstId +
                "','2052','打回发起人')");
            return request.query("insert into T_WF_ACTINST_L values ('" + (Date.now() + Id.get()) + "', '" + nextFacInstId +
                "','2052','打回发起人')");
        }).then(result => {
            logger.info("insert into T_WF_ACTINST_L values ('" + (Date.now() + Id.get()) + "', '" + nextFacInstId +
                "','" + xmlResult.nextNodeId + "','" + xmlResult.nextNodeName + "')");
            logger.info("--- back insert T_WF_ACTINST_L success : " + JSON.stringify(result));
            return request.query("insert into T_WF_APPROVALASSIGN (FAPPROVALASSIGNID,FASSIGNID,FOBJECTTYPEID,FKEYVALUE,FAPPROVALACTION,FVARIABLENAME,FDEFAULTRESULT)" +
                " values ('" + (Date.now() + Id.get()) + "','" + nextAssignId + "','" + table + "','" + keyValue +
                "','<AssignResultForSer><AssignResults><AssignResult><Id>01</Id><Name>/2052/重新提交/1033/Re-submit/3076/重新提交</Name>" +
                "<OperationKey>Submit</OperationKey> <Version>2</Version> <ApprovalType>Forward</ApprovalType> </AssignResult> " +
                "<AssignResult> <Id>02</Id> <Name>/2052/终止流程/1033/Terminate Flow/3076/終止流程</Name> <Version>2</Version> " +
                "<ApprovalType>Terminate</ApprovalType> </AssignResult> </AssignResults> </AssignResultForSer>','ActionResult','01')  ");
        }).then(result => {
            logger.info("insert into T_WF_RECEIVER (fid,FASSIGNID,FRECEIVERID,FTITLE,FCONTENT) " +
                "values ('" + (Date.now() + Id.get()) + "','" + nextAssignId + "','" + origId + "','您有待办任务需要处理','您有待办任务需要处理')");
            return request.query("insert into T_WF_RECEIVER (fid,FASSIGNID,FRECEIVERID,FTITLE,FCONTENT) " +
                "values ('" + (Date.now() + Id.get()) + "','" + nextAssignId + "','" + origId + "','您有待办任务需要处理','您有待办任务需要处理')");
        }).then(result => {

            if (table == "PAEZ_HY_HTYSD") {
                return request.query("update PAEZ_t_Cust_Entry100027 set FDOCUMENTSTATUS = 'D' where fid = '" + keyValue + "'");
            } else if (table == "PAEZ_HY_ZLHT") {
                return request.query("update T_KLM_LeaseContract set FDOCUMENTSTATUS = 'D' where fid = '" + keyValue + "'");
            } else if (table == "1c6e00b8e8fc4ca1a8086ad7e09d0437") {
                return request.query("update KLM_t_Cust_Entry100075 set FDOCUMENTSTATUS = 'D' where fid = '" + keyValue + "'");
            } else if (table == "SD_FDC_HT") {
                return request.query("update SD_FDC_HT set FDOCUMENTSTATUS = 'D' where fid = '" + keyValue + "'");
            } else if (table == "SD_FDC_HTHBD") {
                return request.query("update SD_FDC_HTHBD set FDOCUMENTSTATUS = 'D' where fid = '" + keyValue + "'");
            } else if (table == "SD_HT_BGQZ") {
                return request.query("update SD_HT_BGQZ set FDOCUMENTSTATUS = 'D' where fid = '" + keyValue + "'");
            } else if (table == "SD_FDC_JSD") {
                return request.query("update SD_FDC_JSD set FDOCUMENTSTATUS = 'D' where fid = '" + keyValue + "'");
            } else {
                return request.query("update T_" + table + " set FDOCUMENTSTATUS = 'D' where FID = " + keyValue);
            }
//'CN_PAYAPPLY', 'SAL_OUTSTOCK', 'PAEZ_HY_HTYSD', 'PAEZ_HY_ZLHT'
        }).then(result => {
            if (table == "PAEZ_HY_HTYSD") {
                logger.info("select b.FNAME, a.FBILLNO from PAEZ_t_Cust_Entry100027 a, T_SEC_USER b where a.FCREATORID = b.FUSERID and a.FID = '" + keyValue + "'");
                return request.query("select b.FNAME, a.FBILLNO from PAEZ_t_Cust_Entry100027 a, T_SEC_USER b where a.FCREATORID = b.FUSERID and a.FID = '" + keyValue + "'");
            } else if (table == "PAEZ_HY_ZLHT") {
                logger.info("select b.FNAME, a.FBILLNO from T_KLM_LeaseContract a, T_SEC_USER b where a.FCREATORID = b.FUSERID and a.FID = '" + keyValue + "'");
                return request.query("select b.FNAME, a.FBILLNO from T_KLM_LeaseContract a, T_SEC_USER b where a.FCREATORID = b.FUSERID and a.FID = '" + keyValue + "'");
            } else if (table == "CN_PAYAPPLY") {
                logger.info("select b.FNAME, a.FBILLNO from T_CN_PAYAPPLY a, T_SEC_USER b where a.FCREATORID = b.FUSERID and a.FID = '" + keyValue + "'");
                return request.query("select b.FNAME, a.FBILLNO from T_CN_PAYAPPLY a, T_SEC_USER b where a.FCREATORID = b.FUSERID and a.FID = '" + keyValue + "'");
            } else if (table == "SAL_OUTSTOCK") {
                logger.info("select b.FNAME, a.FBILLNO from T_SAL_OUTSTOCK a, T_SEC_USER b where a.FCREATORID = b.FUSERID and a.FID = '" + keyValue + "'");
                return request.query("select b.FNAME, a.FBILLNO from T_SAL_OUTSTOCK a, T_SEC_USER b where a.FCREATORID = b.FUSERID and a.FID = '" + keyValue + "'");
            } else {
                //callBack({});
                return;
            }

        }).then(result => {
            if (table == "PAEZ_HY_HTYSD") {
                bakMsg = result.recordset[0].FNAME + " 发起的合同预审单,编号:" + result.recordset[0].FBILLNO + " 被打回,原因是:" + remark;
            } else if (table == "PAEZ_HY_ZLHT") {
                bakMsg = result.recordset[0].FNAME + " 发起的租赁合同,单号:" + result.recordset[0].FBILLNO + " 被打回,原因是:" + remark;
            } else if (table == "CN_PAYAPPLY") {
                bakMsg = result.recordset[0].FNAME + " 发起的付款申请单,单号:" + result.recordset[0].FBILLNO + " 被打回,原因是:" + remark;
            } else if (table == "SAL_OUTSTOCK") {
                bakMsg = result.recordset[0].FNAME + " 发起的资产销售单,单号:" + result.recordset[0].FBILLNO + " 被打回,原因是:" + remark;
            } else {
                //callBack({});
                return;
            }
        }).then(result => {
            logger.info("select distinct c.FDINACT from T_WF_ASSIGN a, T_SEC_USER b, T_DIN_ACCT c, T_WF_RECEIVER d " +
                "where a.FPROCINSTID = '" + procInstId + "' and a.FASSIGNID = d.FASSIGNID and d.FRECEIVERID = b.FUSERID and b.FPHONE = c.FPHONE");
            return request.query("select distinct c.FDINACT from T_WF_ASSIGN a, T_SEC_USER b, T_DIN_ACCT c, T_WF_RECEIVER d " +
                "where a.FPROCINSTID = '" + procInstId + "' and a.FASSIGNID = d.FASSIGNID and d.FRECEIVERID = b.FUSERID and b.FPHONE = c.FPHONE");

        }).then(result=> {
            console.log("need notices : " + JSON.stringify(result));
            if (result.recordset.length > 0) {
                for (var j in result.recordset) {
                    notices.push(result.recordset[j].FDINACT);
                }
            }
            return trans.commit();
            //trans.rollback();
        }).then(r=> {
            pool.close();
            logger.info("------------------------ success and will notice back ----------------------")
            if (notices.length > 0) {
                noticeBk.cloud(bakMsg, notices, function (err, data) {
                    callBack(null, "success");
                });
            } else {
                callBack(null, "success");
            }
        }).catch(e=> {
            logger.info(e);
            trans.rollback();
            callBack(rberr);
        });
    }

    let getBackInsts = function (setLists) {
        let found = setLists[0];
        for (let x in setLists) {
            if (found.FBACKACTINSTIDS.length < setLists[x].FBACKACTINSTIDS.length) {
                found = setLists[x];
            }
        }
        return found.FBACKACTINSTIDS + found.FACTINSTID + ":" + found.FACTIVITYID + ",";
    }

    sql.connect({
            user: conf.db.username,
            password: conf.db.password,
            server: conf.db.hostname, // You can use 'localhost\\instance' to connect to named instance
            database: conf.db.database
        }
    ).then(conPool => {
        pool = conPool;
        pool.on('error', err => {
            logger.info(err);
            pool.close();
        });
        return new sql.Transaction(pool).begin();
    }).then(reTrans=> {
        trans = reTrans;
        request = new sql.Request(trans);
        return request.query(" select b.FDEFINITIONXML, c.FSTATUS, a.FORIGINATORID, d.FNAME from T_WF_PROCINST a,T_WF_PROCDEF b, T_WF_ASSIGN c, T_SEC_USER d " +
            "where a.FPROCDEFID = b.FPROCDEFID and a.FORIGINATORID = d.FUSERID " +
            "and a.FPROCINSTID = '" + procInstId + "' and a.FPROCINSTID = c.FPROCINSTID and c.FASSIGNID = '" + assignId + "'");
    }).then(defxml=> {
        if (defxml.recordset[0].FSTATUS == 1) {
            callBack(null, "此消息已审核");
        } else {
            xmlResult = xmlUtil.findNextStep(defxml.recordset[0].FDEFINITIONXML, userId, "打回发起人");

            realProcess(defxml.recordset[0].FORIGINATORID, defxml.recordset[0].FNAME);
        }
    }).catch(e=> {
        logger.info("------" + JSON.stringify(e))
        callBack(e)
    });

}

function agree(table, assignId, userId, procInstId, preArrprovId, remark, msg, msgName, keyValue,
               callBack) {

    if (sql) {
        sql.close();
    }

    logger.info("table " + table)
    logger.info("assignId " + assignId)
    logger.info("userId " + userId)
    logger.info("remark " + remark)
    logger.info("procInstId " + procInstId)
    logger.info("preArrprovId " + preArrprovId)

    var request = null;
    var trans = null;
    var pool = null;
    let nextAssignId = Date.now() + Id.get();
    let nextFacInstId = Date.now() + Id.get();
    let nextApprovItemId = Date.now() + Id.get();
    let xmlResult = "";

    let middle = function () {
        request.query("update T_WF_ASSIGN set FCOMPLETEDTIME = GETDATE(), FSTATUS = 1 where FASSIGNID = '" + assignId + "'").then(result => {
            return request.query("update T_WF_ACTINST set FSTATUS = 1, FCOMPLETEDTIME = GETDATE() where FACTINSTID =" +
                "(select FACTINSTID from T_WF_ACTINST where FPROCINSTID = '" + procInstId + "' and FCOMPLETEDTIME is null)");
        }).then(result => {
            return request.query("update T_WF_APPROVALASSIGN set FRESULT = '01', FDISPOSITION = '" + remark + "' where FASSIGNID = '" + assignId + "'");
        }).then(result => {
            //先删除掉已存在的
            return request.query("delete T_WF_APPROVALITEM  where FAPPROVALASSIGNID = '" + preArrprovId + "' and freceiverid ='" + userId + "' ");
        }).then(result => {
            return request.query("insert into T_WF_APPROVALITEM values('" + nextApprovItemId
                + "', (select FAPPROVALASSIGNID from T_WF_APPROVALASSIGN where FASSIGNID = '" + assignId + "'), " +
                "GETDATE(), GETDATE(), '1', '1', " +
                "'" + userId + "', '01', '" + remark + "', '0', '0', '2', '1', '0')");
        }).then(result => {
            logger.info("insert into T_WF_APPROVALITEM_L values('" + (Date.now() + Id.get()) + "','" + nextApprovItemId
                + "','2052','审核通过')");
            return request.query("insert into T_WF_APPROVALITEM_L values('" + (Date.now() + Id.get()) + "','" + nextApprovItemId
                + "','2052','审核通过')");
        }).then(result => {
            console.log("insert into T_WF_APPROVALASSIGN_L values('" + (Date.now() + Id.get())
                + "',(select FAPPROVALASSIGNID from T_WF_APPROVALASSIGN where FASSIGNID = '" + assignId + "'), '2052', '审核通过', '')");
            return request.query("insert into T_WF_APPROVALASSIGN_L values('" + (Date.now() + Id.get())
                + "',(select FAPPROVALASSIGNID from T_WF_APPROVALASSIGN where FASSIGNID = '" + assignId + "'), '2052', '审核通过', '')");
        }).then(result => {
            return request.query("select FACTINSTID,FACTIVITYID,FBACKACTINSTIDS from T_WF_ACTINST " +
                "where FPROCINSTID = '" + procInstId + "' and FLASTACTIONEVENTID > 0 order by FACTINSTID");
        }).then(actInstResult => {
            logger.info("actInstResult :" + JSON.stringify(actInstResult))
            let actSets = actInstResult.recordset;
            let bkactInst = getBackInsts(actSets)

            console.log("insert into T_WF_ACTINST values ('" + nextFacInstId + "','" + procInstId + "','2','" + xmlResult.nextActivity + "','" + bkactInst + "'," +
                "GETDATE(),null,'" + xmlResult.curActionEventId + "','<VariableInstanceForSer><VariableInstances><VariableInstance><Name>ActionResult" +
                "</Name><Value><String>01</String></Value></VariableInstance></VariableInstances></VariableInstanceForSer>','')");

            return request.query("insert into T_WF_ACTINST values ('" + nextFacInstId + "','" + procInstId + "','2','" + xmlResult.nextActivity + "','" + bkactInst + "'," +
                "GETDATE(),null,'" + xmlResult.curActionEventId + "','<VariableInstanceForSer><VariableInstances><VariableInstance><Name>ActionResult" +
                "</Name><Value><String>01</String></Value></VariableInstance></VariableInstances></VariableInstanceForSer>','')");
        }).then(result => {
            console.log("insert into T_WF_ASSIGN values ('" + nextAssignId + "','" + procInstId + "','" + nextFacInstId + "','" +
                userId + "',GETDATE(),null,'0','" + xmlResult.Name + "','" + preArrprovId + "','" + xmlResult.nextActionEventId + "','')");
            return request.query("insert into T_WF_ASSIGN values ('" + nextAssignId + "','" + procInstId + "','" + nextFacInstId + "','" +
                userId + "',GETDATE(),null,'0','" + xmlResult.Name + "','" + preArrprovId + "','" + xmlResult.nextActionEventId + "','')");
        }).then(result => {
            console.log("insert into T_WF_ACTINST_L values ('" + (Date.now() + Id.get()) + "', '" + nextFacInstId +
                "','" + xmlResult.nextNodeId + "','" + xmlResult.nextNodeName + "')");
            return request.query("insert into T_WF_ACTINST_L values ('" + (Date.now() + Id.get()) + "', '" + nextFacInstId +
                "','" + xmlResult.nextNodeId + "','" + xmlResult.nextNodeName + "')");
        }).then(result => {
            console.log("  insert into T_WF_APPROVALASSIGN (FAPPROVALASSIGNID,FASSIGNID,FOBJECTTYPEID,FKEYVALUE,FAPPROVALACTION,FVARIABLENAME,FDEFAULTRESULT)" +
                " values ('" + (Date.now() + Id.get()) + "','" + nextAssignId + "','" + table + "','" + keyValue +
                "','<AssignResultForSer> <AssignResults> <AssignResult> <Id>01</Id> " +
                "<Name>/2052/审核通过/1033/Approval Successful/3076/審核通過</Name> <Version>2</Version> " +
                "<ApprovalType>Forward</ApprovalType> </AssignResult> <AssignResult> <Id>02</Id> " +
                "<Name>/2052/打回发起人/1033/Back to Initiator/3076/打回發起人</Name> <OperationKey>UnAudit</OperationKey> " +
                "<Version>2</Version> <ApprovalType>Reject</ApprovalType> </AssignResult> <AssignResult> <Id>03</Id> " +
                "<Name>/2052/终止流程/1033/Terminate Flow/3076/終止流程</Name> <OperationKey>UnAudit</OperationKey> <Version>2</Version> " +
                "<ApprovalType>Terminate</ApprovalType> </AssignResult> </AssignResults> </AssignResultForSer>','ActionResult','01')  ");
            return request.query("  insert into T_WF_APPROVALASSIGN (FAPPROVALASSIGNID,FASSIGNID,FOBJECTTYPEID,FKEYVALUE,FAPPROVALACTION,FVARIABLENAME,FDEFAULTRESULT)" +
                " values ('" + (Date.now() + Id.get()) + "','" + nextAssignId + "','" + table + "','" + keyValue +
                "','<AssignResultForSer> <AssignResults> <AssignResult> <Id>01</Id> " +
                "<Name>/2052/审核通过/1033/Approval Successful/3076/審核通過</Name> <Version>2</Version> " +
                "<ApprovalType>Forward</ApprovalType> </AssignResult> <AssignResult> <Id>02</Id> " +
                "<Name>/2052/打回发起人/1033/Back to Initiator/3076/打回發起人</Name> <OperationKey>UnAudit</OperationKey> " +
                "<Version>2</Version> <ApprovalType>Reject</ApprovalType> </AssignResult> <AssignResult> <Id>03</Id> " +
                "<Name>/2052/终止流程/1033/Terminate Flow/3076/終止流程</Name> <OperationKey>UnAudit</OperationKey> <Version>2</Version> " +
                "<ApprovalType>Terminate</ApprovalType> </AssignResult> </AssignResults> </AssignResultForSer>','ActionResult','01')  ");
        }).then(result => {
            console.log("insert into T_WF_RECEIVER (fid,FASSIGNID,FRECEIVERID,FTITLE,FCONTENT) " +
                "values ('" + (Date.now() + Id.get()) + "','" + nextAssignId + "','" + xmlResult.Id + "','" + msg + "','" + (msg + msgName) + "')");
            return request.query("insert into T_WF_RECEIVER (fid,FASSIGNID,FRECEIVERID,FTITLE,FCONTENT) " +
                "values ('" + (Date.now() + Id.get()) + "','" + nextAssignId + "','" + xmlResult.Id + "','" + msg + "','" + (msg + msgName) + "')");
        }).then(result => {
            return trans.commit();
        }).then(result=> {
            pool.close();
            callBack(null, "success");
        }).catch(e=> {
            logger.info(e);
            if (trans) {
                trans.rollback();
            }
            callBack(e);
        });
    }

    let complete = function () {

        sql.connect({
                user: conf.db.username,
                password: conf.db.password,
                server: conf.db.hostname, // You can use 'localhost\\instance' to connect to named instance
                database: conf.db.database
            }
        ).then(conPool => {
            pool = conPool;
            pool.on('error', err => {
                logger.info(err);
                pool.close();
            });
            return new sql.Transaction(pool).begin();
        }).then(reTrans=> {
                trans = reTrans;
                request = new sql.Request(trans);
                console.log("--- new collections connected ----");
                return request.query("update T_WF_ASSIGN set FCOMPLETEDTIME = GETDATE(), FSTATUS = 1 where FASSIGNID = '" + assignId + "'");
            })
            .then(result => {
                return request.query("update T_WF_ACTINST set FSTATUS = 1, FCOMPLETEDTIME = GETDATE() where FACTINSTID =" +
                    "(select FACTINSTID from T_WF_ACTINST where FPROCINSTID = '" + procInstId + "' and FCOMPLETEDTIME is null)");
            }).then(result => {
            return request.query("update T_WF_APPROVALASSIGN set FRESULT = '01', FDISPOSITION = '" + remark + "' where FASSIGNID = '" + assignId + "'");
        }).then(result => {
            //先删除掉已存在的
            return request.query("delete T_WF_APPROVALITEM  where FAPPROVALASSIGNID = '" + preArrprovId + "' and freceiverid ='" + userId + "' ");
        }).then(result => {
            logger.info("insert into T_WF_APPROVALITEM values('" + nextApprovItemId
                + "', (select FAPPROVALASSIGNID from T_WF_APPROVALASSIGN where FASSIGNID = '" + assignId + "'), " +
                "GETDATE(), GETDATE(), '1', '1', '" + userId + "', '01', '" + remark + "', '0', '0', '2', '1', '0')");
            return request.query("insert into T_WF_APPROVALITEM values('" + nextApprovItemId
                + "', (select FAPPROVALASSIGNID from T_WF_APPROVALASSIGN where FASSIGNID = '" + assignId + "'), " +
                "GETDATE(), GETDATE(), '1', '1', '" + userId + "', '01', '" + remark + "', '0', '0', '2', '1', '0')");
        }).then(result => {
            logger.info("insert into T_WF_APPROVALITEM_L values('" + (Date.now() + Id.get()) + "','" + nextApprovItemId
                + "','2052','流程结束')");
            return request.query("insert into T_WF_APPROVALITEM_L values('" + (Date.now() + Id.get()) + "','" + nextApprovItemId
                + "','2052','流程结束')");
        }).then(result => {
            logger.info("insert into T_WF_APPROVALASSIGN_L values('" + (Date.now() + Id.get())
                + "',(select FAPPROVALASSIGNID from T_WF_APPROVALASSIGN where FASSIGNID = '" + assignId + "'), '2052', '审核通过', '')");
            return request.query("insert into T_WF_APPROVALASSIGN_L values('" + (Date.now() + Id.get())
                + "',(select FAPPROVALASSIGNID from T_WF_APPROVALASSIGN where FASSIGNID = '" + assignId + "'), '2052', '审核通过', '')");
        }).then(result => {
            return request.query("select FACTINSTID,FACTIVITYID,FBACKACTINSTIDS from T_WF_ACTINST " +
                "where FPROCINSTID = '" + procInstId + "' order by FACTINSTID");
        }).then(actInstResult => {
            logger.info("actInstResult :" + JSON.stringify(actInstResult))
            let actSets = actInstResult.recordset;
            let bkactInst = getBackInsts(actSets)

            console.log("insert into T_WF_ACTINST values ('" + nextFacInstId + "','" + procInstId + "','1','" + xmlResult.nextActivity + "','" + bkactInst + "'," +
                "GETDATE(),GETDATE(),'" + xmlResult.curActionEventId + "','<VariableInstanceForSer><VariableInstances><VariableInstance><Name>ActionResult" +
                "</Name><Value><String>01</String></Value></VariableInstance></VariableInstances></VariableInstanceForSer>','')");

            return request.query("insert into T_WF_ACTINST values ('" + nextFacInstId + "','" + procInstId + "','1','" + xmlResult.nextActivity + "','" + bkactInst + "'," +
                "GETDATE(),GETDATE(),'" + xmlResult.curActionEventId + "','<VariableInstanceForSer><VariableInstances><VariableInstance><Name>ActionResult" +
                "</Name><Value><String>01</String></Value></VariableInstance></VariableInstances></VariableInstanceForSer>','')");
        }).then(result => {
            logger.info("insert into T_WF_ACTINST fet :" + JSON.stringify(result))
            return request.query("update T_WF_PROCINST set FCOMPLETETIME = GETDATE(), FSTATUS = 1 where FPROCINSTID ='" + procInstId + "'");
        }).then(result => {
            logger.info(" update  T_WF_PROCINST fet :" + JSON.stringify(result))
            if (table == "PAEZ_HY_HTYSD") {
                return request.query("update PAEZ_t_Cust_Entry100027 set FDOCUMENTSTATUS = 'C' where fid = '" + keyValue + "'");
            } else if (table == "PAEZ_HY_ZLHT") {
                return request.query("update T_KLM_LeaseContract set FDOCUMENTSTATUS = 'C' where fid = '" + keyValue + "'");
            } else if (table == "1c6e00b8e8fc4ca1a8086ad7e09d0437") {
                return request.query("update KLM_t_Cust_Entry100075 set FDOCUMENTSTATUS = 'C' where fid = '" + keyValue + "'");
            } else if (table == "SD_FDC_HT") {
                return request.query("update SD_FDC_HT set FDOCUMENTSTATUS = 'C' where fid = '" + keyValue + "'");
            } else if (table == "SD_FDC_HTHBD") {
                return request.query("update SD_FDC_HTHBD set FDOCUMENTSTATUS = 'C' where fid = '" + keyValue + "'");
            } else if (table == "SD_HT_BGQZ") {
                return request.query("update SD_HT_BGQZ set FDOCUMENTSTATUS = 'C' where fid = '" + keyValue + "'");
            } else if (table == "SD_FDC_JSD") {
                return request.query("update SD_FDC_JSD set FDOCUMENTSTATUS = 'C' where fid = '" + keyValue + "'");
            } else if (table == "PUR_PurchaseOrder") {
                return request.query("update T_PUR_POORDER set FDOCUMENTSTATUS = 'C' where fid = '" + keyValue + "'");
            }
            else {
                return request.query("update T_" + table + " set FDOCUMENTSTATUS = 'C' , FAPPROVERID = '" + userId + "', FAPPROVEDATE = GETDATE() where fid = '" + keyValue + "'");
            }
        }).then(result => {
            console.log("++++++++++++++ commit +++++++++++++++++");
            return trans.commit();
        }).then(result=> {
            pool.close();
            callBack(null, "success");
        }).catch(e=> {
            logger.info(e);
            if (trans) {
                trans.rollback();
            }
            callBack(e);
        });
    }

    let getBackInsts = function (setLists) {
        let found = setLists[0];
        for (let x in setLists) {
            if (found.FBACKACTINSTIDS.length < setLists[x].FBACKACTINSTIDS.length) {
                found = setLists[x];
            }
        }
        return found.FBACKACTINSTIDS + found.FACTINSTID + ":" + found.FACTIVITYID + ",";
    }

    sql.connect({
            user: conf.db.username,
            password: conf.db.password,
            server: conf.db.hostname, // You can use 'localhost\\instance' to connect to named instance
            database: conf.db.database
        }
    ).then(conPool => {
        pool = conPool;
        pool.on('error', err => {
            logger.info(err);
            pool.close();
        });
        return new sql.Transaction(pool).begin();
    }).then(reTrans=> {
        trans = reTrans;
        request = new sql.Request(trans);

        console.log("select a.FNUMBER, b.FOBJECTTYPEID, b.FDEFINITIONXML, c.FSTATUS from T_WF_PROCINST " +
            "a,T_WF_PROCDEF b, T_WF_ASSIGN c where a.FPROCDEFID = b.FPROCDEFID " +
            "and a.FPROCINSTID = '" + procInstId + "' and a.FPROCINSTID = c.FPROCINSTID and c.FASSIGNID = '" + assignId + "'");

        return request.query("select a.FNUMBER, b.FOBJECTTYPEID, b.FDEFINITIONXML, c.FSTATUS from T_WF_PROCINST " +
            "a,T_WF_PROCDEF b, T_WF_ASSIGN c where a.FPROCDEFID = b.FPROCDEFID " +
            "and a.FPROCINSTID = '" + procInstId + "' and a.FPROCINSTID = c.FPROCINSTID and c.FASSIGNID = '" + assignId + "'");
    }).then(defxml=> {

        //console.log("--------- defxml : " + JSON.stringify(defxml));

        if (defxml.recordset[0].FSTATUS == 1) {
            callBack(null, "此消息已审核");
        } else {
            xmlResult = xmlUtil.findNextStep(defxml.recordset[0].FDEFINITIONXML, userId, "审核通过");

            //logger.info("xmlResult : " + JSON.stringify(xmlResult));

            //已经完成了审核的最后一步
            if (xmlResult.Id == 0) {
                //支付
                //if (defxml.recordset[0].FOBJECTTYPEID == "SD_FDC_HTHBD") {
                let nubmer = (defxml.recordset[0].FNUMBER.split("_"))[0];
                logger.info("---in complete ------" + nubmer);
                auditCloudOrder(
                    {
                        "parameters": [
                            defxml.recordset[0].FOBJECTTYPEID,
                            //"{\"Numbers\":[\"+nubmer+\"]}"
                            "{\"Numbers\":[\"" + nubmer + "\"]}"
                        ]
                    }
                    , function (err, data) {
                        if (err) {
                            res.render("error", err);
                        } else {
                            console.log("------ will start complete ------------ ");
                            complete();
                            //callBack(null, "success");
                        }
                    });
                //} else {
                //    complete();
                //}
            } else {
                logger.info("---in middle ------" + xmlResult.Id);
                middle();
                //return request.query("update T_WF_ASSIGN set FCOMPLETEDTIME = GETDATE(), FSTATUS = 1 where FASSIGNID = '" + assignId + "'");
            }
        }
    }).catch(e=> {
        logger.info(e);
        callBack(e);
    });

}

action.agree = agree;
action.back = back;
module.exports = action;