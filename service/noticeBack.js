/**
 * Created by zhoulanhong on 9/29/17.
 */
var wiseSql = require('mssql');
var conf = require('../config/config');
var logger = require('../config/log4js').getLogger('noticeBack');
var request = require('sync-request');
var token = require('../libs/token');

var notice = function () {

}

function cloud(msg, lists, cb) {
    logger.info("--- in cloud ---" + msg);
    cb();
    if (lists.length > 0) {

        for (let k = 0; k < lists.length; k++) {
            request('POST', 'https://oapi.dingtalk.com/message/send?access_token=' + token.token()
                , {
                    json: {
                        "touser": lists[k],
                        "agentid": conf.agentId,
                        "msgtype": "text",
                        "text": {
                            "content": msg
                        }
                    }
                }).getBody('utf8');
        }
    }

}

function wise(msg, wfId, cb) {
    if (wiseSql) {
        wiseSql.close();
    }
    wiseSql.connect({
            user: conf.htdb.username,
            password: conf.htdb.password,
            server: conf.htdb.hostname, // You can use 'localhost\\instance' to connect to named instance
            database: conf.htdb.database
        }
    ).then(pool => {
        logger.info("select distinct c.FDINACT from T_SYS_WF_TASK a,T_SYS_USERINFO b, T_DIN_ACCT c  where a.wf_id ='" + wfId
            + "' and a.ACTOR_ID = b.ORG_ID and b.MOBILE = c.FPHONE");
        return pool.request().query("select c.FDINACT from T_SYS_WF_TASK a,T_SYS_USERINFO b, T_DIN_ACCT c  where a.wf_id ='" + wfId
            + "' and a.ACTOR_ID = b.ORG_ID and b.MOBILE = c.FPHONE");
    }).then(lists=> {
        cb(null, {});
        if (lists.recordset.length > 0) {

            for (let k = 0; k < lists.recordset.length; k++) {
                request('POST', 'https://oapi.dingtalk.com/message/send?access_token=' + token.token()
                    , {
                        json: {
                            "touser": lists.recordset[k].FDINACT,
                            "agentid": conf.agentId,
                            "msgtype": "text",
                            "text": {
                                "content": msg
                            }
                        }
                    }).getBody('utf8');
            }
        }

    }).catch(e=> {
        logger.error(e);
    });
}

notice.wise = wise;
notice.cloud = cloud;

module.exports = notice;