/**
 * Created by zhoulanhong on 9/14/17.
 * 保存供应商
 */

var request = require('sync-request');
var conf = require('../config/config');
var sql = require('mssql');

var logger = require('../config/log4js').getLogger('saveFksqd');

function save(obj, cb) {

    if (sql) {
        sql.close();
    }

    let loginBack = request('POST', conf.webApi.loginUrl
        , {
            json: {
                "parameters": [
                    conf.webApi.dbCenterId,
                    conf.webApi.userName,
                    conf.webApi.passwd,
                    conf.webApi.lang
                ]
            }
        });
    let rsp = loginBack.getBody('utf8');
    logger.info(JSON.stringify(rsp));
    let kdCooie = null;
    logger.info(JSON.stringify(kdCooie));
    let rspJsonObj = null;
    try {
        logger.info(loginBack.headers);
        let ckstr = JSON.stringify(loginBack.headers);//.substring(loginBack.headers.toString().indexOf('set-cookie'));
        logger.info(ckstr);
        logger.info(ckstr.substring(ckstr.indexOf('kdservice-sessionid')));
        ckstr = (ckstr.substring(ckstr.indexOf('kdservice-sessionid'))).split(";");
        kdCooie = ckstr[0];
        logger.info("kdCooie" + kdCooie);
        rspJsonObj = JSON.parse(rsp);
        if (rspJsonObj.LoginResultType != "1") {
            cb({
                message: "登陆k3cloud出错",
                error: {
                    status: 0,
                    stack: rspJsonObj.Message
                }
            })
        }
    } catch (e) {
        logger.info(e);
        cb(e);
        return;
    }

    let saveBack = request('POST', conf.webApi.saveFksqdUrl
        , {
            headers: {
                'Cookie': kdCooie
            },
            json: {
                "parameters": [
                    "BD_Supplier",
                    "{\"Model\":{" +
                    "\"FCreateOrgId\":{\"FNumber\":\"100\"}," +
                    "\"FUseOrgId\":{\"FNumber\":\"100\"}," +


                    "\"FBankInfo\":[{\"FOpenBankName\":\"" + obj.bnkName + "\",\"FBankCode\":\"" + obj.bnkNbr + "\"}]," +

//"\"FLocationInfo\":[{\"FLocName\":\"hello\",    \"FLocNewContact\":{\"FNumber\":\"adgfgds\"},      " +
//                    "\"FLocAddress\":\"world\",\"FLocMobile\":\"18582572319\"}],"+

                    "\"FName\":\"" + obj.providerName + "\",}}"
                ]
            }
        });


    let saveRsp = saveBack.getBody('utf8');
    logger.info(saveRsp);
    let rspJson = null;
    let supplierNumber = null;
    try {
        rspJson = JSON.parse(saveRsp);
        supplierNumber = rspJson.Result.Number;
        logger.info(JSON.stringify(rspJson));

        let submitBack = request('POST', conf.webApi.submit
            , {
                headers: {
                    'Cookie': kdCooie
                },
                json: {
                    "parameters": [
                        "BD_Supplier",
                        "{\"Numbers\":[\"" + supplierNumber + "\"]}"
                    ]
                }
            }).getBody('utf8');

        let submitRsp = null;
        try {
            submitRsp = JSON.parse(submitRsp);
            console.log(JSON.stringify(submitRsp))
            let auditBack = request('POST', conf.webApi.audit
                , {
                    headers: {
                        'Cookie': kdCooie
                    },
                    json: {
                        "parameters": [
                            "BD_Supplier",
                            "{\"Numbers\":[\"" + supplierNumber + "\"]}"
                        ]
                    }
                }).getBody('utf8');

            let auditRsp = null;
            try {
                auditRsp = JSON.parse(auditBack);
                console.log(JSON.stringify(auditRsp))

                try {
                    sql.connect({
                            user: conf.htdb.username,
                            password: conf.htdb.password,
                            server: conf.htdb.hostname, // You can use 'localhost\\instance' to connect to named instance
                            database: conf.htdb.database
                        }
                    ).then(pool => {
                        logger.info("insert into T_GYS_SYNC_WISE  values ('" + obj.providerId + "','" + obj.providerName
                            + "','" + obj.bnkNbr + "','" + obj.bnkName + "','" + auditRsp.Result.ResponseStatus.SuccessEntitys[0].Id
                            + "','" + auditRsp.Result.ResponseStatus.SuccessEntitys[0].Number + "', GETDATE())");
                        return pool.request().query("insert into T_GYS_SYNC_WISE  values ('" + obj.providerId + "','" + obj.providerName
                            + "','" + obj.bnkNbr + "','" + obj.bnkName + "','" + auditRsp.Result.ResponseStatus.SuccessEntitys[0].Id
                            + "','" + auditRsp.Result.ResponseStatus.SuccessEntitys[0].Number + "', GETDATE())");
                    }).then(result => {
                        logger.info("----------------------- sdfsgsdfg     :" + JSON.stringify(result));
                        sql.close();
                        cb(null, auditRsp);
                    }).catch(e=> {
                        logger.info("----------------------- sdfsgsdfg     :" + e)
                        cb(e);
                    })
                } catch (e) {
                    logger.info("------------  abc   ----------- sdfsgsdfg     :" + e)
                }
                //cb(null, auditRsp);
            } catch (e) {

            }
        } catch (e) {

        }

    } catch (e) {
        cb(e);
    }

}

module.exports = save;