/**
 * Created by zhoulanhong on 8/26/17.
 */
var conf = require('../config/config');
var sql = require('mssql');
var cmoment = require('moment');
var logger = require('../config/log4js').getLogger('queryDetail');

var detail = function () {
}
function query(tableName, assignId, phonenbr, only, callBack) {

    logger.info("-------------- tablename : " + tableName + "  assignId:" + assignId + "  phonenbr:" + phonenbr + "    only:" + only);

    let supportTable = ['CN_PAYAPPLY', 'SAL_OUTSTOCK', 'PAEZ_HY_HTYSD', 'PAEZ_HY_ZLHT',
        '1c6e00b8e8fc4ca1a8086ad7e09d0437', 'SD_FDC_HTHBD', 'SD_FDC_HT', 'SD_HT_BGQZ',
        'SD_FDC_JSD', 'PUR_Requisition', 'PUR_PurchaseOrder', 'STK_InStock']

    if (sql) {
        sql.close();
    }

    if (supportTable.indexOf(tableName) < 0) {
        callBack({
            message: "暂不支持付款单以外的审批类型",
            error: {
                status: "错误",
                status: "正在开发中"
            }
        })
        return;
    }

    let ret = {
        title: '浩源审核',
        table: tableName,
        assignid: assignId
    };
    let pool = null;
    let queryReceiveDetail = function (key, bottom) {
        pool.request().query("select a.FRECEIVEAMOUNT, b.FNAME from T_" + tableName + " a, T_HR_EMPINFO_L b " +
            "where a. fid = '" + key + "' and a.FPAYUNIT = b.FID").then(result => {

            logger.info("detail result " + JSON.stringify(result));

            callBack(null, ret);
        });

    }


    //采购入库
    let queryCGRKDetail = function (key, bottom) {
        try {
            logger.info("============ query t_STK_InStock ======================" + key)
            let result = null;
            pool.request().query("select a.FBILLNO , a.FDATE, c.FNAME as GYS, d.FNAME as SLBM, f.FNAME as CGY, h.FNAME as CGY, t.FNAME, " +
                "i.FNUMBER as WLBM, j.FNAME as WLMC, k.FNUMBER as JLDW, l.FF100007 as CZ , l.FF100011 as GGXH, l.FF100009 PC, l.FF100010 YS, b.FREALQTY SSSL, b.FMUSTQTY as YSSL, m.FNAME as CK " +
                "from t_STK_InStock a left join T_BD_DEPARTMENT_L d on a.FSTOCKDEPTID = d.FDEPTID " +
                "left join T_BD_OPERATORENTRY e on a.FSTOCKERID = e.FENTRYID " +
                "left join T_BD_STAFF_L f on f.FSTAFFID = e.FSTAFFID " +
                "left join T_BD_OPERATORENTRY g on a.FPURCHASERID = g.FENTRYID " +
                "left join T_BD_STAFF_L h on g.FSTAFFID = h.FSTAFFID, T_STK_INSTOCKENTRY b " +
                "left join T_BD_FLEXSITEMDETAILV l on b.FAUXPROPID = l.FID " +
                "left join T_BD_STOCK_L m on b.FSTOCKID = m.FSTOCKID , T_BD_SUPPLIER_L c, T_BD_MATERIAL i, T_BD_MATERIAL_L j, T_BD_UNIT k, T_SEC_USER t " +
                "where a.FID = b.FID and a.FID = '" + key + "' and a.FSUPPLIERID = c.FSUPPLIERID and a.FCREATORID = t.FUSERID " +
                "and b.FMATERIALID = i.FMATERIALID and i.FMATERIALID = j.FMATERIALID and b.FUNITID = k.FUNITID").then(result => {
                logger.info("select a.FBILLNO , a.FDATE, c.FNAME as GYS, d.FNAME as SLBM, f.FNAME as CGY, h.FNAME as CGY, " +
                    "i.FNUMBER as WLBM, j.FNAME as WLMC, k.FNUMBER as JLDW, l.FF100005, b.FREALQTY SSSL, b.FMUSTQTY as YSSL, m.FNAME as CK " +
                    "from t_STK_InStock a left join T_BD_DEPARTMENT_L d on a.FSTOCKDEPTID = d.FDEPTID " +
                    "left join T_BD_OPERATORENTRY e on a.FSTOCKERID = e.FENTRYID " +
                    "left join T_BD_STAFF_L f on f.FSTAFFID = e.FSTAFFID " +
                    "left join T_BD_OPERATORENTRY g on a.FPURCHASERID = g.FENTRYID " +
                    "left join T_BD_STAFF_L h on g.FSTAFFID = h.FSTAFFID, T_STK_INSTOCKENTRY b " +
                    "left join T_BD_FLEXSITEMDETAILV l on b.FAUXPROPID = l.FID " +
                    "left join T_BD_STOCK_L m on b.FSTOCKID = m.FSTOCKID , T_BD_SUPPLIER_L c, T_BD_MATERIAL i, T_BD_MATERIAL_L j, T_BD_UNIT k " +
                    "where a.FID = b.FID and a.FID = '" + key + "' and a.FSUPPLIERID = c.FSUPPLIERID " +
                    "and b.FMATERIALID = i.FMATERIALID and i.FMATERIALID = j.FMATERIALID and b.FUNITID = k.FUNITID");
                let middle = [
                    {title: "单据类型", text: "入库申请单"},
                    {title: "单据编号", text: result.recordset[0].FBILLNO},
                    {
                        title: "入库日期",
                        text: cmoment(result.recordset[0].FDATE).utcOffset("-00:00").format('YYYY-MM-DD')
                    },
                    {title: "收料部门", text: result.recordset[0].SLBM},
                    {title: "仓管员", text: result.recordset[0].CGY},
                    {title: "采购员", text: result.recordset[0].CGY},
                    {title: "供应商", text: result.recordset[0].GYS},

                ];

                let allRecords = result.recordset;
                for (let ii = 0; ii < allRecords.length; ii++) {
                    middle.push({title: "明细信息(" + (ii + 1) + ")", text: ""});
                    middle.push({title: "物料编码", text: result.recordset[ii].WLBM});
                    middle.push({title: "物料名称", text: result.recordset[ii].WLMC});
                    middle.push({title: "材质", text: result.recordset[ii].CZ});
                    middle.push({title: "要求说明", text: result.recordset[ii].GGXH});
                    middle.push({title: "品牌", text: result.recordset[ii].PC});
                    middle.push({title: "颜色", text: result.recordset[ii].YS});
                    middle.push({title: "应收数量", text: result.recordset[ii].YSSL});
                    middle.push({title: "实收数量", text: result.recordset[ii].SSSL});
                    middle.push({title: "计量单位", text: result.recordset[ii].JLDW});
                    middle.push({title: "仓库", text: result.recordset[ii].CK});
                }

                ret.page = {
                    top: {
                        name: result.recordset[0].FNAME,
                        fullname: result.recordset[0].FNAME,
                        status: result.recordset[0].FDOCUMENTSTATUS == "C" ? "审核完成" : "正在审核"
                    },
                    middle: middle,
                    bottom: bottom
                }
                callBack(null, ret);
            }).catch(e=> {
                logger.info(e);
                callBack(e);
            });
        } catch (e) {
            logger.info(e);
        }
    }


    //处理订单
    let queryDDDetail = function (key, bottom) {
        try {
            logger.info("============ query T_PUR_POORDER ======================" + key)
            let result = null;
            pool.request().query("select a.FBILLNO, c.FNAME as CGBM, d.FNAME as CGY, e.FNAME GYS, a.FDATE, g.FNUMBER as DW, h.FNUMBER, t.FNAME, " +
                "i.FMATERIALDESC, j.FF100007 as CZ , j.FF100011 as GGXH, j.FF100009 PC, j.FF100010 YS, b.FQTY, k.FTAXPRICE HSDJ, k.FPRICE DJ, k.FTAXRATE, k.FALLAMOUNT, b.FGIVEAWAY " +
                "from T_PUR_POORDER a left join T_BD_DEPARTMENT_L c on a.FPURCHASEDEPTID = c.FDEPTID left join T_BD_OPERATORENTRY f " +
                "on a.FPURCHASERID = f.FENTRYID left join T_BD_STAFF_L d on f.FSTAFFID = d.FSTAFFID left join T_BD_SUPPLIER_L e " +
                "on a.FSUPPLIERID = e.FSUPPLIERID, T_PUR_POORDERENTRY b left join T_BD_FLEXSITEMDETAILV j on b.FAUXPROPID = j.FID, " +
                "T_BD_UNIT g, T_BD_MATERIAL h, T_PUR_POORDERENTRY_L i, T_PUR_POORDERENTRY_F k, T_SEC_USER t where a.FID = b.FID " +
                "and a.FID = '" + key + "' and b.FUNITID = g.FUNITID and b.FENTRYID = i.FENTRYID and b.FMATERIALID = h.FMATERIALID " +
                "and b.FENTRYID = k.FENTRYID and a.FCREATORID = t.FUSERID").then(result => {
                logger.info("select a.FBILLNO, c.FNAME as CGBM, d.FNAME as CGY, e.FNAME GYS, a.FDATE, g.FNUMBER as DW, h.FNUMBER, " +
                    "i.FMATERIALDESC, j.FF100005, b.FQTY, k.FTAXPRICE HSDJ, k.FPRICE DJ, k.FTAXRATE, k.FALLAMOUNT, b.FGIVEAWAY " +
                    "from T_PUR_POORDER a left join T_BD_DEPARTMENT_L c on a.FPURCHASEDEPTID = c.FDEPTID left join T_BD_OPERATORENTRY f " +
                    "on a.FPURCHASERID = f.FENTRYID left join T_BD_STAFF_L d on f.FSTAFFID = d.FSTAFFID left join T_BD_SUPPLIER_L e " +
                    "on a.FSUPPLIERID = e.FSUPPLIERID, T_PUR_POORDERENTRY b left join T_BD_FLEXSITEMDETAILV j on b.FAUXPROPID = j.FID, " +
                    "T_BD_UNIT g, T_BD_MATERIAL h, T_PUR_POORDERENTRY_L i, T_PUR_POORDERENTRY_F k where a.FID = b.FID " +
                    "and a.FID = '" + key + "' and b.FUNITID = g.FUNITID and b.FENTRYID = i.FENTRYID and b.FMATERIALID = h.FMATERIALID " +
                    "and b.FENTRYID = k.FENTRYID");
                let middle = [
                    {title: "审批类型", text: "采购订单"},
                    {title: "单据编号", text: result.recordset[0].FBILLNO},
                    {title: "采购部门", text: result.recordset[0].CGBM},
                    {title: "采购员", text: result.recordset[0].CGY},
                    {title: "供应商", text: result.recordset[0].GYS},
                    {
                        title: "采购日期",
                        text: cmoment(result.recordset[0].FDATE).utcOffset("-00:00").format('YYYY-MM-DD')
                    }
                ];

                let allRecords = result.recordset;
                for (let ii = 0; ii < allRecords.length; ii++) {
                    middle.push({title: "明细信息(" + (ii + 1) + ")", text: ""});
                    middle.push({title: "物料编码", text: result.recordset[ii].FNUMBER});
                    middle.push({title: "物料名称", text: result.recordset[ii].FMATERIALDESC});
                    middle.push({title: "材质", text: result.recordset[ii].CZ});
                    middle.push({title: "要求说明", text: result.recordset[ii].GGXH});
                    middle.push({title: "品牌", text: result.recordset[ii].PC});
                    middle.push({title: "颜色", text: result.recordset[ii].YS});
                    middle.push({title: "采购单位", text: result.recordset[ii].DW});
                    middle.push({title: "采购数量", text: result.recordset[ii].FQTY});
                    middle.push({title: "单价", text: result.recordset[ii].DJ});
                    middle.push({title: "税率%", text: result.recordset[ii].FTAXRATE});
                    middle.push({title: "含税单价", text: result.recordset[ii].HSDJ});
                    middle.push({title: "价税合计", text: result.recordset[ii].FALLAMOUNT});
                    middle.push({title: "是否赠品", text: result.recordset[ii].FGIVEAWAY == "1" ? "是" : "否"});
                }

                ret.page = {
                    top: {
                        name: result.recordset[0].FNAME,
                        fullname: result.recordset[0].FNAME,
                        status: result.recordset[0].FDOCUMENTSTATUS == "C" ? "审核完成" : "正在审核"
                    },
                    middle: middle,
                    bottom: bottom
                }
                callBack(null, ret);
            }).catch(e=> {
                logger.info(e);
                callBack(e);
            });
        } catch (e) {
            logger.info(e);
        }
    }

    //处理采购申请订单
    let queryCGDDDetail = function (key, bottom) {
        try {
            logger.info("============  queryT_PUR_REQUISITION ======================" + key)
            let result = null;
            pool.request().query("select a.FBILLNO, a.FAPPLICATIONDATE, d.FMATERIALDESC, c.FNUMBER as DW, e.FNUMBER, " +
                "b.FREQQTY, a.FDOCUMENTSTATUS, f.FNAME as GYS, g.FF100007 as CZ , g.FF100011 as GGXH, g.FF100009 PC, g.FF100010 YS, h.FNAME as SQR, i.FNAME as BM, j.FNAME " +
                "from T_PUR_REQUISITION a " +
                "left join T_BD_STAFF_L h on a.FAPPLICANTID = h.FSTAFFID " +
                "left join T_BD_DEPARTMENT_L i on a.FAPPLICATIONDEPTID = i.FDEPTID, T_PUR_REQENTRY b " +
                "left join T_BD_SUPPLIER_L f on b.FSUGGESTSUPPLIERID = f.FSUPPLIERID " +
                "left join T_BD_FLEXSITEMDETAILV g on b.FAUXPROPID = g.FID, " +
                "T_BD_UNIT c, T_PUR_REQENTRY_L d, T_BD_MATERIAL e, T_SEC_USER j " +
                "where a.FID = b.FID and a.FID = '" + key + "' and b.FUNITID = c.FUNITID and b.FENTRYID = d.FENTRYID " +
                "and b.FMATERIALID = e.FMATERIALID and a.FCREATORID = j.FUSERID").then(result => {
                logger.info("select a.FBILLNO, a.FAPPLICATIONDATE, d.FMATERIALDESC, c.FNUMBER as DW, e.FNUMBER, " +
                    "b.FREQQTY, a.FDOCUMENTSTATUS, f.FNAME as GYS, g.FF100005, h.FNAME as SQR, i.FNAME as BM, j.FNAME " +
                    "from T_PUR_REQUISITION a " +
                    "left join T_BD_STAFF_L h on a.FAPPLICANTID = h.FSTAFFID " +
                    "left join T_BD_DEPARTMENT_L i on a.FAPPLICATIONDEPTID = i.FDEPTID, T_PUR_REQENTRY b " +
                    "left join T_BD_SUPPLIER_L f on b.FSUGGESTSUPPLIERID = f.FSUPPLIERID " +
                    "left join T_BD_FLEXSITEMDETAILV g on b.FAUXPROPID = g.FID, " +
                    "T_BD_UNIT c, T_PUR_REQENTRY_L d, T_BD_MATERIAL e, T_SEC_USER j " +
                    "where a.FID = b.FID and a.FID = '" + key + "' and b.FUNITID = c.FUNITID and b.FENTRYID = d.FENTRYID " +
                    "and b.FMATERIALID = e.FMATERIALID and a.FCREATORID = j.FUSERID");
                logger.info("T_PUR_REQUISITION" + JSON.stringify(result));
                let middle = [
                    {title: "审批类型", text: "采购申请单"},
                    {title: "单据编号", text: result.recordset[0].FBILLNO},
                    {title: "申请部门", text: result.recordset[0].BM},
                    {title: "申请人", text: result.recordset[0].SQR},
                    {
                        title: "申请日期",
                        text: cmoment(result.recordset[0].FAPPLICATIONDATE).utcOffset("-00:00").format('YYYY-MM-DD')
                    }
                ];

                let allRecords = result.recordset;
                for (let ii = 0; ii < allRecords.length; ii++) {
                    middle.push({title: "明细信息(" + (ii + 1) + ")", text: ""});
                    middle.push({title: "物料编码", text: result.recordset[ii].FNUMBER});
                    middle.push({title: "物料名称", text: result.recordset[ii].FMATERIALDESC});
                    middle.push({title: "材质", text: result.recordset[ii].CZ});
                    middle.push({title: "要求说明", text: result.recordset[ii].GGXH});
                    middle.push({title: "品牌", text: result.recordset[ii].PC});
                    middle.push({title: "颜色", text: result.recordset[ii].YS});
                    middle.push({title: "申请数量", text: result.recordset[ii].FREQQTY});
                    middle.push({title: "计量单位", text: result.recordset[ii].DW});
                    middle.push({title: "建议供应商", text: result.recordset[ii].GYS});
                }

                ret.page = {
                    top: {
                        name: result.recordset[0].FNAME,
                        fullname: result.recordset[0].FNAME,
                        status: result.recordset[0].FDOCUMENTSTATUS == "C" ? "审核完成" : "正在审核"
                    },
                    middle: middle,
                    bottom: bottom
                }
                callBack(null, ret);
            }).catch(e=> {
                logger.info(e);
                callBack(e);
            });
        } catch (e) {
            logger.info(e);
        }
    }


    let queryHTDetail = function (key, bottom) {
        try {
            logger.info("============  queryHDetail======================" + key)
            let result = null;
            pool.request().query("select a.FBILLNO, a.FCREATEDATE, a.F_PAEZ_SDATE, a.F_PAEZ_EDATE,e.FNAME,c.F_PAEZ_DECIMAL," +
                "c.F_PAEZ_DECIMAL5, f.FNAME as XMMC,a.FDOCUMENTSTATUS, d.F_PAEZ_PRICE1, d.F_PAEZ_DECIMAL2, " +
                "sum(d.F_PAEZ_AMOUNT1) as HTZJ, g.FNAME as CUSTNAME from T_KLM_LeaseContract a, T_KLM_LeaseContractAssets c," +
                " T_KLM_LeaseContractYsqd d, T_SEC_USER e, T_BD_MATERIAL_L f, T_BD_CUSTOMER_L g where a.fid = '" + key
                + "' and a.FID =c.FID and a.FID = d.FID and a.FCREATORID = e.FUSERID and c.F_PAEZ_BASE1 = f.FMATERIALID " +
                "and a.FCUSTID = g.FCUSTID group by a.FBILLNO, a.FCREATEDATE, a.F_PAEZ_SDATE, a.F_PAEZ_EDATE,e.FNAME,g.FNAME, " +
                "c.F_PAEZ_DECIMAL,c.F_PAEZ_DECIMAL5, f.FNAME, a.FDOCUMENTSTATUS, d.F_PAEZ_PRICE1, d.F_PAEZ_DECIMAL2, d.F_PAEZ_AMOUNT1").then(result => {
                logger.info("select a.FBILLNO, a.FCREATEDATE, a.F_PAEZ_SDATE, a.F_PAEZ_EDATE,e.FNAME,c.F_PAEZ_DECIMAL," +
                    "c.F_PAEZ_DECIMAL5, f.FNAME as XMMC,a.FDOCUMENTSTATUS, d.F_PAEZ_PRICE1, d.F_PAEZ_DECIMAL2, " +
                    "sum(d.F_PAEZ_AMOUNT1) as HTZJ, g.FNAME as CUSTNAME from T_KLM_LeaseContract a, T_KLM_LeaseContractAssets c," +
                    " T_KLM_LeaseContractYsqd d, T_SEC_USER e, T_BD_MATERIAL_L f, T_BD_CUSTOMER_L g where a.fid = '" + key
                    + "' and a.FID =c.FID and a.FID = d.FID and a.FCREATORID = e.FUSERID and c.F_PAEZ_BASE1 = f.FMATERIALID " +
                    "and a.FCUSTID = g.FCUSTID group by a.FBILLNO, a.FCREATEDATE, a.F_PAEZ_SDATE, a.F_PAEZ_EDATE,e.FNAME,g.FNAME, " +
                    "c.F_PAEZ_DECIMAL,c.F_PAEZ_DECIMAL5, f.FNAME, a.FDOCUMENTSTATUS, d.F_PAEZ_PRICE1, d.F_PAEZ_DECIMAL2, d.F_PAEZ_AMOUNT1");
                logger.info(JSON.stringify(result));
                logger.info("---- unit result :" + JSON.stringify(result));
                let middle = [
                    {title: "审批类型", text: "租赁合同"},
                    {title: "单据编号", text: result.recordset[0].FBILLNO},
                    {
                        title: "起租日期",
                        text: cmoment(result.recordset[0].F_PAEZ_SDATE).utcOffset("-00:00").format('YYYY-MM-DD')
                    },
                    {
                        title: "截止日期",
                        text: cmoment(result.recordset[0].F_PAEZ_EDATE).utcOffset("-00:00").format('YYYY-MM-DD')
                    },
                    {title: "资产名称", text: result.recordset[0].XMMC},
                    {title: "租赁单位", text: result.recordset[0].CUSTNAME},
                    {title: "房屋面积", text: result.recordset[0].F_PAEZ_DECIMAL + "平方米"},
                    {title: "租赁面积", text: result.recordset[0].F_PAEZ_DECIMAL5 + "平方米"},
                    {title: "租赁单价", text: result.recordset[0].F_PAEZ_PRICE1 + "元"},
                    {title: "合计总价", text: result.recordset[0].HTZJ + "元"},
                    {title: "保证金额", text: (result.recordset[1] ? result.recordset[1].HTZJ + "元" : "")},
                    {
                        title: "申请时间",
                        //text: result.recordset[0].FCREATEDATE
                        text: cmoment(result.recordset[0].FCREATEDATE).utcOffset("-00:00").format('YYYY-MM-DD HH:mm:ss')
                    }
                ];

                ret.page = {
                    top: {
                        name: result.recordset[0].FNAME,
                        fullname: result.recordset[0].FNAME,
                        status: result.recordset[0].FDOCUMENTSTATUS == "C" ? "审核完成" : "正在审核"
                    },
                    middle: middle,
                    bottom: bottom
                }
                callBack(null, ret);
            }).catch(e=> {
                logger.info(e);
                callBack(e);
            });
        } catch (e) {
            logger.info(e);
        }
    }

    //使用来自山东的合同管理模块--合同支付
    let querySDHTZF = function (key, bottom) {
        try {
            logger.info("============  queryHDetail=============== FJSAMOUNT =======" + key)
            let result = null;
            let queryPaySql = "select a.FBILLNO as QKBH, b.FBILLNO, d.FHTNAME, c.FNAME, e.FNAME as YFDW, g.FNAME as XMMC, h.FNAME as HTNX, b.FJSAMOUNT, " +
                "b.FCONTENT, i.FNAME as BZ, b.FAllAmount_CS, b.FBGAMOUNT, a.FHBALLAMOUNT, a.FQKNote, a.FCREATEDATE, f.FNAME as CNAME, a.FSUMFKAMOUNT, a.FALLAMOUNT_JS, " +
                "isnull((select sum(FHBALLAMOUNT) from SD_FDC_HTHBD where FHT = (select fht from SD_FDC_HTHBD where fid = " + key + ") and " +
                "FDOCUMENTSTATUS = 'C'),0) as TOTAL, a.FDOCUMENTSTATUS from SD_FDC_HTHBD a, SD_FDC_HT b, T_BD_SUPPLIER_L c, SD_FDC_HT_L d, T_BD_SUPPLIER_L e, " +
                "T_SEC_USER f, SD_FDC_GCXM_L g, SD_FDC_HTLB_L h, T_BD_CURRENCY_L i where a.FID = " + key + " and a.FHT = b.FID " +
                "and b.FSUPPLIERID1 = c.FSUPPLIERID and b.FID = d.FID and b.FSUPPLIERID2 = e.FSUPPLIERID and a.FCREATORID = f.FUSERID " +
                "and b.FGCXMID = g.FID and b.FHTLBID = h.FID and b.FSETTLECURRID = i.FCURRENCYID";
            let queryAttachments = "select * from T_BAS_ATTACHMENT where FBILLTYPE = 'SD_FDC_HTHBD' and FINTERID = " + key;

            Promise.all([pool.request().query(queryPaySql), pool.request().query(queryAttachments)]).then(queryRet => {
                //pool.request().query(queryPaySql).then(result => {
                logger.info(queryPaySql);
                logger.info(queryAttachments);

                logger.info("==========================================================");
                logger.info("---- sd ht module pay :" + JSON.stringify(queryRet));
                logger.info("==========================================================");

                let result = queryRet[0];
                let attResult = queryRet[1];

                let contractMoney = result.recordset[0].FAllAmount_CS + result.recordset[0].FBGAMOUNT;

                let payRateBase = result.recordset[0].FAllAmount_CS;
                //如果有结算金额,以结算为准
                if(parseFloat(result.recordset[0].FJSAMOUNT) > 0){
                    payRateBase = parseFloat(result.recordset[0].FJSAMOUNT);
                //如果有变更,总金额为总价 + 变更
                }else if(parseFloat(result.recordset[0].FBGAMOUNT) > 0){
                    payRateBase = contractMoney;
                }

                let attach = [];
                for (let i = 0; i < attResult.recordset.length; i++) {
                    let fn = attResult.recordset[i].FFILEID + attResult.recordset[i].FEXTNAME;
                    attach.push({
                        fName: attResult.recordset[i].FATTACHMENTNAME,
                        fUrl: "http://" + conf.hostName + ":" + conf.port + "/attach/sdContract/" + fn
                    });
                }

                let middle = [
                    {title: "审批类型", text: "合同请款"},
                    //{title: "请款编号", text: result.recordset[0].QKBH},
                    {title: "合同编号", text: result.recordset[0].FBILLNO},
                    {title: "项目名称", text: result.recordset[0].XMMC},
                    {title: "合同类别", text: result.recordset[0].HTNX},
                    {title: "合同名称", text: result.recordset[0].FHTNAME},
                    {title: "乙方单位", text: result.recordset[0].YFDW},
                    {title: "合同金额", text: result.recordset[0].FAllAmount_CS + "(" + result.recordset[0].BZ + ")"},
                    {title: "变更金额", text: result.recordset[0].FBGAMOUNT + "(" + result.recordset[0].BZ + ")"},
                    {title: "合同总价", text: contractMoney + "(" + result.recordset[0].BZ + ")"},
                    {title: "结算金额", text: result.recordset[0].FJSAMOUNT },
                    {title: "累计已付总额", text: result.recordset[0].FSUMFKAMOUNT + "(" + result.recordset[0].BZ + ")"},
                    {
                        title: "累计已付比例",
                        text: ((result.recordset[0].FSUMFKAMOUNT / payRateBase) * 100).toFixed(2) + "%"
                    },
                    {title: "本次请款金额", text: result.recordset[0].FHBALLAMOUNT + "(" + result.recordset[0].BZ + ")"},
                    {
                        title: "本次请款比例",
                        text: ((result.recordset[0].FHBALLAMOUNT / payRateBase) * 100).toFixed(2) + "%"
                    },
                    {title: "申请人员", text: result.recordset[0].CNAME},
                    {title: "备注信息", text: result.recordset[0].FQKNote},

                    {
                        title: "申请时间",
                        //text: result.recordset[0].FCREATEDATE
                        text: cmoment(result.recordset[0].FCREATEDATE).utcOffset("-00:00").format('YYYY-MM-DD HH:mm:ss')
                    }
                ];

                ret.page = {
                    top: {
                        name: result.recordset[0].CNAME.substr(0, 1),
                        fullname: result.recordset[0].CNAME,
                        status: result.recordset[0].FDOCUMENTSTATUS == "C" ? "审核完成" : "正在审核"
                    },
                    middle: middle,
                    attach: attach,
                    bottom: bottom
                }
                callBack(null, ret);
            }).catch(e=> {
                logger.info(e);
                callBack(e);
            });
        }
        catch
            (e) {
            logger.info(e);
        }
    }

    //使用来自山东的合同管理模块--合同
    let querySDHT = function (key, bottom) {
        try {
            logger.info("============  queryHDetail======================" + key)
            let result = null;
            let queryPaySql = "select b.FBILLNO, d.FHTNAME, c.FNAME, e.FNAME as YFDW, b.FAllAmount_CS, b.FBGAMOUNT, b.FDOCUMENTSTATUS, " +
                "b.FCREATEDATE, f.FNAME as CNAME, g.FNAME as XMMC, h.FNAME as HTNX, b.FCONTENT ,i.FNAME as BZ from SD_FDC_HT b, " +
                "T_BD_SUPPLIER_L c, SD_FDC_HT_L d, T_BD_SUPPLIER_L e, T_SEC_USER f, SD_FDC_GCXM_L g, SD_FDC_HTLB_L h, T_BD_CURRENCY_L i " +
                "where  b.FID = " + key + " and b.FSUPPLIERID1 = c.FSUPPLIERID and b.FID = d.FID " +
                "and b.FSUPPLIERID2 = e.FSUPPLIERID and b.FCREATORID = f.FUSERID and b.FGCXMID = g.FID " +
                "and b.FHTLBID = h.FID and b.FSETTLECURRID = i.FCURRENCYID";
            let queryAttachments = "select * from T_BAS_ATTACHMENT where FBILLTYPE = 'SD_FDC_HT' and FINTERID = " + key;

            Promise.all([pool.request().query(queryPaySql), pool.request().query(queryAttachments)]).then(queryRet => {
                //pool.request().query(queryPaySql).then(result => {
                logger.info(queryPaySql);
                logger.info(queryAttachments);

                logger.info("==========================================================");
                logger.info("---- sd ht module :" + JSON.stringify(queryRet));
                logger.info("==========================================================");

                let result = queryRet[0];
                let attResult = queryRet[1];

                let contractMoney = result.recordset[0].FAllAmount_CS + result.recordset[0].FBGAMOUNT;

                let attach = [];
                for (let i = 0; i < attResult.recordset.length; i++) {
                    let fn = attResult.recordset[i].FFILEID + attResult.recordset[i].FEXTNAME;
                    attach.push({
                        fName: attResult.recordset[i].FATTACHMENTNAME,
                        fUrl: "http://" + conf.hostName + ":" + conf.port + "/attach/sdContract/" + fn
                    });
                }

                let middle = [
                    {title: "审批类型", text: "合同签订"},
                    {title: "合同编号", text: result.recordset[0].FBILLNO},
                    {title: "项目名称", text: result.recordset[0].XMMC},
                    {title: "合同名称", text: result.recordset[0].FHTNAME},
                    {title: "乙方单位", text: result.recordset[0].YFDW},
                    {title: "合同类型", text: result.recordset[0].HTNX},
                    {title: "合同内容", text: result.recordset[0].FCONTENT},
                    {title: "合同金额", text: result.recordset[0].FAllAmount_CS + "(" + result.recordset[0].BZ + ")"},
                    {title: "变更金额", text: result.recordset[0].FBGAMOUNT + "(" + result.recordset[0].BZ + ")"},
                    {title: "补充金额", text: "0" + "(" + result.recordset[0].BZ + ")"},
                    {title: "合同总价", text: contractMoney + "(" + result.recordset[0].BZ + ")"},
                    {title: "申请人员", text: result.recordset[0].CNAME},

                    {
                        title: "申请时间",
                        //text: result.recordset[0].FCREATEDATE
                        text: cmoment(result.recordset[0].FCREATEDATE).utcOffset("-00:00").format('YYYY-MM-DD HH:mm:ss')
                    }
                ];

                ret.page = {
                    top: {
                        name: result.recordset[0].CNAME.substr(0, 1),
                        fullname: result.recordset[0].CNAME,
                        status: result.recordset[0].FDOCUMENTSTATUS == "C" ? "审核完成" : "正在审核"
                    },
                    middle: middle,
                    attach: attach,
                    bottom: bottom
                }
                callBack(null, ret);
            }).catch(e=> {
                logger.info(e);
                callBack(e);
            });
        }
        catch
            (e) {
            logger.info(e);
        }
    }

    //使用来自山东的合同管理模块--合同变更
    let querySDHTBG = function (key, bottom) {
        try {
            logger.info("============  query SDHTBG Detail======================" + key);
            let result = null;
            let queryPaySql = "select top 1 b.FBILLNO, d.FHTNAME, c.FNAME, e.FNAME as DFDW, b.FALLAMOUNT, a.FCSAMOUNT, a.FCREATEDATE, " +
                "f.FNAME as CNAME, k.FBGNR, a.FDOCUMENTSTATUS,g.FNAME as XMMC, h.FNAME as HTNX, i.FNAME as BZ, l.FNAME as BGNX, y.FNAME as BGYY, " +
                "isnull((select sum(FHBALLAMOUNT) from SD_FDC_HTHBD where FHT = (select FHTID from SD_HT_BGQZ where fid = " + key + ") " +
                "and FDOCUMENTSTATUS = 'C'),0) as TOTAL from SD_HT_BGQZ a, SD_FDC_HT b, T_BD_SUPPLIER_L c, SD_FDC_HT_L d, T_BD_SUPPLIER_L e, " +
                "T_SEC_USER f, SD_FDC_GCXM_L g, SD_FDC_HTLB_L h, T_BD_CURRENCY_L i, SD_HT_BGQZNR k, SD_HT_BGLX_L l, SD_HT_BGYY_L y where a.FID = " + key +
                "and a.FHTID = b.FID and b.FSUPPLIERID1 = c.FSUPPLIERID and b.FID = d.FID and b.FSUPPLIERID2 = e.FSUPPLIERID and " +
                "a.FCREATORID = f.FUSERID and b.FGCXMID = g.FID and a.FID = k.FID and a.FBGLX = l.FID and b.FHTLBID = h.FID and a.FBGYY = y.FID";

            let queryAttachments = "select * from T_BAS_ATTACHMENT where FBILLTYPE = 'SD_HT_BGQZ' and FINTERID = " + key;

            Promise.all([pool.request().query(queryPaySql), pool.request().query(queryAttachments)]).then(queryRet => {
                //pool.request().query(queryPaySql).then(result => {
                logger.info(queryPaySql);
                logger.info(queryAttachments);

                let result = queryRet[0];
                let attResult = queryRet[1];

                let attach = [];
                for (let i = 0; i < attResult.recordset.length; i++) {
                    let fn = attResult.recordset[i].FFILEID + attResult.recordset[i].FEXTNAME;
                    attach.push({
                        fName: attResult.recordset[i].FATTACHMENTNAME,
                        fUrl: "http://" + conf.hostName + ":" + conf.port + "/attach/sdContract/" + fn
                    });
                }

                let middle = [
                    {title: "审批类型", text: "合同变更"},
                    {title: "合同编号", text: result.recordset[0].FBILLNO},
                    {title: "项目名称", text: result.recordset[0].XMMC},
                    {title: "合同类型", text: result.recordset[0].HTNX},
                    {title: "合同名称", text: result.recordset[0].FHTNAME},
                    {title: "乙方单位", text: result.recordset[0].DFDW},
                    {title: "原合同金额", text: result.recordset[0].FALLAMOUNT + " (" + result.recordset[0].BZ + ")"},
                    {title: "累计已付款金额", text: result.recordset[0].TOTAL + " (" + result.recordset[0].BZ + ")"},
                    {title: "变更金额", text: result.recordset[0].FCSAMOUNT + " (" + result.recordset[0].BZ + ")"},
                    {title: "变更类型", text: result.recordset[0].BGNX},
                    {title: "变更原因", text: result.recordset[0].BGYY},
                    {title: "变更内容", text: result.recordset[0].FBGNR},
                    {
                        title: "变更后合同金额",
                        text: (result.recordset[0].FALLAMOUNT + result.recordset[0].FCSAMOUNT) + "(" + result.recordset[0].BZ + ")"
                    },
                    {title: "申请人员", text: result.recordset[0].CNAME},

                    {
                        title: "申请时间",
                        //text: result.recordset[0].FCREATEDATE
                        text: cmoment(result.recordset[0].FCREATEDATE).utcOffset("-00:00").format('YYYY-MM-DD HH:mm:ss')
                    }
                ];

                ret.page = {
                    top: {
                        name: result.recordset[0].CNAME.substr(0, 1),
                        fullname: result.recordset[0].CNAME,
                        status: result.recordset[0].FDOCUMENTSTATUS == "C" ? "审核完成" : "正在审核"
                    },
                    middle: middle,
                    attach: attach,
                    bottom: bottom
                }
                callBack(null, ret);
            }).catch(e=> {
                logger.info(e);
                callBack(e);
            });
        }
        catch
            (e) {
            logger.info(e);
        }
    }

    //使用来自山东的合同管理模块--合同结算单
    let querySDHTJSD = function (key, bottom) {
        try {
            logger.info("============  query SDHTBG Detail======================" + key);
            let result = null;
            let queryPaySql = "select b.FBILLNO, d.FHTNAME, c.FNAME, e.FNAME as DFDW, b.FAllAmount_CS, b.FBGAMOUNT, a.FJSAMOUNT, a.FCREATEDATE, " +
                "f.FNAME as CNAME , a.FNOTE, a.fcontent, g.FNAME as XMMC, h.FNAME as HTNX, i.FNAME as BZ, a.FDOCUMENTSTATUS, a.FTZAMOUNT, " +
                "isnull((select sum(FHBALLAMOUNT) from SD_FDC_HTHBD where FHT = (select FHTID from SD_FDC_JSD where fid = " + key + ") " +
                "and FDOCUMENTSTATUS = 'C'),0) as TOTAL from SD_FDC_JSD a, SD_FDC_HT b, T_BD_SUPPLIER_L c, SD_FDC_HT_L d, T_BD_SUPPLIER_L e, " +
                "T_SEC_USER f, SD_FDC_GCXM_L g, SD_FDC_HTLB_L h, T_BD_CURRENCY_L i where a.FID = " + key +
                " and a.FHTID = b.FID and b.FSUPPLIERID1 = c.FSUPPLIERID and b.FID = d.FID and b.FSUPPLIERID2 = e.FSUPPLIERID " +
                "and a.FCREATORID = f.FUSERID and b.FGCXMID = g.FID and a.FSETTLECURRID = i.FCURRENCYID and b.FHTLBID = h.FID";

            let queryAttachments = "select * from T_BAS_ATTACHMENT where FBILLTYPE = 'SD_FDC_JSD' and FINTERID = " + key;

            logger.info(queryPaySql);
            logger.info(queryAttachments);
            Promise.all([pool.request().query(queryPaySql), pool.request().query(queryAttachments)]).then(queryRet => {
                //pool.request().query(queryPaySql).then(result => {

                let result = queryRet[0];
                let attResult = queryRet[1];

                let contractMoney = result.recordset[0].FAllAmount_CS + result.recordset[0].FBGAMOUNT;

                let attach = [];
                for (let i = 0; i < attResult.recordset.length; i++) {
                    let fn = attResult.recordset[i].FFILEID + attResult.recordset[i].FEXTNAME;
                    attach.push({
                        fName: attResult.recordset[i].FATTACHMENTNAME,
                        fUrl: "http://" + conf.hostName + ":" + conf.port + "/attach/sdContract/" + fn
                    });
                }

                let middle = [
                    {title: "审批类型", text: "合同结算"},
                    {title: "合同编号", text: result.recordset[0].FBILLNO},
                    {title: "项目名称", text: result.recordset[0].XMMC},
                    {title: "合同类型", text: result.recordset[0].HTNX},
                    {title: "合同名称", text: result.recordset[0].FHTNAME},
                    {title: "乙方单位", text: result.recordset[0].DFDW},
                    {title: "原合同金额", text: result.recordset[0].FAllAmount_CS + " (" + result.recordset[0].BZ + ")"},
                    {title: "变更合同金额", text: result.recordset[0].FBGAMOUNT + " (" + result.recordset[0].BZ + ")"},
                    {title: "累计付款金额", text: result.recordset[0].FTZAMOUNT + " (" + result.recordset[0].BZ + ")"},
                    {title: "累计付款比例", text: ((result.recordset[0].FTZAMOUNT / contractMoney) * 100).toFixed(2) + "%"},
                    {title: "结算金额", text: result.recordset[0].FJSAMOUNT + " (" + result.recordset[0].BZ + ")"},
                    {
                        title: "调整金额",
                        text: (result.recordset[0].FJSAMOUNT - contractMoney) + " (" + result.recordset[0].BZ + ")"
                    },
                    {title: "结算内容", text: result.recordset[0].fcontent},
                    {title: "申请人员", text: result.recordset[0].CNAME},

                    {
                        title: "申请时间",
                        //text: result.recordset[0].FCREATEDATE
                        text: cmoment(result.recordset[0].FCREATEDATE).utcOffset("-00:00").format('YYYY-MM-DD HH:mm:ss')
                    }
                ];

                ret.page = {
                    top: {
                        name: result.recordset[0].CNAME.substr(0, 1),
                        fullname: result.recordset[0].CNAME,
                        status: result.recordset[0].FDOCUMENTSTATUS == "C" ? "审核完成" : "正在审核"
                    },
                    middle: middle,
                    attach: attach,
                    bottom: bottom
                }
                callBack(null, ret);
            }).catch(e=> {
                logger.info(e);
                callBack(e);
            });
        }
        catch
            (e) {
            logger.info(e);
        }
    }

    let queryHtysdDetail = function (key, bottom) {
        try {
            logger.info("============ zlht  queryHtysdDetail======================" + key)
            let result = null;
            pool.request().query("select a.FBILLNO, a.F_PAEZ_SDATE, a.F_PAEZ_EDATE, b.F_PAEZ_DECIMAL5, d.FNAME as CZF, a.F_PAEZ_PRICE, b.F_PAEZ_DECIMAL, e.FNAME, " +
                "f.FNAME as XMMC, a.FDOCUMENTSTATUS from PAEZ_t_Cust_Entry100027 a,PAEZ_t_Cust_Entry100028 b, T_BD_CUSTOMER_L d, T_SEC_USER e," +
                " T_BD_MATERIAL_L f where a.FID = b.FID and a.FID = '" + key + "' and a.FCUSTID = d.FCUSTID and d.FLOCALEID = 2052 " +
                "and a.FCREATORID = e.FUSERID and b.F_PAEZ_BASE1 = f.FMATERIALID").then(result => {
                logger.info("select a.FBILLNO, a.F_PAEZ_SDATE, a.F_PAEZ_EDATE, d.FNAME as CZF, a.F_PAEZ_PRICE, b.F_PAEZ_DECIMAL, e.FNAME, " +
                    "f.FNAME as XMMC from PAEZ_t_Cust_Entry100027 a,PAEZ_t_Cust_Entry100028 b, T_BD_CUSTOMER_L d, T_SEC_USER e," +
                    " T_BD_MATERIAL_L f where a.FID = b.FID  and a.FID = '" + key + "' and a.FCUSTID = d.FCUSTID and d.FLOCALEID = 2052 " +
                    "and a.FCREATORID = e.FUSERID and b.F_PAEZ_BASE1 = f.FMATERIALID");
                logger.info(JSON.stringify(result));
                logger.info("---- unit result :" + JSON.stringify(result));
                let middle = [
                    {title: "审批类型", text: "合同预审单"},
                    {title: "单据编号", text: result.recordset[0].FBILLNO},
                    {title: "承租方", text: result.recordset[0].CZF},
                    {
                        title: "起租日期",
                        text: cmoment(result.recordset[0].F_PAEZ_SDATE).utcOffset("-00:00").format('YYYY-MM-DD')
                    },
                    {
                        title: "截止日期",
                        text: cmoment(result.recordset[0].F_PAEZ_EDATE).utcOffset("-00:00").format('YYYY-MM-DD')
                    },
                    {title: "资产名称", text: result.recordset[0].XMMC},
                    {title: "租赁单价", text: result.recordset[0].F_PAEZ_PRICE + "元"},
                    {title: "房屋面积", text: result.recordset[0].F_PAEZ_DECIMAL + "平方米"},
                    {title: "房屋面积", text: result.recordset[0].F_PAEZ_DECIMAL5 + "平方米"},
                    {
                        title: "申请时间",
                        //text: result.recordset[0].FCREATEDATE
                        text: cmoment(result.recordset[0].FCREATEDATE).utcOffset("-00:00").format('YYYY-MM-DD HH:mm:ss')
                    }
                ];

                ret.page = {
                    top: {
                        name: result.recordset[0].FNAME,
                        fullname: result.recordset[0].FNAME,
                        status: result.recordset[0].FDOCUMENTSTATUS == "D" ? "审核完成" : "正在审核"
                    },
                    middle: middle,
                    bottom: bottom
                }
                callBack(null, ret);
            }).catch(e=> {
                logger.info(e);
                callBack(e);
            });
        } catch (e) {
            logger.info(e);
        }
    }


    let queryOutStockDetail = function (key, bottom) {
        try {
            logger.info("============  queryOutStockDetail======================" + key)
            let result = null;
            pool.request().query("select a.FBILLNO, d.FNAME as KFMC, c.FPRICE, c.FTAXPRICE, e.FNAME as XMMC, b.FREALQTY, " +
                "a.FCREATEDATE, f.FNAME, a.FDOCUMENTSTATUS , b.F_KLM_DECIMAL ,h.FNAME as ZCLX from T_SAL_OUTSTOCK a, " +
                "T_SAL_OUTSTOCKENTRY b, T_SAL_OUTSTOCKENTRY_F c, T_BD_CUSTOMER_L d, T_BD_MATERIAL_L e, T_SEC_USER f ,T_BD_MATERIAL g, " +
                "T_BD_MATERIALGROUP_L h where a.FID =  '" + key + "' and a.FID = b.FID and a.FID = c.FID and a.FCUSTOMERID = d.FCUSTID " +
                "and d.FLOCALEID = 2052 and b.FMATERIALID = e.FMATERIALID and a.FCREATORID = f.FUSERID " +
                "and b.FMATERIALID = g.FMATERIALID and g.FMATERIALGROUP = h.FID").then(result => {
                logger.info("select a.FBILLNO, d.FNAME as KFMC, c.FPRICE, c.FTAXPRICE, e.FNAME as XMMC, b.FREALQTY, " +
                    "a.FCREATEDATE, f.FNAME, a.FDOCUMENTSTATUS , b.F_KLM_DECIMAL ,h.FNAME as ZCLX from T_SAL_OUTSTOCK a, " +
                    "T_SAL_OUTSTOCKENTRY b, T_SAL_OUTSTOCKENTRY_F c, T_BD_CUSTOMER_L d, T_BD_MATERIAL_L e, T_SEC_USER f ,T_BD_MATERIAL g, " +
                    "T_BD_MATERIALGROUP_L h where a.FID =  '" + key + "' and a.FID = b.FID and a.FID = c.FID and a.FCUSTOMERID = d.FCUSTID " +
                    "and d.FLOCALEID = 2052 and b.FMATERIALID = e.FMATERIALID and a.FCREATORID = f.FUSERID " +
                    "and b.FMATERIALID = g.FMATERIALID and g.FMATERIALGROUP = h.FID");
                logger.info(JSON.stringify(result));
                logger.info("---- unit result :" + JSON.stringify(result));
                let middle = [
                    {title: "审批类型", text: "资产销售单"},
                    {title: "单据编号", text: result.recordset[0].FBILLNO},
                    {title: "客户名称", text: result.recordset[0].KFMC},
                    {title: "资产名称", text: result.recordset[0].XMMC},
                    {title: "资产类型", text: result.recordset[0].ZCLX},
                    {title: "销售金额", text: result.recordset[0].FTAXPRICE + "元"},
                    {title: "销售面积", text: result.recordset[0].F_KLM_DECIMAL + "平方米"},
                    {title: "实发数量", text: result.recordset[0].FREALQTY},
                    {
                        title: "申请时间",
                        //text: result.recordset[0].FCREATEDATE
                        text: cmoment(result.recordset[0].FCREATEDATE).utcOffset("-00:00").format('YYYY-MM-DD HH:mm:ss')
                    }
                ];

                ret.page = {
                    top: {
                        name: result.recordset[0].FNAME,
                        fullname: result.recordset[0].FNAME,
                        status: result.recordset[0].FDOCUMENTSTATUS == "D" ? "审核完成" : "正在审核"
                    },
                    middle: middle,
                    bottom: bottom
                }
                callBack(null, ret);
            }).catch(e=> {
                logger.info(e);
                callBack(e);
            });
        } catch (e) {
            logger.info(e);
        }
    }


    let queryZBDetail = function (key, bottom) {
        try {
            logger.info("============  queryZBTZS Detail======================" + key)
            let result = null;
            pool.request().query("select FBILLNO,FDOCUMENTSTATUS, F_KLM_TEXT1 XMMC, F_KLM_TEXT2 SGDW, F_KLM_AMOUNT ABJE, F_KLM_REMARKS BZ, " +
                "F_KLM_DATE KGRQ, F_KLM_DATE1 JGRQ from KLM_t_Cust_Entry100075 where FID = " + key).then(result => {
                logger.info("select FBILLNO,FDOCUMENTSTATUS, F_KLM_TEXT1 XMMC, F_KLM_TEXT2 SGDW, F_KLM_AMOUNT ABJE, F_KLM_REMARKS BZ, " +
                    "F_KLM_DATE KGRQ, F_KLM_DATE1 JGRQ from KLM_t_Cust_Entry100075 where FID = " + key);
                logger.info(JSON.stringify(result));
                logger.info("---- unit result :" + JSON.stringify(result));
                let middle = [
                    {title: "审批类型", text: "中标申请单"},
                    {title: "单据编号", text: result.recordset[0].FBILLNO},
                    {title: "项目名称", text: result.recordset[0].XMMC},
                    {title: "中标单位", text: result.recordset[0].SGDW},
                    {title: "中标金额", text: result.recordset[0].ABJE + "元"},
                    {
                        title: "开工日期",
                        text: cmoment(result.recordset[0].KGRQ).utcOffset("-00:00").format('YYYY-MM-DD HH:mm:ss')
                    },
                    {
                        title: "竣工日期",
                        text: cmoment(result.recordset[0].JGRQ).utcOffset("-00:00").format('YYYY-MM-DD HH:mm:ss')
                    },
                    {title: "中标范围", text: result.recordset[0].BZ},
                ];

                ret.page = {
                    top: {
                        name: result.recordset[0].FNAME,
                        fullname: result.recordset[0].FNAME,
                        status: result.recordset[0].FDOCUMENTSTATUS == "D" ? "审核完成" : "正在审核"
                    },
                    middle: middle,
                    bottom: bottom
                }
                callBack(null, ret);
            }).catch(e=> {
                logger.info(e);
                callBack(e);
            });
        } catch (e) {
            logger.info(e);
        }
    }


    let queryPayApplyDetail = function (key, bottom) {
        try {
            logger.info("-------------------------- cn pay :  " + key);
            logger.info("select a.FBILLNO,a.FAPPLYAMOUNTFOR_H,a.FRECTUNITTYPE, a.FRECTUNIT, b.FNAME, a.FCREATEDATE, e.FNAME as FKYT, f.FNAME as LX, " +
                "a.FPURCHASEDEPTID, c.FDESCRIPTION, a.FDOCUMENTSTATUS from T_CN_PAYAPPLY a, T_SEC_USER b, T_CN_PAYAPPLYENTRY c ," +
                " T_BAS_BILLTYPE_L f, T_BD_DEPARTMENT_L d, " +
                "T_CN_RECPAYPURPOSE_L e where a.fid = '" + key + "' and a.FCREATORID = b.FUSERID and a.fid = c.FID and a.FDEPARTMENT = d.FDEPTID " +
                "and c.FPAYPURPOSEID = e.FID  and a.FBILLTYPEID = f.FBILLTYPEID and f.FLOCALEID = '2052'");
            let result = null;
            pool.request().query("select a.FBILLNO,a.FAPPLYAMOUNTFOR_H,a.FRECTUNITTYPE, a.FRECTUNIT, b.FNAME, a.FCREATEDATE, e.FNAME as FKYT, f.FNAME as LX, " +
                "a.FPURCHASEDEPTID, c.FDESCRIPTION, a.FDOCUMENTSTATUS from T_CN_PAYAPPLY a, T_SEC_USER b, T_CN_PAYAPPLYENTRY c ," +
                " T_BAS_BILLTYPE_L f, " +
                "T_CN_RECPAYPURPOSE_L e where a.fid = '" + key + "' and a.FCREATORID = b.FUSERID and a.fid = c.FID " +
                "and c.FPAYPURPOSEID = e.FID  and a.FBILLTYPEID = f.FBILLTYPEID and f.FLOCALEID = '2052'").then(tresult => {
                result = tresult;

                logger.info(JSON.stringify(result));
                let queryUnitSql = "select FNAME from ";
                if (result.recordset[0].FRECTUNITTYPE == "BD_Empinfo") {
                    queryUnitSql += "T_HR_EMPINFO_L where fid = " + result.recordset[0].FRECTUNIT;
                } else if (result.recordset[0].FRECTUNITTYPE == "FIN_OTHERS") {
                    queryUnitSql += "T_FIN_OTHERS_L where fid = " + result.recordset[0].FRECTUNIT;
                } else if (result.recordset[0].FRECTUNITTYPE == "BD_Supplier") {
                    queryUnitSql += "T_BD_SUPPLIER_L where fsupplierid = " + result.recordset[0].FRECTUNIT;
                } else if (result.recordset[0].FRECTUNITTYPE == "BD_Customer") {
                    queryUnitSql += "T_BD_Customer_L where FCUSTID = " + result.recordset[0].FRECTUNIT;
                } else if (result.recordset[0].FRECTUNITTYPE == "BD_Department") {
                    queryUnitSql += "T_BD_DEPARTMENT_L where FDEPTID = " + result.recordset[0].FRECTUNIT;
                } else if (result.recordset[0].FRECTUNITTYPE == "BD_BANK") {
                    queryUnitSql += "T_BD_BANK_L where FBANKID = " + result.recordset[0].FRECTUNIT;
                } else {
                    callBack("往来单位类型错误", null);
                }
                logger.info(queryUnitSql);
                return pool.request().query(queryUnitSql);
            }).then(unitResult => {
                logger.info("---- unit result :" + JSON.stringify(unitResult));
                let middle = [
                    {title: "审批类型", text: result.recordset[0].LX},
                    {title: "单据编号", text: result.recordset[0].FBILLNO},
                    {title: "收款单位", text: unitResult.recordset[0].FNAME},
                    {title: "付款金额", text: result.recordset[0].FAPPLYAMOUNTFOR_H + "元"},
                    {title: "申请人员", text: result.recordset[0].FNAME},
                    {title: "费用项目", text: result.recordset[0].FKYT},
                    {title: "付款用途", text: result.recordset[0].FDESCRIPTION},
                    {
                        title: "申请时间",
                        //text: result.recordset[0].FCREATEDATE
                        text: cmoment(result.recordset[0].FCREATEDATE).utcOffset("-00:00").format('YYYY-MM-DD HH:mm:ss')
                    }
                ];

                ret.page = {
                    top: {
                        name: result.recordset[0].FNAME,
                        fullname: result.recordset[0].FNAME,
                        status: result.recordset[0].FDOCUMENTSTATUS == "D" ? "审核完成" : "正在审核"
                    },
                    middle: middle,
                    bottom: bottom
                }
                callBack(null, ret);
            }).catch(e=> {
                logger.info(e);
                callBack(e);
            });
        } catch (e) {
            logger.info(e);
        }
    }

    sql.connect({
            user: conf.db.username,
            password: conf.db.password,
            server: conf.db.hostname, // You can use 'localhost\\instance' to connect to named instance
            database: conf.db.database
        }
    ).then(conPool => {
        pool = conPool;

        logger.info("=== fordouble new : select c.FASSIGNID, d.FNAME, c.FCREATETIME, b.FTITLE, e.FOBJECTTYPEID, e.FKEYVALUE, b.FRECEIVERID, " +
        "c.FPROCINSTID, a.FPHONE, f.FDINACT, e.FAPPROVALASSIGNID from T_SEC_USER a, T_WF_RECEIVER b, T_WF_ASSIGN c, T_SEC_USER d, T_WF_APPROVALASSIGN e, " +
        "T_DIN_ACCT f where a.FUSERID = b.FRECEIVERID and b.FASSIGNID = c.FASSIGNID and c.FSENDERID = d.FUSERID and c.FASSIGNID = e.FASSIGNID "
        + (only ? "" : "and c.FSTATUS = 0 ") + (only ? "" : ("and a.fphone = '" + phonenbr + "'")) + " and f.fphone = a.FPHONE and c.FASSIGNID = '" + assignId + "'");
        return pool.request().query("select c.FASSIGNID, d.FNAME, c.FCREATETIME, b.FTITLE, e.FOBJECTTYPEID, e.FKEYVALUE, b.FRECEIVERID, " +
            "c.FPROCINSTID, a.FPHONE, f.FDINACT, e.FAPPROVALASSIGNID from T_SEC_USER a, T_WF_RECEIVER b, T_WF_ASSIGN c, T_SEC_USER d, T_WF_APPROVALASSIGN e, " +
            "T_DIN_ACCT f where a.FUSERID = b.FRECEIVERID and b.FASSIGNID = c.FASSIGNID and c.FSENDERID = d.FUSERID and c.FASSIGNID = e.FASSIGNID "
            + (only ? "" : "and c.FSTATUS = 0 ") + (only ? "" : ("and a.fphone = '" + phonenbr + "'")) + " and f.fphone = a.FPHONE and c.FASSIGNID = '" + assignId + "'");
    }).then(result => {

            logger.info("+++==" + JSON.stringify(result));
            logger.info(result.recordset.length);
            if (result.recordset.length == 0) {
                callBack({
                    message: "此记录已审核",
                    error: {
                        status: 0,
                        stack: "无需要再审核"
                    }
                })
            } else {
                ret.userid = result.recordset[0].FRECEIVERID;
                ret.keyValue = result.recordset[0].FKEYVALUE;
                ret.procInstId = result.recordset[0].FPROCINSTID;
                ret.preArrprovId = result.recordset[0].FAPPROVALASSIGNID;
                ret.msgName = result.recordset[0].FNAME;
                ret.msg = result.recordset[0].FTITLE;

                let bottoms = [];

                //let attachMents = [];
                //
                //pool.request().query("select a.FSUBMITOPINION, b.FNAME, a.FCREATETIME from T_WF_PROCINST a, T_SEC_USER b where FPROCINSTID = '"
                //    + result.recordset[0].FPROCINSTID + "' and a.FORIGINATORID = b.FUSERID")


                pool.request().query("select a.FSUBMITOPINION, b.FNAME, a.FCREATETIME from T_WF_PROCINST a, T_SEC_USER b where FPROCINSTID = '"
                    + result.recordset[0].FPROCINSTID + "' and a.FORIGINATORID = b.FUSERID").then(initResult => {

                    bottoms.push({
                        name: initResult.recordset[0].FNAME,
                        fullName: initResult.recordset[0].FNAME,
                        item: "发起流程",
                        result: "",
                        remark: initResult.recordset[0].FSUBMITOPINION,
                        date: initResult.recordset[0].FCREATETIME ? cmoment(initResult.recordset[0].FCREATETIME).utcOffset("-00:00").format('YYYY-MM-DD') : "",
                        time: initResult.recordset[0].FCREATETIME ? cmoment(initResult.recordset[0].FCREATETIME).utcOffset("-00:00").format('HH:mm:ss') : "",
                        margin: "1",
                        imgDiv: false
                    })

                    return pool.request().query("select a.FRECEIVERNAMES, a.FCOMPLETEDTIME, b.FRESULT , b.FDISPOSITION, c.FRESULTNAME, d.FACTNAME " +
                        "from T_WF_ASSIGN a " +
                        "inner join T_WF_APPROVALASSIGN b on a.FASSIGNID = b.FASSIGNID and a.FPROCINSTID ='" + result.recordset[0].FPROCINSTID + "' " +
                        "left join T_WF_APPROVALASSIGN_L c on b.FAPPROVALASSIGNID = c.FAPPROVALASSIGNID and c.FLOCALEID =2052 " +
                        "left join T_WF_ACTINST_L d on a.FACTINSTID = d.FACTINSTID and d.FLOCALEID = 2052 order by a.FCREATETIME");
                }).then(procResult => {

                    console.log("---- fqyj :" + "select a.FRECEIVERNAMES, a.FCOMPLETEDTIME, b.FRESULT , b.FDISPOSITION, c.FRESULTNAME, d.FACTNAME " +
                        "from T_WF_ASSIGN a " +
                        "inner join T_WF_APPROVALASSIGN b on a.FASSIGNID = b.FASSIGNID and a.FPROCINSTID ='" + result.recordset[0].FPROCINSTID + "' " +
                        "left join T_WF_APPROVALASSIGN_L c on b.FAPPROVALASSIGNID = c.FAPPROVALASSIGNID and c.FLOCALEID =2052 " +
                        "left join T_WF_ACTINST_L d on a.FACTINSTID = d.FACTINSTID and d.FLOCALEID = 2052 order by a.FCREATETIME")

                    let procLists = procResult.recordset;
                    for (let k = 0; k < procLists.length; k++) {
                        //if(k ==0){
                        bottoms.push({
                            name: procLists[k].FRECEIVERNAMES,
                            fullName: procLists[k].FRECEIVERNAMES,
                            item: procLists[k].FACTNAME,
                            result: procLists[k].FRESULTNAME,
                            remark: procLists[k].FDISPOSITION ? procLists[k].FDISPOSITION : "正在审核",
                            date: procLists[k].FCOMPLETEDTIME ? cmoment(procLists[k].FCOMPLETEDTIME).utcOffset("-00:00").format('YYYY-MM-DD') : "",
                            time: procLists[k].FCOMPLETEDTIME ? cmoment(procLists[k].FCOMPLETEDTIME).utcOffset("-00:00").format('HH:mm:ss') : "",
                            margin: "40",
                            imgDiv: true
                        })
                    }
                    logger.info(" table name in select : " + tableName)
                    if (tableName == "AR_RECEIVEBILL") {
                        queryReceiveDetail(result.recordset[0].FKEYVALUE, bottoms);
                    }
                    if (tableName == "CN_PAYAPPLY") {
                        queryPayApplyDetail(result.recordset[0].FKEYVALUE, bottoms);
                    }
                    if (tableName == "SAL_OUTSTOCK") {
                        queryOutStockDetail(result.recordset[0].FKEYVALUE, bottoms);
                    }
                    if (tableName == "PAEZ_HY_HTYSD") {
                        queryHtysdDetail(result.recordset[0].FKEYVALUE, bottoms);
                    }
                    if (tableName == "PAEZ_HY_ZLHT") {
                        queryHTDetail(result.recordset[0].FKEYVALUE, bottoms);
                    }
                    if (tableName == "1c6e00b8e8fc4ca1a8086ad7e09d0437") {
                        queryZBDetail(result.recordset[0].FKEYVALUE, bottoms);
                    }
                    if (tableName == "SD_FDC_HTHBD") {
                        querySDHTZF(result.recordset[0].FKEYVALUE, bottoms);
                    }
                    if (tableName == "SD_FDC_HT") {
                        querySDHT(result.recordset[0].FKEYVALUE, bottoms);
                    }
                    if (tableName == "SD_HT_BGQZ") {
                        querySDHTBG(result.recordset[0].FKEYVALUE, bottoms);
                    }
                    if (tableName == "SD_FDC_JSD") {
                        querySDHTJSD(result.recordset[0].FKEYVALUE, bottoms);
                    }
                    if (tableName == "PUR_Requisition") {
                        queryCGDDDetail(result.recordset[0].FKEYVALUE, bottoms);
                    }
                    if (tableName == "PUR_PurchaseOrder") {
                        queryDDDetail(result.recordset[0].FKEYVALUE, bottoms);
                    }
                    if (tableName == "STK_InStock") {
                        queryCGRKDetail(result.recordset[0].FKEYVALUE, bottoms);
                    }
                }).catch(procE=> {
                    logger.info(procE);
                    callBack(procE)
                });


            }
        }
    ).catch(pe=> {
        logger.info("-------------------- execute error ----------------------");
        logger.info(pe);
        callBack(pe)
    })

    sql.on('error', err => {
        logger.info("-------------------- sql error ----------------------");
        logger.info(err);
        pool.close();
    });

}

detail.queryDetails = query;

module.exports = detail;