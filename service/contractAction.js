/**
 * Created by zhoulanhong on 8/26/17.
 */
var sql = require('mssql');
var conf = require('../config/config');
var Id = require('../libs/Id');
var fkd = require('./saveFksqd');
var logger = require('../config/log4js').getLogger('agreeAction');
var noticeBk = require('./noticeBack');
var saveContract = require('../service/saveContract');
var saveBgSqd = require('../service/saveBGsqd');
var saveBalanced = require('../service/saveBalanced');
var cmoment = require('moment');
var Promise = require('bluebird');

function action() {
}

function back(wfId, nodeId, key, remark, tableName, callBack) {

    if (sql) {
        sql.close();
    }

    logger.info("wfId " + wfId)
    logger.info("nodeId " + nodeId)
    logger.info("key " + key)
    logger.info("remark " + remark)

    var request = null;
    var trans = null;
    var pool = null;
    let nextNodeId = null;
    let nodeName = null;
    let actor = null;
    let creator = null;
    let taskId = Date.now() + Id.get();
    let bakMsg = "";

    let realProcess = function () {
        logger.info("update T_SYS_WF_TASK set FINISH_TIME = GETDATE(), REMARK='" + remark + "', opinion = '打回调整', " +
            "view_state = '1', STATE = 4 where NODE_ID = '" + nodeId + "';")
        request.query("update T_SYS_WF_TASK set FINISH_TIME = GETDATE(), REMARK='" + remark + "', opinion = '打回调整', " +
            "view_state = '1', STATE = 4 where NODE_ID = '" + nodeId + "';").then(result => {
            logger.info("update T_SYS_WF_NODE set STATE = 2 where NODE_ID = '" + nextNodeId + "';")
            return request.query("update T_SYS_WF_NODE set STATE = 2 where NODE_ID = '" + nextNodeId + "';");
        }).then(result => {
            logger.info("update T_SYS_WF_NODE set STATE = 4 where NODE_ID = '" + nodeId + "';")
            return request.query("update T_SYS_WF_NODE set STATE = 4 where NODE_ID = '" + nodeId + "';");
        }).then(result => {
            logger.info("insert into T_SYS_WF_TASK values( '" + wfId + "','" + nextNodeId + "','" + taskId + "','" + tableName + "','" + key + "',null," +
                " '" + actor + "','" + nodeName + "','P',GETDATE(),null,GETDATE(),null,0.00,null,2, '" + creator + "',null,null,null,null)");
            return request.query("insert into T_SYS_WF_TASK values( '" + wfId + "','" + nextNodeId + "','" + taskId + "','" + tableName + "','" + key + "',null," +
                " '" + actor + "','" + nodeName + "','P',GETDATE(),null,GETDATE(),null,0.00,null,2, '" + creator + "',null,null,null,null)");
        }).then(result => {
            if (tableName == "t_con_contract") {
                logger.info("select CREATOR_NAME, CONTRACT_CODE from T_CON_CONTRACT where CONTRACT_ID = '" + key + "'");
                return request.query("select CREATOR_NAME, CONTRACT_CODE from T_CON_CONTRACT where CONTRACT_ID = '" + key + "'");
            } else if (tableName == "t_con_pay_apply") {
                logger.info("select CREATOR_NAME, APPLY_CODE from t_con_pay_apply where APPLY_ID = '" + key + "'");
                return request.query("select CREATOR_NAME, APPLY_CODE from t_con_pay_apply where APPLY_ID = '" + key + "'");
            } else {
                callBack({});
                return;
            }
        }).then(result => {
            if (tableName == "t_con_contract") {
                bakMsg = result.recordset[0].CREATOR_NAME + " 发起的合同,合同编号:" + result.recordset[0].CONTRACT_CODE + " 被打回,原因是:" + remark;
            } else if (tableName == "t_con_pay_apply") {
                bakMsg = result.recordset[0].CREATOR_NAME + " 发起的合同付款申请单,申请单号:" + result.recordset[0].APPLY_CODE + " 被打回,原因是:" + remark;
            } else if (tableName == "t_con_change") {
                bakMsg = result.recordset[0].CREATOR_NAME + " 发起的合同变更,合同编号:" + result.recordset[0].CONTRACT_CODE + " 被打回,原因是:" + remark;
            } else if (tableName == "t_con_balance") {
                bakMsg = result.recordset[0].CREATOR_NAME + " 发起的合同结算,申请单号:" + result.recordset[0].APPLY_CODE + " 被打回,原因是:" + remark;
            } else {
                callBack({});
                return;
            }
            return trans.commit();
            //return trans.rollback();
        }).then(result=> {
            pool.close();
            logger.info("------------------------ success and will notice back ----------------------")
            noticeBk.wise(bakMsg, wfId, function (err, data) {
                callBack(null, "success");
            });
        }).catch(e=> {
            logger.info(e);
            if (trans) {
                trans.rollback();
            }
            callBack(e);
        });
    }

    let getBackInsts = function (setLists) {
        let found = setLists[0];
        for (let x in setLists) {
            if (found.FBACKACTINSTIDS.length < setLists[x].FBACKACTINSTIDS.length) {
                found = setLists[x];
            }
        }
        return found.FBACKACTINSTIDS + found.FACTINSTID + ":" + found.FACTIVITYID + ",";
    }

    sql.connect({
            user: conf.htdb.username,
            password: conf.htdb.password,
            server: conf.htdb.hostname, // You can use 'localhost\\instance' to connect to named instance
            database: conf.htdb.database
        }
    ).then(conPool => {
        pool = conPool;
        pool.on('error', err => {
            logger.info(err);
            pool.close();
        });
        return new sql.Transaction(pool).begin();
    }).then(reTrans=> {
        trans = reTrans;
        request = new sql.Request(trans);


        logger.info("select NODE_ID, NODE_NAME, ACTOR, ACTOR as CREATOR_ID, NODE_TYPE, " +
            "(select top 1 state from T_SYS_WF_TASK where node_id = '" + nodeId + "' order by create_time desc ) as State " +
            "from T_SYS_WF_NODE where WF_ID = '" + wfId + "' and NODE_TYPE = '1'")


        return request.query("select NODE_ID, NODE_NAME, ACTOR, ACTOR as CREATOR_ID, NODE_TYPE, " +
            "(select top 1 state from T_SYS_WF_TASK where node_id = '" + nodeId + "' order by create_time desc ) as State " +
            "from T_SYS_WF_NODE where WF_ID = '" + wfId + "' and NODE_TYPE = '1'");
    }).then(defxml=> {
        if (defxml.recordset[0].STATE == 4) {
            callBack(null, "此消息已审核");
        } else {
            if (defxml.recordset[0].NODE_TYPE == 1) {

                nextNodeId = defxml.recordset[0].NODE_ID;
                nodeName = defxml.recordset[0].NODE_NAME;
                actor = defxml.recordset[0].ACTOR;
                creator = defxml.recordset[0].CREATOR_ID;
                realProcess();
            } else if (defxml.recordset[0].NODE_TYPE == 9) {
                complete();
            } else {
                callBack({
                    message: "无此节点类型",
                    error: {
                        status: 0,
                        stack: "无此节点类型"
                    }
                })
            }
        }
    }).catch(e=> {
        logger.info(e);
        callBack(e);
    });

}

function agree(wfId, nodeId, key, remark, tableName, callBack) {

    if (sql) {
        sql.close();
    }

    logger.info("wfId " + wfId)
    logger.info("nodeId " + nodeId)
    logger.info("key " + key)
    logger.info("remark " + remark)

    var request = null;
    var trans = null;
    var pool = null;
    let nextNodeId = null;
    let nodeName = null;
    let actor = null;
    let creator = null;
    let taskId = Date.now() + Id.get();

    let middle = function () {
        logger.info("update T_SYS_WF_TASK set FINISH_TIME = GETDATE(), REMARK='" + remark + "', opinion = '同意', " +
            "view_state = '1', STATE = 4 where NODE_ID = '" + nodeId + "';")
        request.query("update T_SYS_WF_TASK set FINISH_TIME = GETDATE(), REMARK='" + remark + "', opinion = '同意', " +
            "view_state = '1', STATE = 4 where NODE_ID = '" + nodeId + "';").then(result => {
            logger.info("update T_SYS_WF_NODE set STATE = 2 where NODE_ID = '" + nextNodeId + "';")
            return request.query("update T_SYS_WF_NODE set STATE = 2 where NODE_ID = '" + nextNodeId + "';");
        }).then(result => {
            logger.info("update T_SYS_WF_NODE set STATE = 4 where NODE_ID = '" + nodeId + "';")
            return request.query("update T_SYS_WF_NODE set STATE = 4 where NODE_ID = '" + nodeId + "';");
        }).then(result => {
            logger.info("insert into T_SYS_WF_TASK values( '" + wfId + "','" + nextNodeId + "','" + taskId + "','" + tableName + "','" + key + "',null," +
                " '" + actor + "','" + nodeName + "','P',GETDATE(),null,GETDATE(),null,0.00,null,2, '" + creator + "',null,null,null,null)");
            return request.query("insert into T_SYS_WF_TASK values( '" + wfId + "','" + nextNodeId + "','" + taskId + "','" + tableName + "','" + key + "',null," +
                " '" + actor + "','" + nodeName + "','P',GETDATE(),null,GETDATE(),null,0.00,null,2, '" + creator + "',null,null,null,null)");
        }).then(result => {
            return trans.commit();
            //return trans.rollback();
        }).then(result=> {
            pool.close();
            callBack(null, "success");
        }).catch(e=> {
            logger.info(e);
            if (trans) {
                trans.rollback();
            }
            callBack(e);
        });
    }

    let complete = function () {

        logger.info("update T_SYS_WF_TASK set FINISH_TIME = GETDATE(), REMARK='" + remark + "', opinion = '同意', " +
            "view_state = '1', STATE = 4 where NODE_ID = '" + nodeId + "';")
        request.query("update T_SYS_WF_TASK set FINISH_TIME = GETDATE(), REMARK='" + remark + "', opinion = '同意', " +
            "view_state = '1', STATE = 4 where NODE_ID = '" + nodeId + "';").then(result => {

            if (tableName == "t_con_contract") {
                logger.info("update  T_CON_CONTRACT  set CON_STATUS = 31, WF_STATUS = 4  where CONTRACT_ID = '" + key + "'");
                return request.query("update  T_CON_CONTRACT  set CON_STATUS = 31, WF_STATUS = 4  where CONTRACT_ID = '" + key + "'");
            } else if (tableName == "t_con_pay_apply") {
                logger.info("update  T_CON_PAY_APPLY  set WF_STATUS = 4  where WF_ID  = '" + wfId + "'");
                return request.query("update  T_CON_PAY_APPLY  set WF_STATUS = 4  where WF_ID  = '" + wfId + "'");
            } else if (tableName == "t_con_change") {
                logger.info("update  T_CON_CHANGE  set WF_STATUS = 4  where WF_ID  = '" + wfId + "'");
                return request.query("update  T_CON_CHANGE  set WF_STATUS = 4  where WF_ID  = '" + wfId + "'");
            } else if (tableName == "t_con_balance") {
                logger.info("update  T_CON_BALANCE  set WF_STATUS = 4  where WF_ID  = '" + wfId + "'");
                return request.query("update  T_CON_BALANCE  set WF_STATUS = 4  where WF_ID  = '" + wfId + "'");
            } else {
                callBack({});
                return;
            }
        }).then(result => {
            logger.info("update T_SYS_WF set state = 4 where WF_ID = '" + wfId + "'");
            return request.query("update T_SYS_WF set state = 4 where WF_ID = '" + wfId + "'");
        }).then(result => {
            logger.info("update T_SYS_WF_NODE set state = 4 where NODE_ID = '" + nodeId + "'");
            return request.query("update T_SYS_WF_NODE set state = 4 where NODE_ID = '" + nodeId + "'");

        }).then(result => {

            if (tableName == "t_con_balance") {
                logger.info("update T_CON_CONTRACT set BALANCED_MONEY = (select OUTPUT_VALUE_MONEY from " +
                    "T_CON_BALANCE where wf_id = '" + wfId + "') where CONTRACT_ID = (select CONTRACT_ID from " +
                    "T_CON_BALANCE where wf_id = '" + wfId + "')");
                return request.query("update T_CON_CONTRACT set BALANCED_MONEY = (select OUTPUT_VALUE_MONEY from " +
                    "T_CON_BALANCE where wf_id = '" + wfId + "') where CONTRACT_ID = (select CONTRACT_ID from " +
                    "T_CON_BALANCE where wf_id = '" + wfId + "')");
            } else {
                return;
            }

        }).then(result => {
            return trans.commit();
            //return trans.rollback();
        }).then(result=> {

            //如果是请款,需要向cloud系统发起付款申请单
            if (tableName == "t_con_pay_apply") {
                let gkdObj = {};
                let payRestSqls = [pool.request().query("select a.CONTRACT_NAME, a.CONTRACT_CODE, a.CONTRACT_MONEY, b.APPROVED_MONEY, b.APPLY_TIME, c.PROJECT_NAME, " +
                    "d.name, b.WF_ID, d.BANKNBR, d.BANKNAME, d.CLOUD_CODE, b.APPROVED_REMARK, b.APPLY_ID, a.CONTRACT_ID, b.APPLY_CODE, b.SUM_OUTPUT_VALUE_MONEY, a.SPECIALITY_ID " +
                    "from T_CON_CONTRACT a  left join T_CON_PAY_APPLY b  on a.CONTRACT_ID = b.CONTRACT_ID " +
                    "inner join T_SYS_PROJECT c on a.PROJECT_ID = c.PROJECT_ID " +
                    "inner join T_GYS_SYNC_WISE d on a.B_NAME = d.NAME " +
                    "where a.CONTRACT_ID = (select contract_id from T_CON_PAY_APPLY where wf_id = '" + wfId + "') " +
                    "and b.WF_STATUS = 4 order by b.APPLY_TIME"),
                    pool.request().query("select a.ACTOR_NAME, a.FINISH_TIME, a.REMARK, b.NODE_TYPE from T_SYS_WF_TASK a, T_SYS_WF_NODE b " +
                        "where a.wf_id = '" + wfId + "' and a.NODE_ID = b.NODE_ID order by a.FINISH_TIME")];

                logger.info("select a.CONTRACT_NAME, a.CONTRACT_CODE, a.CONTRACT_MONEY, b.APPROVED_MONEY, b.APPLY_TIME, c.PROJECT_NAME, " +
                    "d.name, b.WF_ID, d.BANKNBR, d.BANKNAME, d.CLOUD_CODE, a.SPECIALITY_ID " +
                    "from T_CON_CONTRACT a  left join T_CON_PAY_APPLY b  on a.CONTRACT_ID = b.CONTRACT_ID " +
                    "inner join T_SYS_PROJECT c on a.PROJECT_ID = c.PROJECT_ID " +
                    "inner join T_GYS_SYNC_WISE d on a.B_NAME = d.NAME " +
                    "where a.CONTRACT_ID = (select contract_id from T_CON_PAY_APPLY where wf_id = '" + wfId + "') " +
                    "and b.WF_STATUS = 4 order by b.APPLY_TIME");
                logger.info("select a.ACTOR_NAME, a.FINISH_TIME, a.REMARK, b.NODE_TYPE from T_SYS_WF_TASK a, T_SYS_WF_NODE b " +
                    "where a.wf_id = '" + wfId + "' and a.NODE_ID = b.NODE_ID order by a.FINISH_TIME");

                Promise.all(payRestSqls).then(payRest => {
                    pool.close();
                    logger.info("----------------- payRest " + JSON.stringify(payRest));

                    let records = payRest[0].recordset;
                    let total_money = records[0].CONTRACT_MONEY;
                    let total_paid = 0;
                    let tenTh = 0;
                    let paid_list = [];

                    for (let j = 0; j < records.length; j++) {
                        total_paid += records[j].SUM_OUTPUT_VALUE_MONEY;
                        if (j < 9) {
                            paid_list.push({
                                curPaid: total_paid,
                                curPrecent: total_paid / total_money,
                                curPaidTime: cmoment(records[j].APPLY_TIME).utcOffset("-00:00").format('YYYY-MM-DD'),
                                curApply: records[j].APPROVED_MONEY,
                                totalPaid: records[j].SUM_OUTPUT_VALUE_MONEY,
                                curBnkNbr: records[j].BANKNBR,
                                curBnkName: records[j].BANKNAME
                            });
                        } else if (j < records.length - 1) {
                            tenTh += records[j].SUM_OUTPUT_VALUE_MONEY;
                        } else {
                            paid_list.push({
                                curPaid: tenTh,
                                curPrecent: tenTh / total_money,
                                curPaidTime: cmoment(records[j].APPLY_TIME).utcOffset("-00:00").format('YYYY-MM-DD'),
                                curApply: records[j].APPROVED_MONEY,
                                totalPaid: records[j].SUM_OUTPUT_VALUE_MONEY,
                                curBnkNbr: records[j].BANKNBR,
                                curBnkName: records[j].BANKNAME
                            });
                        }
                        // 代表是本次申请
                        if (wfId == records[j].WF_ID) {
                            gkdObj = {
                                conName: records[j].CONTRACT_NAME,
                                conCode: records[j].CONTRACT_CODE,
                                conMoney: records[j].CONTRACT_MONEY,
                                prjName: records[j].PROJECT_NAME,
                                venNbr: records[j].CLOUD_CODE,
                                approveRemark: records[j].APPROVED_REMARK,
                                applyTime: cmoment(records[j].APPLY_TIME).utcOffset("-00:00").format('YYYY-MM-DD'),
                                applyMoney: records[j].APPROVED_MONEY,
                                FAPPLY_ID: records[j].APPLY_ID,
                                APPLY_CODE: records[j].APPLY_CODE,
                                CONTRACT_ID: records[j].CONTRACT_ID,
                                SPECIALITY_ID: records[j].SPECIALITY_ID,
                                unittype: "BD_Supplier"
                            }
                        }
                    }

                    let auditRecords = payRest[1].recordset;
                    let audit_list = [];

                    for (let j in auditRecords) {
                        if (auditRecords[j].NODE_TYPE == "1") {
                            gkdObj.creator = auditRecords[j].ACTOR_NAME;
                        } else {
                            audit_list.push({
                                actor: auditRecords[j].ACTOR_NAME,
                                remark: auditRecords[j].REMARK,
                                time: cmoment(auditRecords[j].FINISH_TIME).utcOffset("-00:00").format('YYYY-MM-DD HH:mm')
                            });
                        }
                    }


                    logger.info(JSON.stringify(audit_list));

                    gkdObj.paidList = paid_list;
                    gkdObj.auditList = audit_list;
                    gkdObj.totalPaid = total_paid;

                    logger.info("=======================================================");
                    logger.info(JSON.stringify(gkdObj));
                    logger.info("=======================================================");

                    fkd(gkdObj, function (err, data) {
                        if (err) {
                            logger.info("------------- error  twice  and will return here -----------------");
                            callBack(err);
                        } else {

                            sql.connect({
                                    user: conf.htdb.username,
                                    password: conf.htdb.password,
                                    server: conf.htdb.hostname, // You can use 'localhost\\instance' to connect to named instance
                                    database: conf.htdb.database
                                }
                            ).then(htPool => {
                                logger.info("+++++++++++++++++++++++++++++++++++++" + htPool);
                                logger.info("insert into T_FKD_APPLY select '" + data.Result.Id + "',a.APPLY_ID, a.APPLY_CODE, a.CONTRACT_ID, b.B_ID, " +
                                    "b.B_NAME, b.APPROVED_MONEY,a.HANDLER, GETDATE() from T_CON_PAY_APPLY a , T_CON_CONTRACT b where " +
                                    "a.CONTRACT_ID = b.CONTRACT_ID and a.wf_id = '" + wfId + "'");
                                htPool.request().query("insert into T_FKD_APPLY select '" + data.Result.Id + "', a.APPLY_ID, a.APPLY_CODE, a.CONTRACT_ID, b.B_ID, " +
                                    "b.B_NAME, a.APPROVED_MONEY,a.HANDLER, GETDATE() from T_CON_PAY_APPLY a , T_CON_CONTRACT b where " +
                                    "a.CONTRACT_ID = b.CONTRACT_ID and a.wf_id = '" + wfId + "'").then(result => {
                                    logger.info("===================== " + JSON.stringify(result));
                                    htPool.close();
                                    callBack(null, "success");
                                }).catch(e=> {
                                    logger.info(e)
                                    htPool.close();
                                    callBack(e);
                                })

                            });
                        }
                    });
                }).catch(e=> {
                    logger.info("abcdefg:" + e);
                    pool.close();
                    callBack(e);
                })
            }
            else if (tableName == "t_con_contract") {


                let conObj = {};
                let payRestSqls = [pool.request().query("select a.CONTRACT_CODE, a.CONTRACT_MONEY,a.CREATOR_NAME,a.REMARK, a.SUMMARY,a.B_NAME, a.CREATE_TIME,b.PROJECT_NAME, " +
                    "a.CONTRACT_NAME, c.SPECIALITY_NAME , a.WF_ID, d.WFT_NAME " +
                    "from T_CON_CONTRACT a, T_SYS_PROJECT b, T_CON_SPECIALITY c, T_SYS_WFT d, T_SYS_WF e " +
                    "where a.PROJECT_ID = b.PROJECT_ID and a.SPECIALITY_ID = c.SPECIALITY_ID " +
                    "and d.WFT_ID = e.WFT_ID and a.WF_ID = e.WF_ID " +
                    "and a.CONTRACT_ID = (select CONTRACT_ID from T_CON_CONTRACT where WF_ID = '" + wfId + "')"),
                    pool.request().query("select a.ACTOR_NAME, a.FINISH_TIME, a.REMARK from T_SYS_WF_TASK a, T_SYS_WF_NODE b " +
                        "where a.wf_id = '" + wfId + "' and a.NODE_ID = b.NODE_ID and b.NODE_TYPE = 2 order by a.FINISH_TIME"),
                    pool.request().query("select c.NAME, b.FATHER, a.ORG_ID from T_SYS_ORG_USER a, T_SYS_ORGNIZATION b, T_SYS_ORGNIZATION c " +
                        "where a.ORG_ID = b.ID and b.FATHER = c.ID and a.USER_ID = (select CREATOR_ID from T_CON_CONTRACT where " +
                        "CONTRACT_ID = (select CONTRACT_ID from T_CON_CONTRACT where WF_ID = '" + wfId + "')) and " +
                        "a.org_id != (select CREATOR_ID from T_CON_CONTRACT where CONTRACT_ID = " +
                        "(select CONTRACT_ID from T_CON_CONTRACT where WF_ID = '" + wfId + "')) and a.org_id !='0'"),
                    pool.request().query("select c.FDINACT, b.NAME from T_CON_CONTRACT a, T_SYS_USERINFO b, T_DIN_ACCT c " +
                        "where a.CREATOR_ID = b.ORG_ID and b.MOBILE = c.FPHONE and a.WF_ID ='" + wfId + "'")];

                logger.info("select a.CONTRACT_CODE, a.CONTRACT_MONEY,a.CREATOR_NAME,a.REMARK, a.SUMMARY,a.B_NAME, a.CREATE_TIME,b.PROJECT_NAME, " +
                    "a.CONTRACT_NAME, c.SPECIALITY_NAME , a.WF_ID, d.WFT_NAME " +
                    "from T_CON_CONTRACT a, T_SYS_PROJECT b, T_CON_SPECIALITY c, T_SYS_WFT d, T_SYS_WF e " +
                    "where a.PROJECT_ID = b.PROJECT_ID and a.SPECIALITY_ID = c.SPECIALITY_ID " +
                    "and d.WFT_ID = e.WFT_ID and a.WF_ID = e.WF_ID " +
                    "and a.CONTRACT_ID = (select CONTRACT_ID from T_CON_CONTRACT where WF_ID = '" + wfId + "')");
                logger.info("select a.ACTOR_NAME, a.FINISH_TIME, a.REMARK from T_SYS_WF_TASK a, T_SYS_WF_NODE b " +
                    "where a.wf_id = '" + wfId + "' and a.NODE_ID = b.NODE_ID and b.NODE_TYPE = 2 order by a.FINISH_TIME");
                logger.info("select c.NAME, b.FATHER, a.ORG_ID from T_SYS_ORG_USER a, T_SYS_ORGNIZATION b, T_SYS_ORGNIZATION c " +
                    "where a.ORG_ID = b.ID and b.FATHER = c.ID and a.USER_ID = (select CREATOR_ID from T_CON_CONTRACT where " +
                    "CONTRACT_ID = (select CONTRACT_ID from T_CON_CONTRACT where WF_ID = '" + wfId + "')) and " +
                    "a.org_id != (select CREATOR_ID from T_CON_CONTRACT where CONTRACT_ID = " +
                    "(select CONTRACT_ID from T_CON_CONTRACT where WF_ID = '" + wfId + "')) and a.org_id !='0'");
                logger.info("select c.FDINACT, b.NAME from T_CON_CONTRACT a, T_SYS_USERINFO b, T_DIN_ACCT c " +
                    "where a.CREATOR_ID = b.ORG_ID and b.MOBILE = c.FPHONE and a.WF_ID ='" + wfId + "'");

                Promise.all(payRestSqls).then(payRest => {
                    pool.close();
                    let records = payRest[0].recordset[0];
                    let bmSet = payRest[2].recordset[0];
                    let creatorRec = payRest[3].recordset;

                    logger.info(JSON.stringify(records))
                    logger.info(JSON.stringify(bmSet))
                    logger.info(JSON.stringify(creatorRec))
                    conObj = {
                        "htzynr": records.SUMMARY ? records.SUMMARY.replace(/\r\n/g, "") : "",
                        "bz": records.REMARK ? records.REMARK.replace(/\r\n/g, "") : "",
                        "jbr": records.CREATOR_NAME,
                        "htjk": records.CONTRACT_MONEY,
                        "xmmc": records.PROJECT_NAME,
                        "htbh": records.CONTRACT_CODE,
                        "htmc": records.CONTRACT_NAME,
                        "dfdw": records.B_NAME,
                        "jbbm": bmSet.NAME,
                        "htlb": records.SPECIALITY_NAME,
                        "fkfs": "银行汇款",
                        "jbsj": cmoment(records.CREATE_TIME).utcOffset("-00:00").format('YYYY-MM-DD HH:mm')
                    }

                    if (creatorRec.length > 0) {
                        conObj.creatorId = creatorRec[0].FDINACT;
                        conObj.creatorName = creatorRec[0].NAME;
                    }

                    let auditRecords = payRest[1].recordset;
                    let audit_list = [];

                    for (let j in auditRecords) {
                        if (auditRecords[j].NODE_TYPE == "1") {
                            gkdObj.creator = auditRecords[j].ACTOR_NAME;
                        } else {
                            audit_list.push({
                                actor: auditRecords[j].ACTOR_NAME,
                                remark: auditRecords[j].REMARK,
                                time: cmoment(auditRecords[j].FINISH_TIME).utcOffset("-00:00").format('YYYY-MM-DD HH:mm')
                            });
                        }
                    }


                    logger.info(JSON.stringify(audit_list));

                    conObj.auditList = audit_list;

                    logger.info("=======================================================");
                    logger.info(JSON.stringify(conObj));
                    logger.info("=======================================================");

                    saveContract(conObj, function (err, data) {
                        if (err) {
                            logger.info("------------- error  twice  and will return here -----------------");
                            callBack(err);
                        } else {
                            callBack(null, "success");
                        }
                    });
                }).catch(e=> {
                    logger.info("abcdefg:" + e);
                    pool.close();
                    callBack(e);
                })
            }
            else if (tableName == "t_con_change") {

                let conObj = {};
                let payRestSqls = [pool.request().query("select a.CONTRACT_CODE, a.CONTRACT_MONEY,a.CREATOR_NAME,a.REMARK, " +
                    "a.SUMMARY,a.B_NAME, a.CREATE_TIME,b.PROJECT_NAME, a.CONTRACT_NAME, c.SPECIALITY_NAME , a.WF_ID , d.APPROVED_MONEY, " +
                    "d.SUMMARY as BGMS, d.REASON as BGYY from T_CON_CONTRACT a, T_SYS_PROJECT b, T_CON_SPECIALITY c, T_CON_CHANGE d where " +
                    "a.PROJECT_ID = b.PROJECT_ID and a.SPECIALITY_ID = c.SPECIALITY_ID and a.CONTRACT_ID = d.CONTRACT_ID and d.WF_ID = '" + wfId + "'"),
                    pool.request().query("select a.ACTOR_NAME, a.FINISH_TIME, a.REMARK from T_SYS_WF_TASK a, T_SYS_WF_NODE b " +
                        "where a.wf_id = '" + wfId + "' and a.NODE_ID = b.NODE_ID and b.NODE_TYPE = 2 order by a.FINISH_TIME"),
                    pool.request().query("select sum(a.approved_money) as TOTAL_PAY from T_CON_PAY_APPLY a , T_CON_CHANGE b " +
                        "where a.CONTRACT_ID = b.CONTRACT_ID and a.WF_STATUS = '4' and b.WF_ID = '" + wfId + "'")];

                logger.info("select a.CONTRACT_CODE, a.CONTRACT_MONEY,a.CREATOR_NAME,a.REMARK, " +
                    "a.SUMMARY,a.B_NAME, a.CREATE_TIME,b.PROJECT_NAME, a.CONTRACT_NAME, c.SPECIALITY_NAME , a.WF_ID , d.APPROVED_MONEY, " +
                    "d.SUMMARY as BGMS, d.REASON as BGYY from T_CON_CONTRACT a, T_SYS_PROJECT b, T_CON_SPECIALITY c, T_CON_CHANGE d where " +
                    "a.PROJECT_ID = b.PROJECT_ID and a.SPECIALITY_ID = c.SPECIALITY_ID and a.CONTRACT_ID = d.CONTRACT_ID and d.WF_ID = '" + wfId + "'");
                logger.info("select a.ACTOR_NAME, a.FINISH_TIME, a.REMARK from T_SYS_WF_TASK a, T_SYS_WF_NODE b " +
                    "where a.wf_id = '" + wfId + "' and a.NODE_ID = b.NODE_ID and b.NODE_TYPE = 2 order by a.FINISH_TIME");
                logger.info("select sum(a.approved_money) as totalPay from T_CON_PAY_APPLY a , T_CON_CHANGE b " +
                    "where a.CONTRACT_ID = b.CONTRACT_ID and a.WF_STATUS = '4' and b.WF_ID = '" + wfId + "'");

                Promise.all(payRestSqls).then(payRest => {
                    pool.close();
                    let records = payRest[0].recordset[0];
                    let paySet = payRest[2].recordset[0];
                    let totalUnPaid = records.CONTRACT_MONEY - payRest.TOTAL_PAY;

                    logger.info(JSON.stringify(records))
                    logger.info(JSON.stringify(paySet))

                    conObj = {
                        "xmmc": records.PROJECT_NAME,
                        "htbh": records.CONTRACT_CODE,
                        "htlb": records.SPECIALITY_NAME,
                        "htmc": records.CONTRACT_NAME,
                        "dfdw": records.B_NAME,
                        "htje": records.CONTRACT_MONEY,
                        "ljyfeje": paySet.TOTAL_PAY,//累计已付额金额
                        "wfkje": totalUnPaid,//未付款金额
                        "bgje": records.APPROVED_MONEY,
                        "bghjtje": records.APPROVED_MONEY + records.CONTRACT_MONEY,
                        "bghwfk": records.APPROVED_MONEY + records.CONTRACT_MONEY - paySet.TOTAL_PAY,
                        "fwfbl": ((records.APPROVED_MONEY + records.CONTRACT_MONEY - paySet.TOTAL_PAY) / (records.APPROVED_MONEY + records.CONTRACT_MONEY)).toFixed(2) + "%",
                        "bglr": records.BGYY ? records.BGYY.replace(/\r\n/g, "") : "",
                        "bgjems": records.BGMS ? records.BGMS.replace(/\r\n/g, "") : "",
                        "bz": records.BGMS ? records.BGMS.replace(/\r\n/g, "") : "",  //备注
                    }

                    let auditRecords = payRest[1].recordset;
                    let audit_list = [];

                    for (let j in auditRecords) {
                        if (auditRecords[j].NODE_TYPE == "1") {
                            gkdObj.creator = auditRecords[j].ACTOR_NAME;
                        } else {
                            audit_list.push({
                                actor: auditRecords[j].ACTOR_NAME,
                                remark: auditRecords[j].REMARK,
                                time: cmoment(auditRecords[j].FINISH_TIME).utcOffset("-00:00").format('YYYY-MM-DD HH:mm')
                            });
                        }
                    }


                    logger.info(JSON.stringify(audit_list));

                    conObj.auditList = audit_list;

                    logger.info("=======================================================");
                    logger.info("变更: " + JSON.stringify(conObj));
                    logger.info("=======================================================");

                    saveBgSqd(conObj, function (err, data) {
                        if (err) {
                            logger.info("------------- error  twice  and will return here -----------------");
                            callBack(err);
                        } else {
                            callBack(null, "success");
                        }
                    });
                }).catch(e=> {
                    logger.info("abcdefg:" + e);
                    pool.close();
                    callBack(e);
                })
            }
            else if (tableName == "t_con_balance") {

                let conObj = {};
                let payRestSqls = [pool.request().query("select b.CONTRACT_CODE,b.CONTRACT_NAME,b.CONTRACT_MONEY,a.WATCH_MONEY,b.SIGN_DATE, " +
                    "c.PROJECT_NAME, b.B_NAME ,a.REMARK, a.REQUEST_TIME from T_CON_BALANCE a, T_CON_CONTRACT b, T_SYS_PROJECT c where " +
                    "a.CONTRACT_ID = b.CONTRACT_ID and b.PROJECT_ID = c.PROJECT_ID and a.WF_STATUS = 4 and a.WF_ID = '" + wfId + "'"),
                    pool.request().query("select a.ACTOR_NAME, a.FINISH_TIME, a.REMARK from T_SYS_WF_TASK a, T_SYS_WF_NODE b " +
                        "where a.wf_id = '" + wfId + "' and a.NODE_ID = b.NODE_ID and b.NODE_TYPE = 2 order by a.FINISH_TIME")];

                logger.info("select b.CONTRACT_CODE,b.CONTRACT_NAME,b.CONTRACT_MONEY,a.WATCH_MONEY,b.SIGN_DATE, " +
                    "c.PROJECT_NAME, b.B_NAME ,a.REMARK, a.REQUEST_TIME from T_CON_BALANCE a, T_CON_CONTRACT b, T_SYS_PROJECT c where " +
                    "a.CONTRACT_ID = b.CONTRACT_ID and b.PROJECT_ID = c.PROJECT_ID and a.WF_STATUS = 4 and a.WF_ID = '" + wfId + "'");
                logger.info("select a.ACTOR_NAME, a.FINISH_TIME, a.REMARK from T_SYS_WF_TASK a, T_SYS_WF_NODE b " +
                    "where a.wf_id = '" + wfId + "' and a.NODE_ID = b.NODE_ID and b.NODE_TYPE = 2 order by a.FINISH_TIME");

                Promise.all(payRestSqls).then(payRest => {
                    pool.close();
                    let records = payRest[0].recordset[0];

                    logger.info(JSON.stringify(records))

                    conObj = {
                        "xmmc": records.PROJECT_NAME,
                        "htbh": records.CONTRACT_CODE,
                        "htmc": records.CONTRACT_NAME,
                        "dfdw": records.B_NAME,
                        "htje": records.CONTRACT_MONEY,
                        "httjje": records.WATCH_MONEY - records.CONTRACT_MONEY,//累计已付额金额
                        "jgjsje": records.WATCH_MONEY,//未付款金额
                        "htjdsj": cmoment(records.SIGN_DATE).utcOffset("-00:00").format('YYYY-MM-DD'),
                        "htjgsj": cmoment(records.REQUEST_TIME).utcOffset("-00:00").format('YYYY-MM-DD'),
                        "bz": records.REMARK ? records.REMARK.replace(/\r\n/g, "") : ""  //备注
                    }

                    let auditRecords = payRest[1].recordset;
                    let audit_list = [];

                    for (let j in auditRecords) {
                        if (auditRecords[j].NODE_TYPE == "1") {
                            gkdObj.creator = auditRecords[j].ACTOR_NAME;
                        } else {
                            audit_list.push({
                                actor: auditRecords[j].ACTOR_NAME,
                                remark: auditRecords[j].REMARK,
                                time: cmoment(auditRecords[j].FINISH_TIME).utcOffset("-00:00").format('YYYY-MM-DD HH:mm')
                            });
                        }
                    }

                    logger.info(JSON.stringify(audit_list));

                    conObj.auditList = audit_list;

                    logger.info("=======================================================");
                    logger.info("结算算金额: " + JSON.stringify(conObj));
                    logger.info("=======================================================");

                    saveBalanced(conObj, function (err, data) {
                        if (err) {
                            logger.info("------------- error  twice  and will return here -----------------");
                            callBack(err);
                        } else {
                            callBack(null, "success");
                        }
                    });
                }).catch(e=> {
                    logger.info("abcdefg:" + e);
                    pool.close();
                    callBack(e);
                })
            }
            else {
                pool.close();
                callBack(null, "success");
            }
        }).catch(e=> {
            logger.info(e);
            if (trans) {
                trans.rollback();
            }
            callBack(e);
        });
    }

    let getBackInsts = function (setLists) {
        let found = setLists[0];
        for (let x in setLists) {
            if (found.FBACKACTINSTIDS.length < setLists[x].FBACKACTINSTIDS.length) {
                found = setLists[x];
            }
        }
        return found.FBACKACTINSTIDS + found.FACTINSTID + ":" + found.FACTIVITYID + ",";
    }

    sql.connect({
            user: conf.htdb.username,
            password: conf.htdb.password,
            server: conf.htdb.hostname, // You can use 'localhost\\instance' to connect to named instance
            database: conf.htdb.database
        }
    ).then(conPool => {
        pool = conPool;
        pool.on('error', err => {
            logger.info(err);
            pool.close();
        });
        return new sql.Transaction(pool).begin();
    }).then(reTrans=> {
        trans = reTrans;
        request = new sql.Request(trans);


        logger.info("select b.NODE_TYPE, c.END_NODE_ID , b.NODE_NAME, b.ACTOR , " +
            "(select top 1 CREATOR_ID from T_SYS_WF_TASK where NODE_ID ='" + nodeId + "' order by CREATE_TIME desc ) as CREATOR_ID, " +
            "(select top 1 STATE from T_SYS_WF_TASK where NODE_ID ='" + nodeId + "' order by CREATE_TIME desc ) as STATE " +
            "from T_SYS_WF_NODE b , T_SYS_WF_LINE c where c.END_NODE_ID = b.NODE_ID and c.START_NODE_ID =  '" + nodeId + "'")


        return request.query("select b.NODE_TYPE, c.END_NODE_ID , b.NODE_NAME, b.ACTOR , " +
            "(select top 1 CREATOR_ID from T_SYS_WF_TASK where NODE_ID ='" + nodeId + "' order by CREATE_TIME desc ) as CREATOR_ID, " +
            "(select top 1 STATE from T_SYS_WF_TASK where NODE_ID ='" + nodeId + "' order by CREATE_TIME desc ) as STATE " +
            "from T_SYS_WF_NODE b , T_SYS_WF_LINE c where c.END_NODE_ID = b.NODE_ID and c.START_NODE_ID =  '" + nodeId + "'");
    }).then(defxml=> {
        if (defxml.recordset[0].STATE == 4) {
            callBack(null, "此消息已审核");
        } else {
            if (defxml.recordset[0].NODE_TYPE == 2) {

                nextNodeId = defxml.recordset[0].END_NODE_ID;
                nodeName = defxml.recordset[0].NODE_NAME;
                actor = defxml.recordset[0].ACTOR;
                creator = defxml.recordset[0].CREATOR_ID;
                middle();
            } else if (defxml.recordset[0].NODE_TYPE == 9) {
                logger.info("----- will complete -------");
                complete();
            } else {
                callBack({
                    message: "无此节点类型",
                    error: {
                        status: 0,
                        stack: "无此节点类型"
                    }
                })
            }
        }
    }).catch(e=> {
        logger.info(e);
        callBack(e);
    });

}

action.agree = agree;
action.back = back;
module.exports = action;