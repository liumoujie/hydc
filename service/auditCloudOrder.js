/**
 * Created by zhoulanhong on 9/14/17.
 */

var request = require('sync-request');
var conf = require('../config/config');
var sql = require('mssql');
var token = require('../libs/token');

var logger = require('../config/log4js').getLogger('saveFksqd');

//requestJson
//json: {
//    "parameters": [
//        "SD_FDC_HTHBD",
//        "{\"Numbers\":[\"HTHB2018081000002\"]}"
//    ]
//}

function save(requestJson, cb) {

    if (sql) {
        sql.close();
    }

    let loginBack = request('POST', conf.webApi.loginUrl
        , {
            json: {
                "parameters": [
                    conf.webApi.dbCenterId,
                    conf.webApi.userName,
                    conf.webApi.passwd,
                    conf.webApi.lang
                ]
            }
        });
    logger.info(conf.webApi.loginUrl);
    logger.info({
        "parameters": [
            conf.webApi.dbCenterId,
            conf.webApi.userName,
            conf.webApi.passwd,
            conf.webApi.lang
        ]
    });
    let rsp = loginBack.getBody('utf8');
    logger.info(JSON.stringify(rsp));
    let kdCooie = null;
    logger.info(JSON.stringify(kdCooie));
    let rspJsonObj = null;
    try {
        logger.info(loginBack.headers);
        let ckstr = JSON.stringify(loginBack.headers);//.substring(loginBack.headers.toString().indexOf('set-cookie'));
        logger.info(ckstr);
        logger.info(ckstr.substring(ckstr.indexOf('kdservice-sessionid')));
        ckstr = (ckstr.substring(ckstr.indexOf('kdservice-sessionid'))).split(";");
        kdCooie = ckstr[0];
        logger.info("kdCooie" + kdCooie);
        rspJsonObj = JSON.parse(rsp);
        logger.info(JSON.stringify(rspJsonObj));
        if (rspJsonObj.LoginResultType != "1") {
            cb({
                message: "登陆k3cloud出错",
                error: {
                    status: 0,
                    stack: rspJsonObj.Message
                }
            });
            return;
        }
    } catch (e) {
        logger.info(e);
        cb(e);
        return;
    }

    let rspJson = null;
    try {
            logger.info("--- will start submit/audit fksqd ---");
            //保存完成之后,自动提交,自动审核
            try {
                try {
                    let auditBack = request('POST', conf.webApi.audit
                        , {
                            headers: {
                                'Cookie': kdCooie
                            },
                            json: requestJson,
                            //json: {
                            //    "parameters": [
                            //        "SD_FDC_HTHBD",
                            //        "{\"Numbers\":[\"HTHB2018081000002\"]}"
                            //    ]
                            //}
                        }).getBody('utf8');

                    console.log("--- audit back : " + JSON.stringify(auditBack));

                    cb(null,"success");

                } catch (e) {

                }
            } catch (e) {
                cb(e);
            }

    } catch (e) {
        cb(e);
    }

    sql.on('error', function (e) {
        logger.info('-----------------------------');
        logger.info(e);
    })

}

module.exports = save;