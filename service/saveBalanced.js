/**
 * Created by zhoulanhong on 9/14/17.
 */

var request = require('sync-request');
var conf = require('../config/config');
var sql = require('mssql');
var token = require('../libs/token');

var logger = require('../config/log4js').getLogger('saveBalanced');

function save(obj, cb) {

    if (sql) {
        sql.close();
    }

    let loginBack = request('POST', conf.webApi.loginUrl
        , {
            json: {
                "parameters": [
                    conf.webApi.dbCenterId,
                    conf.webApi.userName,
                    conf.webApi.passwd,
                    conf.webApi.lang
                ]
            }
        });
    let rsp = loginBack.getBody('utf8');
    logger.info("===================== ===== "+JSON.stringify(obj));
    let kdCooie = null;
    logger.info(JSON.stringify(kdCooie));
    let rspJsonObj = null;
    try {
        logger.info(loginBack.headers);
        let ckstr = JSON.stringify(loginBack.headers);//.substring(loginBack.headers.toString().indexOf('set-cookie'));
        logger.info(ckstr);
        logger.info(ckstr.substring(ckstr.indexOf('kdservice-sessionid')));
        ckstr = (ckstr.substring(ckstr.indexOf('kdservice-sessionid'))).split(";");
        kdCooie = ckstr[0];
        logger.info("kdCooie" + kdCooie);
        rspJsonObj = JSON.parse(rsp);
        logger.info(JSON.stringify(rspJsonObj));
        if (rspJsonObj.LoginResultType != "1") {
            cb({
                message: "登陆k3cloud出错",
                error: {
                    status: 0,
                    stack: rspJsonObj.Message
                }
            });
            return;
        }
    } catch (e) {
        logger.info(e);
        cb(e);
        return;
    }

    //审批时间:F_KLM_Date
    //审批意见:F_KLM_Text8
    //审批人:F_KLM_Text7

    let auditListStr = "{\"F_KLM_Text7\":\"" +
        obj.auditList[0].actor + "\",\"F_KLM_Text8\":\"" +
        obj.auditList[0].remark + "\",\"F_KLM_Date\":\"" +
        obj.auditList[0].time + "\"}";
    for (let i = 1; i < obj.auditList.length; i++) {
        auditListStr += ",{\"F_KLM_Text7\":\"" +
            obj.auditList[i].actor + "\",\"F_KLM_Text8\":\"" +
            obj.auditList[i].remark + "\",\"F_KLM_Date\":\"" +
            obj.auditList[i].time + "\"}"
    }

    let jsonPara = {
        "parameters": [
            "k4a5681686dc74235b3a251fbff7223a5",
            "{\"Model\":{" +
            "\"F_KLM_Text\":\"" + obj.xmmc + "\"," +   // 项目名称
            "\"F_KLM_Text1\":\"" + obj.htbh + "\"," +   // 合同编号
            "\"F_KLM_Text2\":\"" + obj.htmc + "\"," +   // 合同名称
            "\"F_KLM_Text3\":\"" + obj.dfdw + "\"," +    //  对方单位
            "\"F_KLM_Decimal\":\"" + obj.htje + "\"," +    //  合同金额
            "\"F_KLM_Decimal1\":\"" + obj.httjje + "\"," +    //  合同调整金额
            "\"F_KLM_Decimal2\":\"" + obj.jgjsje + "\"," +    //  竣工结算金额
            "\"F_KLM_Text4\":\"" + obj.htjdsj + "\"," +    //  合同签订时间
            "\"F_KLM_Text5\":\"" + obj.htjgsj + "\"," +    //  合同竣工时间
            "\"F_KLM_Text6\":\"" + obj.bz + "\"," +    //  备注

            "\"FEntity\":[" + auditListStr + "]" +
            "}}"
        ]
    }
    logger.info(JSON.stringify(jsonPara));
    let saveBack = request('POST', conf.webApi.saveFksqdUrl
        , {
            headers: {
                'Cookie': kdCooie
            },
            json: jsonPara
        });
    let saveRsp = saveBack.getBody('utf8');
    logger.info(saveRsp);
    let supplierNumber = null;
    let rspJson = null;
    try {
        rspJson = JSON.parse(saveRsp);
        logger.info(JSON.stringify(rspJson));
        if (rspJson.Result.ResponseStatus.IsSuccess) {
            logger.info("--- will start submit/audit fksqd ---");

            //保存完成之后,自动提交,自动审核
            try {
                rspJson = JSON.parse(saveRsp);
                supplierNumber = rspJson.Result.Number;
                logger.info(JSON.stringify(rspJson));

                let submitBack = request('POST', conf.webApi.submit
                    , {
                        headers: {
                            'Cookie': kdCooie
                        },
                        json: {
                            "parameters": [
                                "k4a5681686dc74235b3a251fbff7223a5",
                                "{\"Numbers\":[\"" + supplierNumber + "\"]}"
                            ]
                        }
                    }).getBody('utf8');

                let submitRsp = null;
                try {
                    submitRsp = JSON.parse(submitRsp);
                    logger.info(JSON.stringify(submitRsp))
                    let auditBack = request('POST', conf.webApi.audit
                        , {
                            headers: {
                                'Cookie': kdCooie
                            },
                            json: {
                                "parameters": [
                                    "k4a5681686dc74235b3a251fbff7223a5",
                                    "{\"Numbers\":[\"" + supplierNumber + "\"]}"
                                ]
                            }
                        }).getBody('utf8');

                    cb(null, "success");
                } catch (e) {

                }
            } catch (e) {
                cb(e);
            }

        } else {
            logger.info("-------------- in  cb callback --------- ");
            cb(rspJson);
        }

    } catch (e) {
        cb(e);
    }

    sql.on('error', function (e) {
        logger.info('-----------------------------');
        logger.info(e);
    })

}

module.exports = save;

//"\"F_KLM_Entity1\":[{\"F_KLM_Text6\":\"曾晋明\",\"F_KLM_Text7\":\"同意通过\",\"F_KLM_Time\":\"1900-01-01\"}],"+
